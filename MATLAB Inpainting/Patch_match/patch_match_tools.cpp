//this function defines the functions which are tools used for the
//spatio-temporal patch_match algorithm


#include "patch_match_tools.h"

//check if the displacement values have already been used
template <class T>
bool check_already_used_patch( nTupleVolume<T> *dispField, int x, int y, int t, int dispX, int dispY, int dispT)
{
    
    if ( (((int)dispField->get_value(x,y,t,0)) == dispX) && 
            (((int)dispField->get_value(x,y,t,1)) == dispY) &&
            (((int)dispField->get_value(x,y,t,2)) == dispT)
            )
        return 1;
    else
        return 0;
    
}

//check if the pixel is occluded
template <class T>
int check_is_occluded( nTupleVolume<T> *imgVolOcc, int x, int y, int t)
{
	if (imgVolOcc->xSize == 0)
		return 0;
	if ( (imgVolOcc->get_value(x,y,t,0)) > 0)
		return 1;
	else
		return 0;
}

template <class T>
float calclulate_patch_error(nTupleVolume<T> *departVolume,nTupleVolume<T> *arrivalVolume,nTupleVolume<T> *dispField, nTupleVolume<T> *occVol,
		int xA, int yA, int tA, float minError, const parameterStruct *params)
{
	int xB, yB, tB;
	float errorOut;

	xB = (xA) + (int)dispField->get_value(xA,yA,tA,0);
	yB = (yA) + (int)dispField->get_value(xA,yA,tA,1);
	tB = (tA) + (int)dispField->get_value(xA,yA,tA,2);

	errorOut = ssd_patch_measure(departVolume, arrivalVolume,dispField,occVol, xA, yA, tA, xB, yB, tB, minError, params);
	return(errorOut);
}

template <class T>
int check_disp_field(nTupleVolume<T> *dispField, nTupleVolume<T> *departVolume, nTupleVolume<T> *arrivalVolume, nTupleVolume<T> *occVol, const parameterStruct *params)
{
	int dispValX,dispValY,dispValT,hPatchSizeX,hPatchSizeY,hPatchSizeT;
	int xB,yB,tB;
	int i,j,k,returnVal;

	hPatchSizeX = (int)floor((float)((departVolume->patchSizeX)/2));	//half the patch size
	hPatchSizeY = (int)floor((float)((departVolume->patchSizeY)/2));	//half the patch size
	hPatchSizeT = (int)floor((float)((departVolume->patchSizeT)/2));	//half the patch size

	returnVal = 0;
	for (k=hPatchSizeT; k< ((dispField->tSize) -hPatchSizeT); k++)
		for (j=hPatchSizeY; j< ((dispField->ySize) -hPatchSizeY); j++)
			for (i=hPatchSizeX; i< ((dispField->xSize) -hPatchSizeX); i++)
			{
				dispValX = (int)dispField->get_value(i,j,k,0);
				dispValY = (int)dispField->get_value(i,j,k,1);
				dispValT = (int)dispField->get_value(i,j,k,2);


				xB = dispValX + i;
				yB = dispValY + j;
				tB = dispValT + k;

				if ( check_in_inner_boundaries(arrivalVolume, xB, yB, tB,params) == 0)
				{
					MY_PRINTF("Error, the displacement is incorrect.\n");
					MY_PRINTF("xA : %d\n yA : %d\n tA : %d\n",i,j,k);
					MY_PRINTF(" dispValX : %d\n dispValY : %d\n dispValT : %d\n",dispValX,dispValY,dispValT);
					MY_PRINTF(" xB : %d\n yB : %d\n tB : %d\n",xB,yB,tB);
					returnVal= -1;
				}
				else if (check_is_occluded(occVol,xB,yB,tB) == 1)
				{
					MY_PRINTF("Error, the displacement leads to an occluded pixel.\n");
					MY_PRINTF(" xB : %d\n yB : %d\n tB : %d\n",xB,yB,tB);
					returnVal= -1;
				}
			}
	return(returnVal);

}

template <class T>
void patch_match_full_search(nTupleVolume<T> *dispField, nTupleVolume<T> *imgVolA,nTupleVolume<T> *imgVolB,
        nTupleVolume<T> *occVol, nTupleVolume<T> *modVol, const parameterStruct *params)
{
    float minSSD,ssdTemp;
    int hPatchSizeX, hPatchSizeY, hPatchSizeT;
    int i,j,k,ii,jj,kk;
    int bestX, bestY, bestT;
    
    hPatchSizeX = (int)floor((float)((dispField->patchSizeX)/2));	//half the patch size
	hPatchSizeY = (int)floor((float)((dispField->patchSizeY)/2));	//half the patch size
	hPatchSizeT = (int)floor((float)((dispField->patchSizeT)/2));	//half the patch size
            
    //#pragma omp parallel for shared(dispField, occVol, imgVolA, imgVolB) private(kk,jj,ii,minSSD,ssdTemp,bestX,bestY,bestT,i,j,k)
	for (k=hPatchSizeT; k< ((dispField->tSize) -hPatchSizeT); k++)
		for (j=hPatchSizeY; j< ((dispField->ySize) -hPatchSizeY); j++)
        {
			for (i=hPatchSizeX; i< ((dispField->xSize) -hPatchSizeX); i++)
            {
                minSSD = FLT_MAX;
                if (modVol->xSize >0)
                    if (modVol->get_value(i,j,k,0) == 0)   //if we don't want to modify this match
                       continue;
                //search for the best match
                for (kk=hPatchSizeT; kk< ((dispField->tSize) -hPatchSizeT); kk++)
                    for (jj=hPatchSizeY; jj< ((dispField->ySize) -hPatchSizeY); jj++)
                        for (ii=hPatchSizeX; ii< ((dispField->xSize) -hPatchSizeX); ii++)
                        {
                            if (occVol->xSize >0)
                                if (check_is_occluded(occVol,ii,jj,kk))   //if this pixel is occluded, continue
                                    continue;

                            ssdTemp = ssd_patch_measure<T>(imgVolA, imgVolB, dispField,occVol, i, j, k,ii, jj, kk, minSSD,params);
                            if ( (ssdTemp != -1) && (ssdTemp <= minSSD))   //we have a new best match
                            {
                                minSSD = ssdTemp;
                                bestX = ii - i;
                                bestY = jj - j;
                                bestT = kk - k;
                            }
                        }
                dispField->set_value(i,j,k,0,(T)bestX);
                dispField->set_value(i,j,k,1,(T)bestY);
                dispField->set_value(i,j,k,2,(T)bestT);
                dispField->set_value(i,j,k,3,minSSD);
            }
        }

}

template <class T>
void initialise_displacement_field(
        nTupleVolume<T> *dispField, 
        nTupleVolume<T> *departVolume, 
        nTupleVolume<T> *arrivalVolume, 
        nTupleVolume<T> *firstGuessVolume, 
        nTupleVolume<T> *occVol, 
        const parameterStruct *params)
{
	//declarations
	int i,j,k, xDisp, yDisp, tDisp, hPatchSizeX, hPatchSizeY, hPatchSizeT, hPatchSizeCeilX, hPatchSizeCeilY, hPatchSizeCeilT;
	int xMin,xMax,yMin,yMax,tMin,tMax;
	int xFirst,yFirst,tFirst;
	int isNotOcc, currShift,currLinInd;
    float ssdTemp;

	hPatchSizeX = (int)floor(((float)arrivalVolume->patchSizeX)/2);
	hPatchSizeY = (int)floor(((float)arrivalVolume->patchSizeY)/2);
	hPatchSizeT = (int)floor(((float)arrivalVolume->patchSizeT)/2);

	hPatchSizeCeilX = (int)ceil(((float)arrivalVolume->patchSizeX)/2);
	hPatchSizeCeilY = (int)ceil(((float)arrivalVolume->patchSizeY)/2);
	hPatchSizeCeilT = (int)ceil(((float)arrivalVolume->patchSizeT)/2);
    
	for (i=0; i< (dispField->xSize); i++)
		for (j=0; j< (dispField->ySize); j++)
			for (k=0; k< (dispField->tSize); k++)
			{
                if(firstGuessVolume->xSize > 0){
                    xDisp = (int)firstGuessVolume->get_value(i,j,k,0);
                    yDisp = (int)firstGuessVolume->get_value(i,j,k,1);
                    tDisp = (int)firstGuessVolume->get_value(i,j,k,2);
                }
                else{
                    xDisp = (int)FLT_MAX;
                    yDisp = (int)FLT_MAX;
                    tDisp = (int)FLT_MAX;
                }

                isNotOcc = !check_is_occluded(occVol,xDisp+i,yDisp+j,tDisp+k)
                           && check_in_inner_boundaries(arrivalVolume,xDisp+i,yDisp+j,tDisp+k,params);

                //if there is a valid first guess
                while(isNotOcc == 0){
                    // Special case where the patch size is the size of the dimension
                    if (arrivalVolume->xSize == arrivalVolume->patchSizeX)
                        xDisp = 0;
                    else
                        xDisp = ((rand()%( (arrivalVolume->xSize) -2*hPatchSizeCeilX-1)) + hPatchSizeX)-i;
                    
                    if (arrivalVolume->ySize == arrivalVolume->patchSizeY)
                        yDisp = 0;
                    else
                        yDisp = ((rand()%( (arrivalVolume->ySize) -2*hPatchSizeCeilY-1)) + hPatchSizeY)-j;
                    
                    if (arrivalVolume->tSize == arrivalVolume->patchSizeT)
                        tDisp = 0;
                    else
                        tDisp = ((rand()%( (arrivalVolume->tSize) -2*hPatchSizeCeilT-1)) + hPatchSizeT)-k;
                                        
                    isNotOcc = !check_is_occluded(occVol,xDisp+i,yDisp+j,tDisp+k)
                               && check_in_inner_boundaries(arrivalVolume,xDisp+i,yDisp+j,tDisp+k,params);
                }
                
                if (check_in_inner_boundaries(departVolume,i,j,k,params))
                    ssdTemp = ssd_patch_measure<T>(departVolume, arrivalVolume, dispField,occVol, i, j, k, i+xDisp, j+yDisp, k+tDisp, -1,params);
                else
                    ssdTemp = FLT_MAX;
                
                //if everything is all right, set the displacements
                dispField->set_value(i,j,k,0,(T)xDisp);
                dispField->set_value(i,j,k,1,(T)yDisp);
                dispField->set_value(i,j,k,2,(T)tDisp);
                dispField->set_value(i,j,k,3,(T)ssdTemp); //set the ssd error
            }
}

template <class T>
int patch_match_random_search(nTupleVolume<T> *dispField, nTupleVolume<T> *imgVolA, nTupleVolume<T> *imgVolB,
        nTupleVolume<T> *occVol, nTupleVolume<T> *modVol, const parameterStruct *params)
{
	//create random number seed
	float ssdTemp;

	int hPatchSizeX = (int)floor((float)((dispField->patchSizeX)/2));	//half the patch size
	int hPatchSizeY = (int)floor((float)((dispField->patchSizeY)/2));	//half the patch size
	int hPatchSizeT = (int)floor((float)((dispField->patchSizeT)/2));	//half the patch size
    
	//calculate the maximum z (patch search index)
    int wMax = max_int(max_int(imgVolB->xSize,imgVolB->ySize),imgVolB->tSize);
	int zMax = (int)ceil((float)-(log((float)wMax))/log(0.5f));
    int wValues[zMax];
    for (int z=0; z < zMax; z++)
        wValues[z] = (int)round_float(wMax*((float)pow(0.5f,z)));

    bool wasModified = false;
    int nbModified = 0;
    
    //#pragma omp parallel for shared(dispField, occVol, imgVolA, imgVolB,nbModified) private(ssdTemp,xTemp,yTemp,tTemp,wTemp,randMinX,randMaxX,randMinY,randMaxY,randMinT,randMaxT,xRand,yRand,tRand,i,j,k,z)
	for(int k=0; k < dispField->tSize; k++)
		for(int j=0; j < dispField->ySize; j++)
			for(int i=0; i < dispField->xSize; i++)
			{
                if (modVol->xSize >0)
                    if (modVol->get_value(i,j,k,0) == 0)   //if we don't want to modify this match
                        continue;
                
                wasModified = false;
				float ssdTemp = dispField->get_value(i,j,k,3); //get the saved ssd value
                
                for (int z=0; z < zMax; z++)	//test for different search indices
                {
                    int xTemp = i+(int)dispField->get_value(i,j,k,0);	//get the arrival position of the current offset
                    int yTemp = j+(int)dispField->get_value(i,j,k,1);	//get the arrival position of the current offset
                    int tTemp = k+(int)dispField->get_value(i,j,k,2);	//get the arrival position of the current offset
                    int wTemp = wValues[z];
                    
                    // X values
                    int randMinX = max_int(xTemp - wTemp,hPatchSizeX);
                    int randMaxX = min_int(xTemp + wTemp,imgVolB->xSize - hPatchSizeX - 1);
                    
                    // Y values
                    int randMinY = max_int(yTemp - wTemp,hPatchSizeY);
                    int randMaxY = min_int(yTemp + wTemp,imgVolB->ySize - hPatchSizeY - 1);
                    
                    // T values
                    int randMinT = max_int(tTemp - wTemp,hPatchSizeT);
                    int randMaxT = min_int(tTemp + wTemp,imgVolB->tSize - hPatchSizeT - 1);

                    //new positions in the image imgB
                    int xRand = rand_int_range(randMinX, randMaxX);	//random values between xMin and xMax, clamped to the sizes of the image B
                    int yRand = rand_int_range(randMinY, randMaxY);	//random values between yMin and yMax, clamped to the sizes of the image B
                    int tRand = rand_int_range(randMinT, randMaxT);	//random values between tMin and tMax, clamped to the sizes of the image B

                    if (check_is_occluded(occVol,xRand,yRand,tRand))
                        continue;	//the new position is occluded
                    
                    if (check_in_inner_boundaries(imgVolB,xRand,yRand,tRand,params) == 0)
                        continue;	//the new position is not in the inner boundaries

                    ssdTemp = ssd_patch_measure(imgVolA, imgVolB, dispField,occVol, i, j, k, xRand, yRand, tRand, ssdTemp,params);

                    if (ssdTemp != -1)	//we have a better match
                    {
                        dispField->set_value(i,j,k,0, (T)(xRand-i));
                        dispField->set_value(i,j,k,1, (T)(yRand-j));
                        dispField->set_value(i,j,k,2, (T)(tRand-k));
                        dispField->set_value(i,j,k,3, (T)(ssdTemp));

                        wasModified = true;
                    }
                    else
                        ssdTemp = dispField->get_value(i,j,k,3); //set the saved ssd value bakc to its proper (not -1) value
                }
                
                if(wasModified)
                    nbModified++;
			}

    return(nbModified);
}

//one iteration of the propagation of the patch match algorithm
template <class T>
int patch_match_propagation(nTupleVolume<T> *dispField, 
        nTupleVolume<T> *departVolume, 
        nTupleVolume<T> *arrivalVolume, 
        nTupleVolume<T> *occVolume,  
        nTupleVolume<T> *modVolume, 
        const parameterStruct *params, bool goForward)
{
	// Number of pixels that the displacement was changed for a neighbor's.
    int nbModified = 0;
    
    // Initialize the variables for the reversible while loop.
    bool xHasEnded = false, yHasEnded = false, tHasEnded = false;
    int x = 0, y = 0, t = 0;
    int xStart = 0, yStart = 0, tStart = 0;
    if(!goForward){
        xStart = ((dispField->xSize) - 1);
        yStart = ((dispField->ySize) - 1);
        tStart = ((dispField->tSize) - 1);
    }
    
    // The loop goes forward or backward depending on the goForward variable.
    t = tStart;
    tHasEnded = false;
    while(!tHasEnded){
        y = yStart;
        yHasEnded = false;
        while(!yHasEnded){
            x = xStart;
            xHasEnded = false;
            while(!xHasEnded){
                if(modVolume->xSize > 0 && modVolume->get_value(x,y,t,0) == 0){
                    if(goForward && x >= ((dispField->xSize) - 1)) xHasEnded = true;
                    else if(!goForward && x <= 0) xHasEnded = true;
                    if(goForward) x++;
                    else x--;
                    continue;
                }
                
                // Loop body
                int checkDispX = 0, checkDispY = 0, checkDispT = 0;
                if(goForward){
                    if(x != 0) checkDispX = -1;
                    if(y != 0) checkDispY = -1;
                    if(t != 0) checkDispT = -1;
                }
                else{
                    if(x != ((departVolume->xSize)-1)) checkDispX = 1;
                    if(y != ((departVolume->ySize)-1)) checkDispY = 1;
                    if(t != ((departVolume->tSize)-1)) checkDispT = 1;
                }
                
                bool wasModified = false;                
                float curVal = 0;
                float minVal = dispField->get_value(x,y,t,3);
                int dispX = 0, dispY = 0, dispT = 0;
                if(checkDispX != 0){
                    dispX = (int)dispField->get_value(x + checkDispX, y, t, 0);
                    dispY = (int)dispField->get_value(x + checkDispX, y, t, 1);
                    dispT = (int)dispField->get_value(x + checkDispX, y, t, 2);
                    if(check_in_inner_boundaries(arrivalVolume,x+dispX,y+dispY,t+dispT,params) 
                       && !check_is_occluded(occVolume, x+dispX, y+dispY, t+dispT)
                       && !check_already_used_patch(dispField, x, y, t, dispX, dispY, dispT)){
                        curVal = ssd_patch_measure(departVolume, arrivalVolume, dispField,occVolume,x,y,t,x+dispX,y+dispY,t+dispT,minVal,params);
                        if(curVal >= 0 && curVal < minVal){
                            minVal = curVal;
                            copy_pixel_values_nTuple_volume(dispField,dispField, x + checkDispX, y, t, x, y, t);
                            dispField->set_value(x,y,t,3,minVal);
                            wasModified = true;
                        }
                    }
                }

                if(checkDispY != 0){
                    dispX = (int)dispField->get_value(x, y + checkDispY, t, 0);
                    dispY = (int)dispField->get_value(x, y + checkDispY, t, 1);
                    dispT = (int)dispField->get_value(x, y + checkDispY, t, 2); 
                    if (check_in_inner_boundaries(arrivalVolume,x+dispX,y+dispY,t+dispT,params) 
                        && !check_is_occluded(occVolume, x+dispX, y+dispY, t+dispT) 
                        && !check_already_used_patch(dispField, x, y, t, dispX, dispY, dispT)){
                        curVal = ssd_patch_measure(departVolume, arrivalVolume, dispField,occVolume,x,y,t,x+dispX,y+dispY,t+dispT,minVal,params);
                        if(curVal >= 0 && curVal < minVal){
                            minVal = curVal;
                            copy_pixel_values_nTuple_volume(dispField,dispField, x, y + checkDispY, t, x, y, t);
                            dispField->set_value(x,y,t,3,minVal);
                            wasModified = true;
                        }
                    }
                }

                if(checkDispT != 0){
                    dispX = (int)dispField->get_value(x, y, t + checkDispT, 0);
                    dispY = (int)dispField->get_value(x, y, t + checkDispT, 1);
                    dispT = (int)dispField->get_value(x, y, t + checkDispT, 2); 
                    if (check_in_inner_boundaries(arrivalVolume,x+dispX,y+dispY,t+dispT,params) 
                        && !check_is_occluded(occVolume, x+dispX, y+dispY, t+dispT)
                        && !check_already_used_patch(dispField, x, y, t, dispX, dispY, dispT)){
                        curVal = ssd_patch_measure(departVolume, arrivalVolume, dispField,occVolume,x,y,t,x+dispX,y+dispY,t+dispT,minVal,params);
                        if(curVal >= 0 && curVal < minVal){
                            minVal = curVal;
                            copy_pixel_values_nTuple_volume(dispField,dispField, x, y, t + checkDispT, x, y, t);
                            dispField->set_value(x,y,t,3,minVal);
                            wasModified = true;
                        }
                    }
                }
                
                if(wasModified)
                    nbModified++;
                
                // Loop body end                
                if(goForward && x >= ((dispField->xSize) - 1)) xHasEnded = true;
                else if(!goForward && x <= 0) xHasEnded = true;
                if(goForward) x++;
                else x--;
            }
            if(goForward && y >= ((dispField->ySize) - 1)) yHasEnded = true;
            else if(!goForward && y <= 0) yHasEnded = true;
            if(goForward) y++;
            else y--;
        }
        if(goForward && t >= ((dispField->tSize) - 1)) tHasEnded = true;
        else if(!goForward && t <= 0) tHasEnded = true;
        if(goForward) t++;
        else t--;
    }
    
    return(nbModified);
}

//this function returns the minimum error of the patch differences around the pixel at (i,j,k)
//and returns the index of the best position in correctInd :
// -1 : none are correct
// 0 : left/right
// 1 : upper/lower
// 2 : before/after
template <class T>
float get_min_correct_error(nTupleVolume<T> *dispField,nTupleVolume<T> *departVol,nTupleVolume<T> *arrivalVol, nTupleVolume<T> *occVol,
							int x, int y, int t, bool goForward, int *correctInd, float *minVector, float minError,
                            const parameterStruct *params)
{
	float minVal = minError;
    int dispX, dispY, dispT;

	*correctInd = -1;	//initialise the correctInd vector to -1
    for (int i=0;i<NDIMS;i++)
		minVector[i] = -1;
    
    int checkDispX = 0, checkDispY = 0, checkDispT = 0;
    if(goForward){
        if(x != 0) checkDispX = -1;
        if(y != 0) checkDispY = -1;
        if(t != 0) checkDispT = -1;
    }
    else{
        if(x != ((departVol->xSize)-1)) checkDispX = 1;
        if(y != ((departVol->ySize)-1)) checkDispY = 1;
        if(t != ((departVol->tSize)-1)) checkDispT = 1;
    }
    
    if(checkDispX != 0){        
        dispX = (int)dispField->get_value(x + checkDispX, y, t, 0);
        dispY = (int)dispField->get_value(x + checkDispX, y, t, 1);
        dispT = (int)dispField->get_value(x + checkDispX, y, t, 2);
        if(check_in_inner_boundaries(arrivalVol,x+dispX,y+dispY,t+dispT,params) 
           && !check_is_occluded(occVol, x+dispX, y+dispY, t+dispT)
           && !check_already_used_patch(dispField, x, y, t, dispX, dispY, dispT))
           minVector[0] = ssd_patch_measure(departVol, arrivalVol, dispField,occVol,x,y,t,x+dispX,y+dispY,t+dispT,minError,params);
    }
    
    if(checkDispY != 0){
        dispX = (int)dispField->get_value(x, y + checkDispY, t, 0);
        dispY = (int)dispField->get_value(x, y + checkDispY, t, 1);
        dispT = (int)dispField->get_value(x, y + checkDispY, t, 2); 
        if (check_in_inner_boundaries(arrivalVol,x+dispX,y+dispY,t+dispT,params) 
            && !check_is_occluded(occVol, x+dispX, y+dispY, t+dispT) 
            && !check_already_used_patch(dispField, x, y, t, dispX, dispY, dispT))
            minVector[1] = ssd_patch_measure(departVol, arrivalVol, dispField,occVol,x,y,t,x+dispX,y+dispY,t+dispT,minError,params);
    }
    
    if(checkDispT != 0){
        dispX = (int)dispField->get_value(x, y, t + checkDispT, 0);
        dispY = (int)dispField->get_value(x, y, t + checkDispT, 1);
        dispT = (int)dispField->get_value(x, y, t + checkDispT, 2); 
        if (check_in_inner_boundaries(arrivalVol,x+dispX,y+dispY,t+dispT,params) 
            && !check_is_occluded(occVol, x+dispX, y+dispY, t+dispT)
            && !check_already_used_patch(dispField, x, y, t, dispX, dispY, dispT))
            minVector[2] = ssd_patch_measure(departVol, arrivalVol, dispField,occVol,x,y,t,x+dispX,y+dispY,t+dispT,minError,params);
    }
    
    for (int i=0;i<NDIMS;i++){
        if (minVector[i] == -1)
            continue;

        if ( minVector[i] < minVal)
        {
            minVal = minVector[i];
            *correctInd = i;
        }
    }

    //if none of the displacements are valid
	if((*correctInd) == -1)
		minVal = -1;
    
	return(minVal);
}


/*
		for (i=0;i<NDIMS;i++)
		{
            if (minVector[i] == -1)
                continue;
            
            if ( minVector[i] < minVal)
			{
				switch (i){
					case 0 :
						coordDispX = (int)dispField->get_value(max_int(x-1,0),y,t,0);
						coordDispY = (int)dispField->get_value(max_int(x-1,0),y,t,1);
						coordDispT = (int)dispField->get_value(max_int(x-1,0),y,t,2);
// // 						if ( check_in_inner_boundaries(arrivalVol,x+coordDispX,y+coordDispY,t+coordDispT,params) &&
// // 							(!check_is_occluded(occVol, x+coordDispX, y+coordDispY, t+coordDispT)) &&
// // 							 check_min_shift_distance(coordDispX, coordDispY, coordDispT,params))
// // 						{
							minVal = minVector[i];
							*correctInd = 0;
// // 						}
						break;
					case 1 :
						coordDispX = (int)dispField->get_value(x,max_int(y-1,0),t,0);
						coordDispY = (int)dispField->get_value(x,max_int(y-1,0),t,1);
						coordDispT = (int)dispField->get_value(x,max_int(y-1,0),t,2);
// // 						if ( check_in_inner_boundaries(arrivalVol,x+coordDispX,y+coordDispY,t+coordDispT,params) && 
// // 							(!check_is_occluded(occVol, x+coordDispX, y+coordDispY, t+coordDispT)) && 
// // 							 check_min_shift_distance(coordDispX, coordDispY, coordDispT,params))
// // 						{
							minVal = minVector[i];
							*correctInd = 1;
// // 						}
						break;
					case 2 :
						coordDispX = (int)dispField->get_value(x,y,max_int(t-1,0),0);
						coordDispY = (int)dispField->get_value(x,y,max_int(t-1,0),1);
						coordDispT = (int)dispField->get_value(x,y,max_int(t-1,0),2);
// // 						if ( check_in_inner_boundaries(arrivalVol,x+coordDispX,y+coordDispY,t+coordDispT,params) && 
// // 							(!check_is_occluded(occVol, x+coordDispX, y+coordDispY, t+coordDispT)) &&
// // 							check_min_shift_distance(coordDispX, coordDispY, coordDispT,params))
// // 						{
							minVal = minVector[i];
							*correctInd = 2;
// 						}
						break;
					default :
						MY_PRINTF("Error in get_min_correct_error. The index i : %d is above 3.\n",i);
				}
			}
		}
*/
               /*
                switch (i){
					case 0 :
						coordDispX = (int)dispField->get_value(min_int(x+1,((int)dispField->xSize)-1),y,t,0);
						coordDispY = (int)dispField->get_value(min_int(x+1,((int)dispField->xSize)-1),y,t,1);
						coordDispT = (int)dispField->get_value(min_int(x+1,((int)dispField->xSize)-1),y,t,2);
 						if ( check_in_inner_boundaries(arrivalVol,x+coordDispX,y+coordDispY,t+coordDispT,params) && 
 							(!check_is_occluded(occVol, x+coordDispX, y+coordDispY, t+coordDispT)) &&
 							check_min_shift_distance(coordDispX, coordDispY, coordDispT,params))
 						{
                            minVal = minVector[i];
                            *correctInd = 0;
 						}
						break;
					case 1 :
                        minVal = minVector[i];
                        *correctInd = 1;
//						coordDispX = (int)dispField->get_value(x,min_int(y+1,((int)dispField->ySize)-1),t,0);
//						coordDispY = (int)dispField->get_value(x,min_int(y+1,((int)dispField->ySize)-1),t,1);
//						coordDispT = (int)dispField->get_value(x,min_int(y+1,((int)dispField->ySize)-1),t,2);
// 						if ( check_in_inner_boundaries(arrivalVol,x+coordDispX,y+coordDispY,t+coordDispT,params) && 
// 							(!check_is_occluded(occVol, x+coordDispX, y+coordDispY, t+coordDispT)) &&
// 							check_min_shift_distance(coordDispX, coordDispY, coordDispT,params))
// 						{
// 						}
						break;
					case 2 :							
                        minVal = minVector[i];
						*correctInd = 2;
//						coordDispX = (int)dispField->get_value(x,y,min_int(t+1,((int)dispField->tSize)-1),0);
//						coordDispY = (int)dispField->get_value(x,y,min_int(t+1,((int)dispField->tSize)-1),1);
//						coordDispT = (int)dispField->get_value(x,y,min_int(t+1,((int)dispField->tSize)-1),2);
// 						if ( check_in_inner_boundaries(arrivalVol,x+coordDispX,y+coordDispY,t+coordDispT,params) && 
// 							(!check_is_occluded(occVol, x+coordDispX, y+coordDispY, t+coordDispT)) &&
// 							check_min_shift_distance(coordDispX, coordDispY, coordDispT,params))
// 						{
// 						}
						break;
					default :
						MY_PRINTF("Error in get_min_correct_error. The index i : %d is above 3.\n",i);
				}
                */
