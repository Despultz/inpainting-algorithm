// These are the MATLAB lines used to extract the data from the running of MATLAB.

// The following batches of commands must be after the construction of occVolPatchMatch for each peel iteration.

matlabDetOcc_Search_P1 = fopen('matlabDetOcc_Search_P1.bin', 'W');
fwrite(matlabDetOcc_Search_P1, permute(squeeze(occVolPatchMatch(:,:,:)),[2 1 3]),'single');
fclose(matlabDetOcc_Search_P1);

matlabDetOcc_Search_P2 = fopen('matlabDetOcc_Search_P2.bin', 'W');
fwrite(matlabDetOcc_Search_P2, permute(squeeze(occVolPatchMatch(:,:,:)),[2 1 3]),'single');
fclose(matlabDetOcc_Search_P2);

matlabDetOcc_Search_P3 = fopen('matlabDetOcc_Search_P3.bin', 'W');
fwrite(matlabDetOcc_Search_P3, permute(squeeze(occVolPatchMatch(:,:,:)),[2 1 3]),'single');
fclose(matlabDetOcc_Search_P3);

// The following batches of commande should be done after reconstructing the occVolBorder for each peel iteration.

matlabDetOcc_Recon_P1 = fopen('matlabDetOcc_Recon_P1.bin', 'W');
fwrite(matlabDetOcc_Recon_P1, permute(squeeze(occVolBorder(:,:,:)),[2 1 3]),'single');
fclose(matlabDetOcc_Recon_P1);

matlabDetOcc_Recon_P2 = fopen('matlabDetOcc_Recon_P2.bin', 'W');
fwrite(matlabDetOcc_Recon_P2, permute(squeeze(occVolBorder(:,:,:)),[2 1 3]),'single');
fclose(matlabDetOcc_Recon_P2);

matlabDetOcc_Recon_P3 = fopen('matlabDetOcc_Recon_P3.bin', 'W');
fwrite(matlabDetOcc_Recon_P3, permute(squeeze(occVolBorder(:,:,:)),[2 1 3]),'single');
fclose(matlabDetOcc_Recon_P3);