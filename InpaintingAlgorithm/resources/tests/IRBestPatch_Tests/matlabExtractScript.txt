// These are the MATLAB lines used to extract the data from the running of MATLAB.

// The following 7 batches of commands must be before the reconstruction.
beforeReconRed = fopen('beforeReconRed.bin', 'W');
fwrite(beforeReconRed, permute(squeeze(imgVol(1,:,:,:)),[2 1 3]),'single');
fclose(beforeReconRed);

beforeReconGreen = fopen('beforeReconGreen.bin', 'W');
fwrite(beforeReconGreen, permute(squeeze(imgVol(2,:,:,:)),[2 1 3]),'single');
fclose(beforeReconGreen);

beforeReconBlue = fopen('beforeReconBlue.bin', 'W');
fwrite(beforeReconBlue, permute(squeeze(imgVol(3,:,:,:)),[2 1 3]),'single');
fclose(beforeReconBlue);

beforeReconDispX = fopen('beforeReconDispX.bin', 'W');
fwrite(beforeReconDispX, permute(squeeze(shiftVol(1,:,:,:)),[2 1 3]),'int32');
fclose(beforeReconDispX);

beforeReconDispY = fopen('beforeReconDispY.bin', 'W');
fwrite(beforeReconDispY, permute(squeeze(shiftVol(2,:,:,:)),[2 1 3]),'int32');
fclose(beforeReconDispY);

beforeReconDispZ = fopen('beforeReconDispZ.bin', 'W');
fwrite(beforeReconDispZ, permute(squeeze(shiftVol(3,:,:,:)),[2 1 3]),'int32');
fclose(beforeReconDispZ);

beforeReconDispA = fopen('beforeReconDispA.bin', 'W');
fwrite(beforeReconDispA, permute(squeeze(shiftVol(4,:,:,:)),[2 1 3]),'single');
fclose(beforeReconDispA);

// The following 3 batches of commands must be after the reconstruction.
afterReconRed = fopen('afterReconRed.bin', 'W');
fwrite(afterReconRed, permute(squeeze(imgVol(1,:,:,:)),[2 1 3]),'single');
fclose(afterReconRed);

afterReconGreen = fopen('afterReconGreen.bin', 'W');
fwrite(afterReconGreen, permute(squeeze(imgVol(2,:,:,:)),[2 1 3]),'single');
fclose(afterReconGreen);

afterReconBlue = fopen('afterReconBlue.bin', 'W');
fwrite(afterReconBlue, permute(squeeze(imgVol(3,:,:,:)),[2 1 3]),'single');
fclose(afterReconBlue);