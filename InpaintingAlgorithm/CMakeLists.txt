cmake_minimum_required(VERSION 3.3)
project(inpaintingAlgorithmRun)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++0x")

set(MAIN_SOURCE_FILES main.cpp)
add_executable(inpaintingAlgorithmRun ${MAIN_SOURCE_FILES})

add_subdirectory(algorithm)
add_subdirectory(tests)

include_directories(algorithm)

target_link_libraries(inpaintingAlgorithmRun inpaintingAlgorithm)

# Copies the contents of the resources dir from SOURCE to the BINARY folder.
add_custom_command(TARGET inpaintingAlgorithmRun POST_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy_directory
                   ${CMAKE_SOURCE_DIR}/resources $<TARGET_FILE_DIR:inpaintingAlgorithmRun>/resources)