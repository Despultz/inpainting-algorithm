//
// Created by Jonathan on 4/26/2016.
//

#ifndef INPAINTINGALGORITHMRUN_VOLUMEITERATORTESTENVIRONMENT_H
#define INPAINTINGALGORITHMRUN_VOLUMEITERATORTESTENVIRONMENT_H

#include "gtest/gtest.h"
#include "../../../algorithm/standardTools/PaddedMatrixDim.h"
#include "../../../algorithm/standardTools/PaddedMatrix.h"

class PaddedMatrixItTestEnvironment : public ::testing::Environment{
private:
    PaddedMatrixDim dim;
    PaddedMatrix<int> data;

public:
    PaddedMatrixItTestEnvironment() :
            dim(PaddedMatrixDim(10, 10, 10, 2, 2, 2)),
            data(PaddedMatrix<int>(&dim)),
            ::testing::Environment()
    {
    }

    virtual void SetUp(){
        InitializeData();
    };

    PaddedMatrixDim * const GetDim(){
        return &dim;
    }

    int* GetData(){
        return data.GetData();
    }

private:
    void InitializeData(){
        int dataValue = 0;

        for(unsigned int a = 0; a < dim.GetSizeZ(); ++a){
            for(unsigned int b = 0; b < dim.GetSizeY(); ++b){
                for(unsigned int c = 0; c < dim.GetSizeZ(); ++c){
                    data.Set(c, b, a, dataValue++);
                }
            }
        }
    }
};

#endif //INPAINTINGALGORITHMRUN_VOLUMEITERATORTESTENVIRONMENT_H
