//
// Created by Jonathan on 04/02/2016.
//

#include "gtest/gtest.h"
#include "../../../algorithm/standardTools/ImageManipulator.h"
#include "../../../algorithm/inpaintingTools/InpaintingOpenCVIO.h"

TEST(ImageManipulator_GaussianFilter, test_eq_3x3){
    int size = 3;
    double * gaussianFilter = ImageManipulator::GaussianFilter(1.5, size, size);

    double expectedCorner = 0.094741658210174703;
    double expectedMiddle = 0.11831801270312062;
    double expectedCenter = 0.14776131634681883;

    ASSERT_DOUBLE_EQ(expectedCorner, gaussianFilter[0]);
    ASSERT_DOUBLE_EQ(expectedCorner, gaussianFilter[2]);
    ASSERT_DOUBLE_EQ(expectedCorner, gaussianFilter[2 * size]);
    ASSERT_DOUBLE_EQ(expectedCorner, gaussianFilter[2 * size + 2]);

    ASSERT_DOUBLE_EQ(expectedMiddle, gaussianFilter[1]);
    ASSERT_DOUBLE_EQ(expectedMiddle, gaussianFilter[size]);
    ASSERT_DOUBLE_EQ(expectedMiddle, gaussianFilter[size + 2]);
    ASSERT_DOUBLE_EQ(expectedMiddle, gaussianFilter[2 * size + 1]);

    ASSERT_DOUBLE_EQ(expectedCenter, gaussianFilter[size + 1]);

    delete [] gaussianFilter;
}

TEST(ImageManipulator_Grayscale, test_MATLAB_one_frame){
    ::std::string const testResDir = "../../resources/tests/ImageManipulator_Tests/grayscale/";

    ::std::string const matlabRedFile = testResDir + "matlabRedForGrayF1.bin";
    ::std::string const matlabGreenFile = testResDir + "matlabGreenForGrayF1.bin";
    ::std::string const matlabBlueFile = testResDir + "matlabBlueForGrayF1.bin";

    ::std::string const matlabGrayFile = testResDir + "matlabGrayscaleF1.bin";

    PaddedMatrixDim const frameDim = PaddedMatrixDim(264, 68, 1);

    // Load the binary files into matrices.
    PaddedMatrix<float> matlabRedMatrix = PaddedMatrix<float>(&frameDim);
    PaddedMatrix<float> matlabGreenMatrix = PaddedMatrix<float>(&frameDim);
    PaddedMatrix<float> matlabBlueMatrix = PaddedMatrix<float>(&frameDim);
    PaddedMatrix<float> matlabGrayMatrix = PaddedMatrix<float>(&frameDim);

    InpaintingOpenCVIO openCVIO = InpaintingOpenCVIO();
    openCVIO.LoadMatrixFromBin(matlabRedFile, &matlabRedMatrix);
    openCVIO.LoadMatrixFromBin(matlabGreenFile, &matlabGreenMatrix);
    openCVIO.LoadMatrixFromBin(matlabBlueFile, &matlabBlueMatrix);
    openCVIO.LoadMatrixFromBin(matlabGrayFile, &matlabGrayMatrix);

    // Execute the tested code
    PaddedMatrix<float> ourGrayMatrix = ImageManipulator::GrayScaleFromRGB(&matlabRedMatrix, &matlabGreenMatrix, &matlabBlueMatrix);

    // Test the values.
    PaddedMatrixIt it = PaddedMatrixIt(&frameDim); // Can use the frame dim to iterate the video, since same X and Y
    do{
        float valueExpected = matlabGrayMatrix.GetData()[it.GetPosition()];
        float valueTested = ourGrayMatrix.GetData()[it.GetPosition()];

        ASSERT_FLOAT_EQ(valueTested, valueExpected);
    }while(it.Next());
}

TEST(ImageManipulator_Caselles, test_MATLAB_full_video) {
    // Resources
    ::std::string const testResDir = "../../resources/tests/ImageManipulator_Tests/caselles/";

    ::std::string const gradXLevel1 = testResDir + "matlabGradXL1.bin";
    ::std::string const gradYLevel1 = testResDir + "matlabGradYL1.bin";

    ::std::string const videoFileName = "../../resources/beach_umbrella.avi";
    ::std::string const occFileName = "../../resources/beach_umbrella_occlusion.png";

    PaddedMatrixDim dim = PaddedMatrixDim(264, 68, 98, 2, 2, 2);

    // Calculate our results
    PaddedMatrix<float> redMatrix = PaddedMatrix<float>(&dim);
    PaddedMatrix<float> greenMatrix = PaddedMatrix<float>(&dim);
    PaddedMatrix<float> blueMatrix = PaddedMatrix<float>(&dim);
    PaddedMatrix<float> occMatrix = PaddedMatrix<float>(&dim);
    PaddedMatrix<float> gradXMatrix = PaddedMatrix<float>(&dim);
    PaddedMatrix<float> gradYMatrix = PaddedMatrix<float>(&dim);

    InpaintingOpenCVIO openCVIO = InpaintingOpenCVIO();
    openCVIO.LoadRGBMatrixFromVideo(videoFileName, &redMatrix, &greenMatrix, &blueMatrix);
    openCVIO.LoadOcclusionMatrixFromImage(occFileName, &occMatrix);

    ImageManipulator::CalculateCasellesDescriptors(&dim, &redMatrix, &greenMatrix, &blueMatrix, &occMatrix, &gradXMatrix, &gradYMatrix, (int)exp2f(3));

    // Load the MATLAB results
    PaddedMatrix<float> matlabGradXMatrix = PaddedMatrix<float>(&dim);
    PaddedMatrix<float> matlabGradYMatrix = PaddedMatrix<float>(&dim);

    openCVIO.LoadMatrixFromBin(gradXLevel1, &matlabGradXMatrix);
    openCVIO.LoadMatrixFromBin(gradYLevel1, &matlabGradYMatrix);

    // Test
    float const maxRelDiff = 0.01f;

    float * matlabGradXData = matlabGradXMatrix.GetData();
    float * matlabGradYData = matlabGradYMatrix.GetData();

    float * testGradXData = gradXMatrix.GetData();
    float * testGradYData = gradYMatrix.GetData();

    float valAvgDiffX = 0;
    float valAvgDiffY = 0;
    int valCount = 0;

    PaddedMatrixIt matIt = PaddedMatrixIt(&dim);
    do{
        // Test the X gradients
        float matlabGradX = matlabGradXData[matIt.GetPosition()];
        float ourGradX = testGradXData[matIt.GetPosition()];

        float diffX = fabsf(ourGradX - matlabGradX);
        float maxDiffX = matlabGradX * maxRelDiff;

        ASSERT_NEAR(ourGradX, matlabGradX, 0.01 * matlabGradX);

        // Test the Y gradients
        float matlabGradY = matlabGradYData[matIt.GetPosition()];
        float ourGradY = testGradYData[matIt.GetPosition()];

        float diffY = fabsf(ourGradY - matlabGradY);
        float maxDiffY = matlabGradY * maxRelDiff;

        // ASSERT_NEAR(ourGradY, matlabGradY, 0.1f * matlabGradY);

        if(matlabGradX != 0 && matlabGradY != 0){
            valAvgDiffX += ::std::fabs(ourGradX / matlabGradX - 1.0f);
            valAvgDiffY += ::std::fabs(ourGradY / matlabGradY - 1.0f);
            valCount++;
        }
    }while(matIt.Next());

    ASSERT_LT(valAvgDiffX / valCount, 0.01f);
    ASSERT_LT(valAvgDiffY / valCount, 0.01f);
}