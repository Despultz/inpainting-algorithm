//
// Created by Jonathan on 4/26/2016.
//

#include "gtest/gtest.h"
#include "../../../algorithm/standardTools/PaddedMatrixRevIt.h"
#include "PaddedMatrixItTestEnvironment.h"

PaddedMatrixItTestEnvironment* const iteratorEnv = (PaddedMatrixItTestEnvironment* const)::testing::AddGlobalTestEnvironment(new PaddedMatrixItTestEnvironment);

TEST(PaddedMatrixRevIt_Test, Reverse_Iterate){
    // Setup
    PaddedMatrixRevIt iterator = PaddedMatrixRevIt(iteratorEnv->GetDim());
    int dataValue = iteratorEnv->GetDim()->GetVolume() - 1;

    //Test
    do{
        ASSERT_EQ(dataValue--, iteratorEnv->GetData()[iterator.GetPosition()]);
    }while(iterator.Next());

    ASSERT_EQ(-1, dataValue); // Verify we got to the end.
}

TEST(PaddedMatrixRevIt_Test, Reverse_Iterate_LeapX2){
    // Setup
    PaddedMatrixRevIt iterator = PaddedMatrixRevIt(iteratorEnv->GetDim());
    int dataValue = iteratorEnv->GetDim()->GetVolume() - 1;

    //Test
    do{
        ASSERT_EQ(dataValue, iteratorEnv->GetData()[iterator.GetPosition()]);
        dataValue -= 2;
    }while(iterator.Next(2, 1));

    ASSERT_EQ(-1, dataValue); // Verify we got to the end.
}

TEST(PaddedMatrixRevIt_Test, Reverse_Iterate_LeapY2){
    // Setup
    PaddedMatrixRevIt iterator = PaddedMatrixRevIt(iteratorEnv->GetDim());
    int dataValue = iteratorEnv->GetDim()->GetVolume() - 1;

    //Test
    do{
        ASSERT_EQ(dataValue, iteratorEnv->GetData()[iterator.GetPosition()]);

        if(dataValue % iteratorEnv->GetDim()->GetSizeX() == 0) dataValue -= iteratorEnv->GetDim()->GetSizeX();
        dataValue -= 1;

    }while(iterator.Next(1, 2));

    ASSERT_EQ(-1, dataValue); // Verify we got to the end.
}

TEST(PaddedMatrixRevIt_Test, Reverse_Iterate_LeapX2Y2){
    // Setup
    PaddedMatrixRevIt iterator = PaddedMatrixRevIt(iteratorEnv->GetDim());
    int dataValue = iteratorEnv->GetDim()->GetVolume() - 1;

    //Test
    do{
        ASSERT_EQ(dataValue, iteratorEnv->GetData()[iterator.GetPosition()]);

        if(dataValue % iteratorEnv->GetDim()->GetSizeX() == 1) dataValue -= iteratorEnv->GetDim()->GetSizeX(); // == 1 since we skip last value and minus 2 X...
        dataValue -= 2;

    }while(iterator.Next(2, 2));

    ASSERT_EQ(-1, dataValue); // Verify we got to the end.
}

TEST(PaddedMatrixRevIt_Test, Reverse_Iterate_XYZ) {
    int dimX = 7, dimY = 6, dimZ = 4;
    PaddedMatrixDim dim = PaddedMatrixDim(dimX, dimY, dimZ);
    PaddedMatrixRevIt revIt = PaddedMatrixRevIt(&dim);

    for(int z = dimZ - 1; z >= 0; z--)
        for(int y = dimY - 1; y >= 0; y--)
            for(int x = dimX - 1; x >= 0; x--){
                ASSERT_EQ(x, revIt.GetCurX());
                ASSERT_EQ(y, revIt.GetCurY());
                ASSERT_EQ(z, revIt.GetCurZ());
                revIt.Next();
            }
}

TEST(PaddedMatrixRevIt_Test, Iterate_DefaultIndex) {
    int dimX = 7, dimY = 6, dimZ = 4;
    PaddedMatrixDim dim = PaddedMatrixDim(dimX, dimY, dimZ);
    PaddedMatrixRevIt revIt = PaddedMatrixRevIt(&dim);

    for(int i = dimX * dimY * dimZ - 1; i >= 0; i--){
        ASSERT_EQ(i, revIt.GetIndex());
        revIt.Next();
    }
}

TEST(PaddedMatrixRevIt_Test, Iterate_Index) {
    int dimX = 7, dimY = 6, dimZ = 4;
    PaddedMatrixDim dim = PaddedMatrixDim(dimX, dimY, dimZ);
    PaddedMatrixRevIt revIt = PaddedMatrixRevIt(&dim, dimX * dimY * dimZ - 1, 0);

    for(int i = dimX * dimY * dimZ - 1; i >= 0; i--){
        ASSERT_EQ(i, revIt.GetIndex());
        revIt.Next();
    }
}