//
// Created by Jonathan on 15/03/2016.
//

#include "gtest/gtest.h"
#include "../../../algorithm/standardTools/PaddedMatrixDim.h"
#include "../../../algorithm/standardTools/PaddedMatrix.h"
#include "PaddedMatrixItTestEnvironment.h"

PaddedMatrixItTestEnvironment* const iteratorEnv = (PaddedMatrixItTestEnvironment* const)::testing::AddGlobalTestEnvironment(new PaddedMatrixItTestEnvironment);

TEST(PaddedMatrixIt_Test, Forward_Iterate){
    // Setup
    PaddedMatrixIt iterator = PaddedMatrixIt(iteratorEnv->GetDim());
    int dataValue = 0;

    //Test
    do{
        ASSERT_EQ(dataValue, iteratorEnv->GetData()[iterator.GetPosition()]);

        dataValue += 1;
    }while(iterator.Next());

    ASSERT_EQ(dataValue, iteratorEnv->GetDim()->GetVolume()); // We made it to the end
}

TEST(PaddedMatrixIt_Test, Forward_Iterate_LeapX2){
    // Setup
    PaddedMatrixIt iterator = PaddedMatrixIt(iteratorEnv->GetDim());
    int dataValue = 0;

    //Test
    do{
        ASSERT_EQ(dataValue, iteratorEnv->GetData()[iterator.GetPosition()]);

        dataValue += 2;
    }while(iterator.Next(2, 1));

    ASSERT_EQ(dataValue, iteratorEnv->GetDim()->GetVolume()); // We made it to the end
}

TEST(PaddedMatrixIt_Test, Forward_Iterate_LeapY2){
    // Setup
    PaddedMatrixIt iterator = PaddedMatrixIt(iteratorEnv->GetDim());
    int dataValue = 0;

    //Test
    do{
        ASSERT_EQ(dataValue, iteratorEnv->GetData()[iterator.GetPosition()]);

        dataValue += 1;
        if(dataValue % iteratorEnv->GetDim()->GetSizeX() == 0) dataValue += iteratorEnv->GetDim()->GetSizeX();
    }while(iterator.Next(1, 2));

    ASSERT_EQ(dataValue, iteratorEnv->GetDim()->GetVolume()); // We made it to the end
}

TEST(PaddedMatrixIt_Test, Forward_Iterate_LeapX2Y2){
    // Setup
    PaddedMatrixIt iterator = PaddedMatrixIt(iteratorEnv->GetDim());
    int dataValue = 0;

    //Test
    do{
        ASSERT_EQ(dataValue, iteratorEnv->GetData()[iterator.GetPosition()]);

        dataValue += 2;
        if(dataValue % iteratorEnv->GetDim()->GetSizeX() == 0) dataValue += iteratorEnv->GetDim()->GetSizeX();
    }while(iterator.Next(2, 2));

    ASSERT_EQ(dataValue, iteratorEnv->GetDim()->GetVolume()); // We made it to the end
}

TEST(PaddedMatrixIt_Test, Iterate_XYZ) {
    int dimX = 7, dimY = 6, dimZ = 4;
    PaddedMatrixDim dim = PaddedMatrixDim(dimX, dimY, dimZ);
    PaddedMatrixIt it = PaddedMatrixIt(&dim);

    for(int z = 0; z < dimZ; z++)
        for(int y = 0; y < dimY; y++)
            for(int x = 0; x < dimX; x++){
                ASSERT_EQ(x, it.GetCurX());
                ASSERT_EQ(y, it.GetCurY());
                ASSERT_EQ(z, it.GetCurZ());
                it.Next();
            }
}

TEST(PaddedMatrixIt_Test, Iterate_DefaultIndex) {
    int dimX = 7, dimY = 6, dimZ = 4;
    PaddedMatrixDim dim = PaddedMatrixDim(dimX, dimY, dimZ);
    PaddedMatrixIt it = PaddedMatrixIt(&dim);

    for(int i = 0; i < dimX * dimY * dimZ; i++){
        ASSERT_EQ(i, it.GetIndex());
        it.Next();
    }
}

TEST(PaddedMatrixIt_Test, Iterate_Index) {
    int dimX = 7, dimY = 6, dimZ = 4;
    PaddedMatrixDim dim = PaddedMatrixDim(dimX, dimY, dimZ);
    PaddedMatrixIt it = PaddedMatrixIt(&dim, 0, dimX * dimY * dimZ);

    for(int i = 0; i < dimX * dimY * dimZ; i++){
        ASSERT_EQ(i, it.GetIndex());
        it.Next();
    }
}