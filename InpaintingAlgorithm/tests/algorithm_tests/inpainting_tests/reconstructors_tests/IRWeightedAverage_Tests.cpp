//
// Created by Jonathan on 5/20/2016.
//

#include "gtest/gtest.h"
#include "../../../../algorithm/standardTools/PaddedMatrixDim.h"
#include "../../../../algorithm/standardTools/PaddedMatrix.h"
#include "../../../../algorithm/inpaintingTools/InpaintingOpenCVIO.h"
#include "../../../../algorithm/inpainting/dataSources/IDSConcrete.h"
#include "../../../../algorithm/inpainting/dataSources/decorators/IDSDDetailedOcclusion.h"
#include "../../../../algorithm/inpainting/reconstructors/IRWeightedAverage.h"
#include "../../../../algorithm/inpainting/dataSources/decorators/IDSDDetailedOcclusionPeeling.h"

void TestWeightedAverageReconstruction(bool const hasInvalid){
    // Size of the video samples (binary file)
    int const sizeX = 66, sizeY = 17, sizeZ = 98;
    int const patchSizeX = 5, patchSizeY = 5, patchSizeZ = 5;
    float const sigmaColour = 0.75f;

    // Binary files
    ::std::string const testFilesDirectory = "../../resources/tests/IRWeightedAverage_Tests/";
    ::std::string const testFilesSubDirectory = testFilesDirectory + (hasInvalid ? "not_all_valid/" : "all_valid/");

    ::std::string const beforeReconRedFile = testFilesSubDirectory + "beforeReconRed.bin";
    ::std::string const beforeReconGreenFile = testFilesSubDirectory + "beforeReconGreen.bin";
    ::std::string const beforeReconBlueFile = testFilesSubDirectory + "beforeReconBlue.bin";
    ::std::string const beforeReconGradXFile = testFilesSubDirectory + "beforeReconGradX.bin";
    ::std::string const beforeReconGradYFile = testFilesSubDirectory + "beforeReconGradY.bin";
    ::std::string const beforeReconDispAFile = testFilesSubDirectory + "beforeReconDispA.bin";
    ::std::string const beforeReconDispXFile = testFilesSubDirectory + "beforeReconDispX.bin";
    ::std::string const beforeReconDispYFile = testFilesSubDirectory + "beforeReconDispY.bin";
    ::std::string const beforeReconDispZFile = testFilesSubDirectory + "beforeReconDispZ.bin";

    ::std::string const occlusionImageL2File = testFilesDirectory + "occlusionImageL2.png";

    ::std::string const afterReconRedFile = testFilesSubDirectory + "afterReconRed.bin";
    ::std::string const afterReconGreenFile = testFilesSubDirectory + "afterReconGreen.bin";
    ::std::string const afterReconBlueFile = testFilesSubDirectory + "afterReconBlue.bin";
    ::std::string const afterReconGradXFile = testFilesSubDirectory + "afterReconGradX.bin";
    ::std::string const afterReconGradYFile = testFilesSubDirectory + "afterReconGradY.bin";

    // Get the data ready (Load it into our data structures)
    PaddedMatrixDim * const matDim = new PaddedMatrixDim(sizeX, sizeY, sizeZ, patchSizeX/2, patchSizeY/2, patchSizeZ/2);
    PaddedMatrix<float> * const beforeReconRedMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const beforeReconGreenMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const beforeReconBlueMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const beforeReconGradXMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const beforeReconGradYMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const beforeReconDispAMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<int> * const beforeReconDispXMat = new PaddedMatrix<int>(matDim);
    PaddedMatrix<int> * const beforeReconDispYMat = new PaddedMatrix<int>(matDim);
    PaddedMatrix<int> * const beforeReconDispZMat = new PaddedMatrix<int>(matDim);
    PaddedMatrix<float> * const occMat = new PaddedMatrix<float>(matDim);

    PaddedMatrix<float> afterReconRedMat = PaddedMatrix<float>(matDim);
    PaddedMatrix<float> afterReconGreenMat = PaddedMatrix<float>(matDim);
    PaddedMatrix<float> afterReconBlueMat = PaddedMatrix<float>(matDim);
    PaddedMatrix<float> afterReconGradXMat = PaddedMatrix<float>(matDim);
    PaddedMatrix<float> afterReconGradYMat = PaddedMatrix<float>(matDim);

    InpaintingOpenCVIO openCVIO = InpaintingOpenCVIO();
    openCVIO.LoadMatrixFromBin(beforeReconRedFile, beforeReconRedMat);
    openCVIO.LoadMatrixFromBin(beforeReconGreenFile, beforeReconGreenMat);
    openCVIO.LoadMatrixFromBin(beforeReconBlueFile, beforeReconBlueMat);
    openCVIO.LoadMatrixFromBin(beforeReconGradXFile, beforeReconGradXMat);
    openCVIO.LoadMatrixFromBin(beforeReconGradYFile, beforeReconGradYMat);
    openCVIO.LoadMatrixFromBin(beforeReconDispAFile, beforeReconDispAMat);
    openCVIO.LoadMatrixFromBin(beforeReconDispXFile, beforeReconDispXMat, 1);
    openCVIO.LoadMatrixFromBin(beforeReconDispYFile, beforeReconDispYMat, 1);
    openCVIO.LoadMatrixFromBin(beforeReconDispZFile, beforeReconDispZMat, 1);
    openCVIO.LoadOcclusionMatrixFromImage(occlusionImageL2File, occMat);

    openCVIO.LoadMatrixFromBin(afterReconRedFile, &afterReconRedMat);
    openCVIO.LoadMatrixFromBin(afterReconGreenFile, &afterReconGreenMat);
    openCVIO.LoadMatrixFromBin(afterReconBlueFile, &afterReconBlueMat);
    openCVIO.LoadMatrixFromBin(afterReconGradXFile, &afterReconGradXMat);
    openCVIO.LoadMatrixFromBin(afterReconGradYFile, &afterReconGradYMat);

    // Prepare the input for the code
    IDSConcrete concreteIDS = IDSConcrete(matDim, beforeReconRedMat, beforeReconGreenMat, beforeReconBlueMat,
                                          occMat, beforeReconGradXMat, beforeReconGradYMat,
                                          beforeReconDispXMat, beforeReconDispYMat,
                                          beforeReconDispZMat, beforeReconDispAMat);

    // Run the tested code
    IRWeightedAverage testedIR = IRWeightedAverage(sigmaColour, patchSizeX, patchSizeY, patchSizeZ);

    if(hasInvalid){
        IDSDDetailedOcclusionPeeling detailedOccPeelIDS = IDSDDetailedOcclusionPeeling(&concreteIDS, patchSizeX, patchSizeY, patchSizeZ);
        testedIR.Reconstruct(&detailedOccPeelIDS, true);
    }
    else{
        IDSDDetailedOcclusion detailedOccIDS = IDSDDetailedOcclusion(&concreteIDS, patchSizeX, patchSizeY, patchSizeZ);
        testedIR.Reconstruct(&detailedOccIDS, false);
    }

    // Compare the output with MATLAB
    float const maxDist = 0.00001f; // There's more operations happening between two versions, and MATLAB uses doubles, so we loose some precision in C++ because we're using floats. (Hypothesis)

    float * matlabRed = afterReconRedMat.GetData();
    float * matlabGreen = afterReconGreenMat.GetData();
    float * matlabBlue = afterReconBlueMat.GetData();
    float * matlabGradX = afterReconGradXMat.GetData();
    float * matlabGradY = afterReconGradYMat.GetData();

    float * oursRed = concreteIDS.GetRed()->GetData();
    float * oursGreen = concreteIDS.GetGreen()->GetData();
    float * oursBlue = concreteIDS.GetBlue()->GetData();
    float * oursGradX = concreteIDS.GetGradX()->GetData();
    float * oursGradY = concreteIDS.GetGradY()->GetData();

    PaddedMatrixIt matIt = PaddedMatrixIt(matDim);
    do{
        int i = matIt.GetPosition();
        ASSERT_NEAR(matlabRed[i], oursRed[i], matlabRed[i] * maxDist);
        ASSERT_NEAR(matlabGreen[i], oursGreen[i], matlabGreen[i] * maxDist);
        ASSERT_NEAR(matlabBlue[i], oursBlue[i], matlabBlue[i] * maxDist);
        ASSERT_NEAR(matlabGradX[i], oursGradX[i], matlabGradX[i] * maxDist);
        ASSERT_NEAR(matlabGradY[i], oursGradY[i], matlabGradY[i] * maxDist);
    }while(matIt.Next());
}

TEST(IRWeightedAverage_Reconstruct, Compare_Matlab){
    TestWeightedAverageReconstruction(false);
}

TEST(IRWeightedAverage_Reconstruct, Compare_Matlab_HasInvalid) {
    TestWeightedAverageReconstruction(true);
}