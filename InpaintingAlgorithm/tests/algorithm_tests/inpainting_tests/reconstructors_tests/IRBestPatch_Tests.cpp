//
// Created by Jonathan on 6/1/2016.
//

#include "gtest/gtest.h"
#include "../../../../algorithm/standardTools/PaddedMatrix.h"
#include "../../../../algorithm/inpaintingTools/InpaintingOpenCVIO.h"
#include "../../../../algorithm/inpainting/dataSources/IDSConcrete.h"
#include "../../../../algorithm/inpainting/dataSources/decorators/IDSDDetailedOcclusion.h"
#include "../../../../algorithm/inpainting/reconstructors/IRBestPatch.h"

TEST(IRBestPatch_Reconstruct, Compare_Matlab) {
    // Size of the video samples (binary file)
    int const sizeX = 264, sizeY = 68, sizeZ = 98;
    int const patchSizeX = 5, patchSizeY = 5, patchSizeZ = 5;

    // Binary files
    ::std::string const testFilesDirectory = "../../resources/tests/IRBestPatch_Tests/";

    ::std::string const beforeReconRedFile = testFilesDirectory + "beforeReconRed.bin";
    ::std::string const beforeReconGreenFile = testFilesDirectory + "beforeReconGreen.bin";
    ::std::string const beforeReconBlueFile = testFilesDirectory + "beforeReconBlue.bin";
    ::std::string const beforeReconDispAFile = testFilesDirectory + "beforeReconDispA.bin";
    ::std::string const beforeReconDispXFile = testFilesDirectory + "beforeReconDispX.bin";
    ::std::string const beforeReconDispYFile = testFilesDirectory + "beforeReconDispY.bin";
    ::std::string const beforeReconDispZFile = testFilesDirectory + "beforeReconDispZ.bin";

    ::std::string const occFile = "../../resources/beach_umbrella_occlusion.png"; // Should move this file somewhere, copy it to tests.

    ::std::string const afterReconRedFile = testFilesDirectory + "afterReconRed.bin";
    ::std::string const afterReconGreenFile = testFilesDirectory + "afterReconGreen.bin";
    ::std::string const afterReconBlueFile = testFilesDirectory + "afterReconBlue.bin";

    // Get the data ready (Load it into our data structures)
    PaddedMatrixDim * const matDim = new PaddedMatrixDim(sizeX, sizeY, sizeZ, patchSizeX/2, patchSizeY/2, patchSizeZ/2);
    PaddedMatrix<float> * const beforeReconRedMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const beforeReconGreenMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const beforeReconBlueMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const beforeReconDispAMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<int> * const beforeReconDispXMat = new PaddedMatrix<int>(matDim);
    PaddedMatrix<int> * const beforeReconDispYMat = new PaddedMatrix<int>(matDim);
    PaddedMatrix<int> * const beforeReconDispZMat = new PaddedMatrix<int>(matDim);
    PaddedMatrix<float> * const occMat = new PaddedMatrix<float>(matDim);

    PaddedMatrix<float> afterReconRedMat = PaddedMatrix<float>(matDim);
    PaddedMatrix<float> afterReconGreenMat = PaddedMatrix<float>(matDim);
    PaddedMatrix<float> afterReconBlueMat = PaddedMatrix<float>(matDim);

    InpaintingOpenCVIO openCVIO = InpaintingOpenCVIO();
    openCVIO.LoadMatrixFromBin(beforeReconRedFile, beforeReconRedMat);
    openCVIO.LoadMatrixFromBin(beforeReconGreenFile, beforeReconGreenMat);
    openCVIO.LoadMatrixFromBin(beforeReconBlueFile, beforeReconBlueMat);
    openCVIO.LoadMatrixFromBin(beforeReconDispAFile, beforeReconDispAMat);
    openCVIO.LoadMatrixFromBin(beforeReconDispXFile, beforeReconDispXMat, 1);
    openCVIO.LoadMatrixFromBin(beforeReconDispYFile, beforeReconDispYMat, 1);
    openCVIO.LoadMatrixFromBin(beforeReconDispZFile, beforeReconDispZMat, 1);
    openCVIO.LoadOcclusionMatrixFromImage(occFile, occMat);

    openCVIO.LoadMatrixFromBin(afterReconRedFile, &afterReconRedMat);
    openCVIO.LoadMatrixFromBin(afterReconGreenFile, &afterReconGreenMat);
    openCVIO.LoadMatrixFromBin(afterReconBlueFile, &afterReconBlueMat);

    // Prepare the input for the code
    IDSConcrete concreteIDS = IDSConcrete(matDim, beforeReconRedMat, beforeReconGreenMat, beforeReconBlueMat,
                                          occMat, nullptr, nullptr, beforeReconDispXMat, beforeReconDispYMat,
                                          beforeReconDispZMat, beforeReconDispAMat);
    IDSDDetailedOcclusion detailedOccIDS = IDSDDetailedOcclusion(&concreteIDS, patchSizeX, patchSizeY, patchSizeZ);

    // Run the tested code
    IRBestPatch testedIR = IRBestPatch(patchSizeX, patchSizeY, patchSizeZ);
    testedIR.Reconstruct(&detailedOccIDS, false);

    // Compare the output with MATLAB
    PaddedMatrixIt matIt = PaddedMatrixIt(matDim);
    do{
        ASSERT_FLOAT_EQ(afterReconRedMat.GetData()[matIt.GetPosition()], concreteIDS.GetRed()->GetData()[matIt.GetPosition()]);
        ASSERT_FLOAT_EQ(afterReconGreenMat.GetData()[matIt.GetPosition()], concreteIDS.GetGreen()->GetData()[matIt.GetPosition()]);
        ASSERT_FLOAT_EQ(afterReconBlueMat.GetData()[matIt.GetPosition()], concreteIDS.GetBlue()->GetData()[matIt.GetPosition()]);
    }while(matIt.Next());
}
