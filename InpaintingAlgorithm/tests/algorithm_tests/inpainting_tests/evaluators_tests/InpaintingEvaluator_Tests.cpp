//
// Created by Jonathan on 6/3/2016.
//

#include "gtest/gtest.h"
#include "../../../../algorithm/standardTools/PaddedMatrix.h"
#include "../../../../algorithm/inpaintingTools/InpaintingOpenCVIO.h"
#include "../../../../algorithm/inpainting/InpaintingEvaluator.h"
#include "../../../../algorithm/inpainting/dataSources/decorators/IDSDDetailedOcclusion.h"
#include "../../../../algorithm/inpainting/dataSources/IDSConcrete.h"
#include "../../../../algorithm/inpainting/dataSources/decorators/IDSDDetailedOcclusionPeeling.h"

TEST(InpaintingEvaluator_Evaluate, Compare_Matlab) {
    // Size of the video samples (binary file)
    int const sizeX = 66, sizeY = 17, sizeZ = 98;
    int const patchSizeX = 5, patchSizeY = 5, patchSizeZ = 5;
    int const beta = 50; // Importance of gradients vs colors

    // Binary files TODO Make all these binary files for this test in particular instead of borrowing.
    ::std::string const testFilesDirectory = "../../resources/tests/IRWeightedAverage_Tests/all_valid/";

    ::std::string const beforeReconRedFile = testFilesDirectory + "beforeReconRed.bin";
    ::std::string const beforeReconGreenFile = testFilesDirectory + "beforeReconGreen.bin";
    ::std::string const beforeReconBlueFile = testFilesDirectory + "beforeReconBlue.bin";
    ::std::string const beforeReconGradXFile = testFilesDirectory + "beforeReconGradX.bin";
    ::std::string const beforeReconGradYFile = testFilesDirectory + "beforeReconGradY.bin";
    ::std::string const beforeReconDispAFile = testFilesDirectory + "beforeReconDispA.bin";
    ::std::string const beforeReconDispXFile = testFilesDirectory + "beforeReconDispX.bin";
    ::std::string const beforeReconDispYFile = testFilesDirectory + "beforeReconDispY.bin";
    ::std::string const beforeReconDispZFile = testFilesDirectory + "beforeReconDispZ.bin";

    ::std::string const occlusionImageL2File = testFilesDirectory + "occlusionImageL2.png";

    // Get the data ready (Load it into our data structures)
    PaddedMatrixDim * const matDim = new PaddedMatrixDim(sizeX, sizeY, sizeZ, patchSizeX/2, patchSizeY/2, patchSizeZ/2);
    PaddedMatrix<float> * const beforeReconRedMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const beforeReconGreenMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const beforeReconBlueMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const beforeReconGradXMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const beforeReconGradYMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const beforeReconDispAMat = new PaddedMatrix<float>(matDim); // This will be filled with the expected results.
    PaddedMatrix<int> * const beforeReconDispXMat = new PaddedMatrix<int>(matDim);
    PaddedMatrix<int> * const beforeReconDispYMat = new PaddedMatrix<int>(matDim);
    PaddedMatrix<int> * const beforeReconDispZMat = new PaddedMatrix<int>(matDim);
    PaddedMatrix<float> * const occMat = new PaddedMatrix<float>(matDim);

    PaddedMatrix<float> * const emptyReconDispAMat = new PaddedMatrix<float>(matDim);

    InpaintingOpenCVIO openCVIO = InpaintingOpenCVIO();
    openCVIO.LoadMatrixFromBin(beforeReconRedFile, beforeReconRedMat, 1);
    openCVIO.LoadMatrixFromBin(beforeReconGreenFile, beforeReconGreenMat, 1);
    openCVIO.LoadMatrixFromBin(beforeReconBlueFile, beforeReconBlueMat, 1);
    openCVIO.LoadMatrixFromBin(beforeReconGradXFile, beforeReconGradXMat, 1);
    openCVIO.LoadMatrixFromBin(beforeReconGradYFile, beforeReconGradYMat, 1);
    openCVIO.LoadMatrixFromBin(beforeReconDispAFile, beforeReconDispAMat, 1);
    openCVIO.LoadMatrixFromBin(beforeReconDispXFile, beforeReconDispXMat, 1);
    openCVIO.LoadMatrixFromBin(beforeReconDispYFile, beforeReconDispYMat, 1);
    openCVIO.LoadMatrixFromBin(beforeReconDispZFile, beforeReconDispZMat, 1);
    openCVIO.LoadOcclusionMatrixFromImage(occlusionImageL2File, occMat);

    // Set the data
    IDSConcrete concreteIDS = IDSConcrete(matDim, beforeReconRedMat, beforeReconGreenMat, beforeReconBlueMat,
                                          occMat, beforeReconGradXMat, beforeReconGradYMat,
                                          beforeReconDispXMat, beforeReconDispYMat,
                                          beforeReconDispZMat, emptyReconDispAMat);
    IDSDDetailedOcclusion detailedOccIDS = IDSDDetailedOcclusion(&concreteIDS, patchSizeX, patchSizeY, patchSizeZ);

    // Tested evaluator
    InpaintingEvaluator ie = InpaintingEvaluator(beta, patchSizeX, patchSizeY, patchSizeZ);

    // Test
    PaddedMatrixIt matIt = PaddedMatrixIt(matDim);
    do{
        if(detailedOccIDS.GetOcc()->GetData()[matIt.GetPosition()] < DOCC_NEAROCC)
            continue;

        int dispX = detailedOccIDS.GetDispX()->GetData()[matIt.GetPosition()];
        int dispY = detailedOccIDS.GetDispY()->GetData()[matIt.GetPosition()];
        int dispZ = detailedOccIDS.GetDispZ()->GetData()[matIt.GetPosition()];

        float ourResult = ie.EvaluateDisplacement(&detailedOccIDS, -1.0f, false,
                                                  matIt.GetCurX(), matIt.GetCurY(), matIt.GetCurZ(),
                                                  dispX, dispY, dispZ);
        float matlabResult = beforeReconDispAMat->GetData()[matIt.GetPosition()];

        ASSERT_NEAR(matlabResult, ourResult, matlabResult * 0.00001f); // Really big numbers (sometimes) and the fact that we are change the colors aren't the same changes results.
    }while(matIt.Next());
}

TEST(InpaintingEvaluator_Evaluate, Compare_Matlab_InvalidPixels_True) {
    // Size of the video samples (binary file)
    int const sizeX = 66, sizeY = 17, sizeZ = 98;
    int const patchSizeX = 5, patchSizeY = 5, patchSizeZ = 5;
    int const beta = 50; // Importance of gradients vs colors

    // Binary files
    ::std::string const testFilesDirectory = "../../resources/tests/InpaintingEvaluator_Tests/";

    ::std::string const matlabRedFile = testFilesDirectory + "matlabRed.bin";
    ::std::string const matlabGreenFile = testFilesDirectory + "matlabGreen.bin";
    ::std::string const matlabBlueFile = testFilesDirectory + "matlabBlue.bin";
    ::std::string const matlabGradXFile = testFilesDirectory + "matlabGradX.bin";
    ::std::string const matlabGradYFile = testFilesDirectory + "matlabGradY.bin";
    ::std::string const matlabDispAFile = testFilesDirectory + "matlabDispA.bin";
    ::std::string const matlabDispXFile = testFilesDirectory + "matlabDispX.bin";
    ::std::string const matlabDispYFile = testFilesDirectory + "matlabDispY.bin";
    ::std::string const matlabDispZFile = testFilesDirectory + "matlabDispZ.bin";

    ::std::string const occlusionImageL2File = testFilesDirectory + "occlusionImageL2.png";

    // Get the data ready (Load it into our data structures)
    PaddedMatrixDim * const matDim = new PaddedMatrixDim(sizeX, sizeY, sizeZ, patchSizeX/2, patchSizeY/2, patchSizeZ/2);
    PaddedMatrix<float> * const matlabRedMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const matlabGreenMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const matlabBlueMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const matlabGradXMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const matlabGradYMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const matlabDispAMat = new PaddedMatrix<float>(matDim); // This will contain MATLAB's "answer"
    PaddedMatrix<int> * const matlabDispXMat = new PaddedMatrix<int>(matDim);
    PaddedMatrix<int> * const matlabDispYMat = new PaddedMatrix<int>(matDim);
    PaddedMatrix<int> * const matlabDispZMat = new PaddedMatrix<int>(matDim);
    PaddedMatrix<float> * const occMat = new PaddedMatrix<float>(matDim);

    PaddedMatrix<float> * const emptyReconDispAMat = new PaddedMatrix<float>(matDim); // This is will conatina our "answer"

    InpaintingOpenCVIO openCVIO = InpaintingOpenCVIO();
    openCVIO.LoadMatrixFromBin(matlabRedFile, matlabRedMat, 1);
    openCVIO.LoadMatrixFromBin(matlabGreenFile, matlabGreenMat, 1);
    openCVIO.LoadMatrixFromBin(matlabBlueFile, matlabBlueMat, 1);
    openCVIO.LoadMatrixFromBin(matlabGradXFile, matlabGradXMat, 1);
    openCVIO.LoadMatrixFromBin(matlabGradYFile, matlabGradYMat, 1);
    openCVIO.LoadMatrixFromBin(matlabDispAFile, matlabDispAMat, 1);
    openCVIO.LoadMatrixFromBin(matlabDispXFile, matlabDispXMat, 1);
    openCVIO.LoadMatrixFromBin(matlabDispYFile, matlabDispYMat, 1);
    openCVIO.LoadMatrixFromBin(matlabDispZFile, matlabDispZMat, 1);
    openCVIO.LoadOcclusionMatrixFromImage(occlusionImageL2File, occMat);

    // Set the data
    IDSConcrete concreteIDS = IDSConcrete(matDim, matlabRedMat, matlabGreenMat, matlabBlueMat,
                                          occMat, matlabGradXMat, matlabGradYMat,
                                          matlabDispXMat, matlabDispYMat,
                                          matlabDispZMat, emptyReconDispAMat);
    IDSDDetailedOcclusionPeeling detailedOccIDS = IDSDDetailedOcclusionPeeling(&concreteIDS, patchSizeX, patchSizeY, patchSizeZ);

    // Test the occlusion TODO Make a new unit test for this.

    // Tested evaluator
    InpaintingEvaluator ie = InpaintingEvaluator(beta, patchSizeX, patchSizeY, patchSizeZ);

    // Test
    PaddedMatrixIt matIt = PaddedMatrixIt(matDim);
    do{
        if(detailedOccIDS.GetOcc()->GetData()[matIt.GetPosition()] < DOCC_NEAROCC)
            continue;

        int dispX = detailedOccIDS.GetDispX()->GetData()[matIt.GetPosition()];
        int dispY = detailedOccIDS.GetDispY()->GetData()[matIt.GetPosition()];
        int dispZ = detailedOccIDS.GetDispZ()->GetData()[matIt.GetPosition()];

        float ourResult = ie.EvaluateDisplacement(&detailedOccIDS, -1.0f, true,
                                               matIt.GetCurX(), matIt.GetCurY(), matIt.GetCurZ(),
                                               dispX, dispY, dispZ);
        float matlabResult = matlabDispAMat->GetData()[matIt.GetPosition()];

        ASSERT_NEAR(matlabResult, ourResult, matlabResult * 0.00001f); // Really big numbers (sometimes) and the fact that we are change the colors aren't the same changes results.
    }while(matIt.Next());
}