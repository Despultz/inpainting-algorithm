//
// Created by Jonathan on 6/6/2016.
//

#include "gtest/gtest.h"
#include "../../../../../algorithm/standardTools/PaddedMatrixIt.h"
#include "../../../../../algorithm/standardTools/PaddedMatrix.h"
#include "../../../../../algorithm/inpaintingTools/InpaintingOpenCVIO.h"
#include "../../../../../algorithm/inpainting/dataSources/decorators/IDSDDetailedOcclusionPeeling.h"
#include "../../../../../algorithm/inpainting/dataSources/IDSConcrete.h"

TEST(IDSDDetailedOcclusionPeeling_ConstructOcclusion, Compare_Matlab_Search_Peel1) {
    // Size of the video samples (binary file)
    int const sizeX = 66, sizeY = 17, sizeZ = 98;
    int const patchSizeX = 5, patchSizeY = 5, patchSizeZ = 5;

    // Binary files
    ::std::string const testFilesDirectory = "../../resources/tests/IDSDDetailedOcclusionPeeling_Tests/";
    ::std::string const matlabDetOccFile = testFilesDirectory + "matlabDetOcc_Search_P1.bin";
    ::std::string const occlusionImageL2File = testFilesDirectory + "occlusionImageL2.png"; // TODO Find a place for these "default" occlusion matrices.

    // Matrices
    PaddedMatrixDim * const matDim = new PaddedMatrixDim(sizeX, sizeY, sizeZ, patchSizeX/2, patchSizeY/2, patchSizeZ/2);
    PaddedMatrix<float> * const emptyMatFloat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<int> * const emptyMatInt = new PaddedMatrix<int>(matDim);

    PaddedMatrix<float> * const occMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const matlabDetOccMat = new PaddedMatrix<float>(matDim);  // This will contain MATLAB's "answer"

    // Load the matrices' values from binary files
    InpaintingOpenCVIO openCVIO = InpaintingOpenCVIO();
    openCVIO.LoadOcclusionMatrixFromImage(occlusionImageL2File, occMat);
    openCVIO.LoadMatrixFromBin(matlabDetOccFile, matlabDetOccMat, 1);

    // Calculate our detailed occlusion
    IDSConcrete concreteIDS = IDSConcrete(matDim, emptyMatFloat, emptyMatFloat, emptyMatFloat,
                                          occMat, emptyMatFloat, emptyMatFloat,
                                          emptyMatInt, emptyMatInt, emptyMatInt, emptyMatFloat);
    IDSDDetailedOcclusionPeeling detailedOccIDS = IDSDDetailedOcclusionPeeling(&concreteIDS, patchSizeX, patchSizeY, patchSizeZ);

    // Test the values
    PaddedMatrixIt matIt = PaddedMatrixIt(matDim);
    do {
        float matlabValue = matlabDetOccMat->GetData()[matIt.GetPosition()];
        float ourValue = detailedOccIDS.GetOcc()->GetData()[matIt.GetPosition()];

        ASSERT_TRUE((matlabValue == 0.0f) == (ourValue <= DOCC_NEARBORDER));
        ASSERT_TRUE((matlabValue == 1.0f) == (ourValue >= DOCC_BORDER && ourValue <= DOCC_CORE));
        ASSERT_TRUE((matlabValue == 2.0f) == (ourValue == DOCC_NEAROCC));
    } while (matIt.Next());
}

TEST(IDSDDetailedOcclusionPeeling_ConstructOcclusion, Compare_Matlab_Recon_Peel1) {
    // Size of the video samples (binary file)
    int const sizeX = 66, sizeY = 17, sizeZ = 98;
    int const patchSizeX = 5, patchSizeY = 5, patchSizeZ = 5;

    // Binary files
    ::std::string const testFilesDirectory = "../../resources/tests/IDSDDetailedOcclusionPeeling_Tests/";
    ::std::string const matlabDetOccFile = testFilesDirectory + "matlabDetOcc_Recon_P1.bin";
    ::std::string const occlusionImageL2File = testFilesDirectory + "occlusionImageL2.png"; // TODO Find a place for these "default" occlusion matrices.

    // Matrices
    PaddedMatrixDim * const matDim = new PaddedMatrixDim(sizeX, sizeY, sizeZ, patchSizeX/2, patchSizeY/2, patchSizeZ/2);
    PaddedMatrix<float> * const emptyMatFloat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<int> * const emptyMatInt = new PaddedMatrix<int>(matDim);

    PaddedMatrix<float> * const occMat = new PaddedMatrix<float>(matDim);
    PaddedMatrix<float> * const matlabDetOccMat = new PaddedMatrix<float>(matDim);  // This will contain MATLAB's "answer"

    // Load the matrices' values from binary files
    InpaintingOpenCVIO openCVIO = InpaintingOpenCVIO();
    openCVIO.LoadOcclusionMatrixFromImage(occlusionImageL2File, occMat);
    openCVIO.LoadMatrixFromBin(matlabDetOccFile, matlabDetOccMat, 1);

    // Calculate our detailed occlusion
    IDSConcrete concreteIDS = IDSConcrete(matDim, emptyMatFloat, emptyMatFloat, emptyMatFloat,
                                          occMat, emptyMatFloat, emptyMatFloat,
                                          emptyMatInt, emptyMatInt, emptyMatInt, emptyMatFloat);
    IDSDDetailedOcclusionPeeling detailedOccIDS = IDSDDetailedOcclusionPeeling(&concreteIDS, patchSizeX, patchSizeY, patchSizeZ);

    // Test the values
    PaddedMatrixIt matIt = PaddedMatrixIt(matDim);
    do {
        float matlabValue = matlabDetOccMat->GetData()[matIt.GetPosition()];
        float ourValue = detailedOccIDS.GetOcc()->GetData()[matIt.GetPosition()];

        ASSERT_TRUE((matlabValue == 0.0f) == (ourValue <= DOCC_NEAROCC));
        ASSERT_TRUE((matlabValue == 1.0f) == (ourValue >= DOCC_BORDER && ourValue <= DOCC_BORDER_ALT));
        ASSERT_TRUE((matlabValue == 2.0f) == (ourValue == DOCC_CORE));
    } while (matIt.Next());
}