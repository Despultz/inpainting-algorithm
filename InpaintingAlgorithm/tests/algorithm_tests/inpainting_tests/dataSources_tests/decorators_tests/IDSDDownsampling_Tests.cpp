//
// Created by Jonathan on 5/19/2016.
//

#include "gtest/gtest.h"
#include "../../../../../algorithm/inpainting/dataSources/IDSConcrete.h"
#include "../../../../../algorithm/inpaintingTools/InpaintingOpenCVIO.h"
#include "../../../../../algorithm/inpainting/dataSources/decorators/IDSDDownsampling.h"

IDSConcrete GetBaseData(){
    const std::string videoFileName = "../../resources/beach_umbrella.avi";
    const std::string occFileName = "../../resources/beach_umbrella_occlusion.png";

    PaddedMatrixDim * dim = new PaddedMatrixDim(264, 68, 98, 2, 2, 2);

    PaddedMatrix<float> * redMatrix = new PaddedMatrix<float>(dim);
    PaddedMatrix<float> * blueMatrix = new PaddedMatrix<float>(dim);
    PaddedMatrix<float> * greenMatrix = new PaddedMatrix<float>(dim);
    PaddedMatrix<float> * occMatrix = new PaddedMatrix<float>(dim);

    InpaintingOpenCVIO openCVIO = InpaintingOpenCVIO();
    openCVIO.LoadRGBMatrixFromVideo(videoFileName, redMatrix, greenMatrix, blueMatrix);
    openCVIO.LoadOcclusionMatrixFromImage(occFileName, occMatrix);

    return IDSConcrete(dim, redMatrix, greenMatrix, blueMatrix, occMatrix);
}

TEST(IDSDDownsampling_Tests, Compare_Matlab_CasellesDownsampling) {
    // The files containing the data extracted from MATLAB
    const std::string gradXLevel2 = "../../resources/tests/normGradX_L2.csv";
    const std::string gradXLevel3 = "../../resources/tests/normGradX_L3.csv";

    const std::string gradYLevel2 = "../../resources/tests/normGradY_L2.csv";
    const std::string gradYLevel3 = "../../resources/tests/normGradY_L3.csv";

    // Prepare our structures and our data
    int const downsamplingRate = 2, downsamplingLevels = 3;
    int const sizeX = 264, sizeY = 68, sizeZ = 98;
    int const sizeX2 = sizeX/downsamplingRate, sizeY2 = sizeY/downsamplingRate;
    int const sizeX3 = sizeX2/downsamplingRate, sizeY3 = sizeY2/downsamplingRate;

    PaddedMatrixDim dims[2] = { PaddedMatrixDim(sizeX2, sizeY2, sizeZ, 2, 2, 2),
                                PaddedMatrixDim(sizeX3, sizeY3, sizeZ, 2, 2, 2) };

    InpaintingOpenCVIO openCVIO = InpaintingOpenCVIO();
    IDSConcrete idsBaseData = GetBaseData();

    // Load the MATLAB results
    PaddedMatrix<float> matlabGradXMatrixDS[2] = { PaddedMatrix<float>(&dims[0]), PaddedMatrix<float>(&dims[1]) };
    PaddedMatrix<float> matlabGradYMatrixDS[2] = { PaddedMatrix<float>(&dims[0]), PaddedMatrix<float>(&dims[1]) };

    openCVIO.LoadMatrixFromCSV(gradXLevel2, &matlabGradXMatrixDS[0]);
    openCVIO.LoadMatrixFromCSV(gradXLevel3, &matlabGradXMatrixDS[1]);

    openCVIO.LoadMatrixFromCSV(gradYLevel2, &matlabGradYMatrixDS[0]);
    openCVIO.LoadMatrixFromCSV(gradYLevel3, &matlabGradYMatrixDS[1]);

    // Run the tested code (The constructor automatically downsamples the data for RGB and Caselles)
    IDSDDownsampling testIdsdd = IDSDDownsampling(&idsBaseData, downsamplingLevels, downsamplingRate);

    // Test the results
    for(int curTestLevel = 1; curTestLevel >= 0; curTestLevel--){
        float diffSumX = 0.0f;
        float diffSumY = 0.0f;
        int diffCount = 0;

        testIdsdd.SetCurLevel(curTestLevel + 1);

        float * matlabGradX = matlabGradXMatrixDS[curTestLevel].GetData();
        float * matlabGradY = matlabGradYMatrixDS[curTestLevel].GetData();

        float * testGradX = testIdsdd.GetGradX()->GetData();
        float * testGradY = testIdsdd.GetGradY()->GetData();;

        PaddedMatrixIt matIt = PaddedMatrixIt(&dims[curTestLevel]);
        do{
            if(matlabGradX[matIt.GetPosition()] != 0 && matlabGradY[matIt.GetPosition()] != 0) {
                diffCount++;
                diffSumX += fabsf(testGradX[matIt.GetPosition()] / matlabGradX[matIt.GetPosition()] - 1);
                diffSumY += fabsf(testGradY[matIt.GetPosition()] / matlabGradY[matIt.GetPosition()] - 1);
            }
        }while(matIt.Next());

        ASSERT_LT(diffSumX / diffCount, 0.011f); // Awkward 0.0100481 difference when tested.
        ASSERT_LT(diffSumY / diffCount, 0.011f);

        testIdsdd.UpsampleShiftMatrices();
    }
}

TEST(IDSDDownsampling_Tests, Compare_Matlab_ShiftMatricesUpsampling) {
    // The files containing the data extracted from MATLAB
    ::std::string const testRessourceDirectory = "../../resources/tests/IDSDDownsampling_Tests/disp_matrices_upsampling/";
    ::std::string const shiftA_after = testRessourceDirectory + "shiftAAfterTest.bin";
    ::std::string const shiftX_after = testRessourceDirectory + "shiftXAfterTest.bin";
    ::std::string const shiftY_after = testRessourceDirectory + "shiftYAfterTest.bin";
    ::std::string const shiftZ_after = testRessourceDirectory + "shiftZAfterTest.bin";

    ::std::string const shiftA_before = testRessourceDirectory + "shiftABeforeTest.bin";
    ::std::string const shiftX_before = testRessourceDirectory + "shiftXBeforeTest.bin";
    ::std::string const shiftY_before = testRessourceDirectory + "shiftYBeforeTest.bin";
    ::std::string const shiftZ_before = testRessourceDirectory + "shiftZBeforeTest.bin";

    // Prepare our structures and our data
    int const downsamplingRate = 2, downsamplingLevels = 3;

    InpaintingOpenCVIO openCVIO = InpaintingOpenCVIO();
    IDSConcrete idsBaseData = GetBaseData();

    IDSDDownsampling testIdsdd = IDSDDownsampling(&idsBaseData, downsamplingLevels, downsamplingRate); // Creates all the needed matrices.

    // Fill our data with MATLAB's data.
    PaddedMatrix<float> * const shiftAMat_before = testIdsdd.GetDispA();
    PaddedMatrix<int> * const shiftXMat_before = testIdsdd.GetDispX();
    PaddedMatrix<int> * const shiftYMat_before = testIdsdd.GetDispY();
    PaddedMatrix<int> * const shiftZMat_before = testIdsdd.GetDispZ();

    openCVIO.LoadMatrixFromBin<float>(shiftA_before, shiftAMat_before, 1);
    openCVIO.LoadMatrixFromBin<int>(shiftX_before, shiftXMat_before, 1);
    openCVIO.LoadMatrixFromBin<int>(shiftY_before, shiftYMat_before, 1);
    openCVIO.LoadMatrixFromBin<int>(shiftZ_before, shiftZMat_before, 1);

    // Run the tested code
    testIdsdd.SetCurLevel(downsamplingLevels - 1);
    testIdsdd.UpsampleShiftMatrices();
    testIdsdd.SetCurLevel(downsamplingLevels - 2);

    // Test the results.
    PaddedMatrix<float> shiftAMat_MatlabAfter = PaddedMatrix<float>(testIdsdd.GetDim());
    PaddedMatrix<int> shiftXMat_MatlabAfter = PaddedMatrix<int>(testIdsdd.GetDim());
    PaddedMatrix<int> shiftYMat_MatlabAfter = PaddedMatrix<int>(testIdsdd.GetDim());
    PaddedMatrix<int> shiftZMat_MatlabAfter = PaddedMatrix<int>(testIdsdd.GetDim());

    openCVIO.LoadMatrixFromBin<float>(shiftA_after, &shiftAMat_MatlabAfter, 1);
    openCVIO.LoadMatrixFromBin<int>(shiftX_after, &shiftXMat_MatlabAfter, 1);
    openCVIO.LoadMatrixFromBin<int>(shiftY_after, &shiftYMat_MatlabAfter, 1);
    openCVIO.LoadMatrixFromBin<int>(shiftZ_after, &shiftZMat_MatlabAfter, 1);

    float * shiftAData_MatlabAfter = shiftAMat_MatlabAfter.GetData();
    int * shiftXData_MatlabAfter = shiftXMat_MatlabAfter.GetData();
    int * shiftYData_MatlabAfter = shiftYMat_MatlabAfter.GetData();
    int * shiftZData_MatlabAfter = shiftZMat_MatlabAfter.GetData();

    float * shiftAData_OursAfter = testIdsdd.GetDispA()->GetData();
    int * shiftXData_OursAfter = testIdsdd.GetDispX()->GetData();
    int * shiftYData_OursAfter = testIdsdd.GetDispY()->GetData();
    int * shiftZData_OursAfter = testIdsdd.GetDispZ()->GetData();

    PaddedMatrixIt matIt = PaddedMatrixIt(testIdsdd.GetDim());
    do{
        ASSERT_FLOAT_EQ(shiftAData_OursAfter[matIt.GetPosition()], shiftAData_MatlabAfter[matIt.GetPosition()]);
        ASSERT_EQ(shiftXData_OursAfter[matIt.GetPosition()], shiftXData_MatlabAfter[matIt.GetPosition()]);
        ASSERT_EQ(shiftYData_OursAfter[matIt.GetPosition()], shiftYData_MatlabAfter[matIt.GetPosition()]);

        // The MATLAB interpolation has issues while interpolating the Z, probably due to rounding of some sort.
        ASSERT_NEAR(shiftZData_OursAfter[matIt.GetPosition()], shiftZData_MatlabAfter[matIt.GetPosition()], 1);
    }while(matIt.Next());
}

TEST(IDSDDownsampling_Tests, Compare_Matlab_ColorOccDownsampling){
    std::string const fileRedLevelX = "../../resources/tests/IDSDDownsampling_Tests/redLevel";
    std::string const fileGreenLevelX = "../../resources/tests/IDSDDownsampling_Tests/greenLevel";
    std::string const fileBlueLevelX = "../../resources/tests/IDSDDownsampling_Tests/blueLevel";
    std::string const fileOccLevelX = "../../resources/tests/IDSDDownsampling_Tests/occLevel";
    std::string const fileSuffix = ".bin";

    // Other needed variables
    InpaintingOpenCVIO openCVIO = InpaintingOpenCVIO();
    int const downsamplingLevels = 3, downsamplingRate = 2;

    // Setup our data
    IDSConcrete idsBaseData = GetBaseData();

    // Downsample the data
    IDSDDownsampling testIdsdd = IDSDDownsampling(&idsBaseData, downsamplingLevels, downsamplingRate); // Creates all the needed matrices.

    // Loop for each level
    for(int i = 0; i < downsamplingLevels; i++){
        // Get our matrices for this level
        testIdsdd.SetCurLevel(i);

        // Load the MATLAB matrices for this level
        std::ostringstream redFilename;
        redFilename << fileRedLevelX << i + 1 << fileSuffix;
        PaddedMatrix<float> redMatrix = PaddedMatrix<float>(testIdsdd.GetDim());
        openCVIO.LoadMatrixFromBin<float>(redFilename.str(), &redMatrix);

        std::ostringstream greenFilename;
        greenFilename << fileGreenLevelX << i + 1 << fileSuffix;
        PaddedMatrix<float> greenMatrix = PaddedMatrix<float>(testIdsdd.GetDim());
        openCVIO.LoadMatrixFromBin<float>(greenFilename.str(), &greenMatrix);

        std::ostringstream blueFilename;
        blueFilename << fileBlueLevelX << i + 1 << fileSuffix;
        PaddedMatrix<float> blueMatrix = PaddedMatrix<float>(testIdsdd.GetDim());
        openCVIO.LoadMatrixFromBin<float>(blueFilename.str(), &blueMatrix);

        std::ostringstream occFilename;
        occFilename << fileOccLevelX << i + 1 << fileSuffix;
        PaddedMatrix<float> occMatrix = PaddedMatrix<float>(testIdsdd.GetDim());
        openCVIO.LoadMatrixFromBin<float>(occFilename.str(), &occMatrix);

        /*
        for(int j = 0; j < testIdsdd.GetDim()->GetSizeZ(); j++){
            openCVIO.GeneralIOForRGB(&redMatrix, &greenMatrix, &blueMatrix, j, InpaintingIO::IH_DBG_SHOW);
            openCVIO.GeneralIOForRGB(testIdsdd.GetRed(), testIdsdd.GetGreen(), testIdsdd.GetBlue(), j, InpaintingIO::IH_DBG_SHOW);
        }
        */

        // Float arrays for this level (expected / actual)
        float * redMatrixMatlab = redMatrix.GetData();
        float * greenMatrixMatlab = greenMatrix.GetData();
        float * blueMatrixMatlab = blueMatrix.GetData();
        float * occMatrixMatlab = occMatrix.GetData();

        float * redMatrixUs = testIdsdd.GetRed()->GetData();
        float * greenMatrixUs = testIdsdd.GetGreen()->GetData();
        float * blueMatrixUs = testIdsdd.GetBlue()->GetData();
        float * occMatrixUs = testIdsdd.GetOcc()->GetData();

        PaddedMatrixIt matIt = PaddedMatrixIt(testIdsdd.GetDim());
        do{
            ASSERT_NEAR(redMatrixMatlab[matIt.GetPosition()], redMatrixUs[matIt.GetPosition()], 0.05f);
            ASSERT_NEAR(greenMatrixMatlab[matIt.GetPosition()], greenMatrixUs[matIt.GetPosition()], 0.05f);
            ASSERT_NEAR(blueMatrixMatlab[matIt.GetPosition()], blueMatrixUs[matIt.GetPosition()], 0.05f);
            ASSERT_TRUE(occMatrixMatlab[matIt.GetPosition()] == 0.0f || occMatrixUs[matIt.GetPosition()] > 0.0f);
        }while(matIt.Next());
    }
}