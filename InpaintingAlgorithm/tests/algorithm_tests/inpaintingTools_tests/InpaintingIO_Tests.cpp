//
// Created by Jonathan on 5/26/2016.
//

#include "gtest/gtest.h"
#include "../../../algorithm/inpaintingTools/InpaintingOpenCVIO.h"

// For human eyes. Could test with the values, but that would be very hardcoded, and for only a few values.
TEST(InpaintingIO, read_binary){
    const std::string redBinFile = "../../resources/tests/binTestRed.bin";
    const std::string greenBinFile = "../../resources/tests/binTestGreen.bin";
    const std::string blueBinFile = "../../resources/tests/binTestBlue.bin";

    InpaintingOpenCVIO openCVIO = InpaintingOpenCVIO();
    PaddedMatrixDim dim = PaddedMatrixDim(66, 17, 98, 2, 2, 2);

    PaddedMatrix<float> redMat = PaddedMatrix<float>(&dim);
    PaddedMatrix<float> grnMat = PaddedMatrix<float>(&dim);
    PaddedMatrix<float> bluMat = PaddedMatrix<float>(&dim);

    openCVIO.LoadMatrixFromBin<float>(redBinFile, &redMat);
    openCVIO.LoadMatrixFromBin<float>(greenBinFile, &grnMat);
    openCVIO.LoadMatrixFromBin<float>(blueBinFile, &bluMat);

    for(int i = 0; i < dim.GetSizeZ(); ++i){
        openCVIO.GeneralIOForRGB(&redMat, &grnMat, &bluMat, i, InpaintingIO::IH_DBG_SHOW);
    }
}