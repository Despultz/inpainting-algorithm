//
// Created by Jonathan on 11/03/2016.
//

#include "PaddedMatrixKernelIt.h"

//TODO I don't think this is anywhere near the optimal way of doing things for performance reasons.
PaddedMatrixKernelIt::PaddedMatrixKernelIt(PaddedMatrixDim const * const outerDimension, PaddedMatrixDim const * const innerDimension)
    : outDim(outerDimension), inDim(innerDimension)
{
    // Set the position to the start.
    backStepFromCenter = inDim->GetHalfSizeXBefore(); // For pair dimension (EX 4) we go back the smallest amount and go forward the biggest amount (aka 1 CENTER 2 3 for a size 4)
    backStepFromCenter += inDim->GetHalfSizeYBefore() * outDim->GetBuffSizeX();
    backStepFromCenter += inDim->GetHalfSizeZBefore() * outDim->GetBuffAreaXY();

    endI = inDim->GetVolume();
}

void PaddedMatrixKernelIt::Start(int centerBuffI) {
    curBuffI = centerBuffI - backStepFromCenter;

    curI = 0;
    curInX = 0;
    curInY = 0;
    curInZ = 0;
}

bool PaddedMatrixKernelIt::Next() {
    ++curI;
    if(curI >= endI) return false;

    ++curInX;
    ++curBuffI;
    if(curInX >= inDim->GetSizeX()){
        curInX = 0;
        ++curInY;
        curBuffI += outDim->GetBuffSizeX() - inDim->GetSizeX();

        if(curInY >= inDim->GetSizeY()){
            curInY = 0;
            ++curInZ;
            curBuffI += outDim->GetBuffAreaXY() - inDim->GetSizeY() * outDim->GetBuffSizeX();
        }
    }

    return true;
}

int PaddedMatrixKernelIt::GetPosition() {
    return curBuffI;
}

int PaddedMatrixKernelIt::GetIndex() {
    return curI;
}

int PaddedMatrixKernelIt::GetRelX() {
    return curInX - (int)floor(inDim->GetSizeX() / 2.0f);
}

int PaddedMatrixKernelIt::GetRelY() {
    return curInY - (int)floor(inDim->GetSizeY() / 2.0f);
}

int PaddedMatrixKernelIt::GetRelZ() {
    return curInZ - (int)floor(inDim->GetSizeZ() / 2.0f);
}
