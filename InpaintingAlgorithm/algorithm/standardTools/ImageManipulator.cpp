//
// Created by Jonathan on 23/02/2016.
//

#include "ImageManipulator.h"

double * ImageManipulator::GaussianFilter(double sigma, int sizeX, int sizeY) {
    double * gaussianFilter = new double[sizeX * sizeY];

    double middleX = (sizeX - 1)/2;
    double middleY = (sizeY - 1)/2;
    double divisor = 2*sigma*sigma;
    double sum = 0;

    for(int a = 0; a < sizeX; ++a){
        double aRelMid = (a - middleX)*(a - middleX);

        for(int b = 0; b < sizeY; ++b){
            int index = a * sizeY + b;
            double bRelMid = (b - middleY)*(b - middleY);
            gaussianFilter[index] = pow(M_E, -(aRelMid + bRelMid)/divisor);
            sum += gaussianFilter[index];
        }
    }

    for(int a = 0; a < sizeX; ++a){
        for(int b = 0; b < sizeY; ++b) {
            gaussianFilter[a * sizeX + b]/=sum;
        }
    }

    return gaussianFilter;
}

PaddedMatrix<float> ImageManipulator::GrayScaleFromRGB(PaddedMatrix<float> *redMat, PaddedMatrix<float> *blueMat, PaddedMatrix<float> *grnMat)
{
    // Not pretty, but still a start
    float const matlabRedFactor = 0.298936021293776f;
    float const matlabGreenFactor = 0.587043074451121f;
    float const matlabBlueFactor = 0.114020904255103f;

    // We assume all the matrixes have the same dimension as the red one.
    PaddedMatrixDim const * const dim = redMat->GetDimension();
    PaddedMatrix<float> grayMatrix = PaddedMatrix<float>(dim);

    float* grayData = grayMatrix.GetData();
    float* redData = redMat->GetData();
    float* grnData = blueMat->GetData();
    float* bluData = grnMat->GetData();

    PaddedMatrixIt volIt = PaddedMatrixIt(dim);
    do{
        // http://www.johndcook.com/blog/2009/08/24/algorithms-convert-color-grayscale/
        // http://www.tannerhelland.com/3643/grayscale-image-algorithm-vb6/ (Using BT.601, verified with MATLAB values)
        grayData[volIt.GetPosition()] = matlabRedFactor * redData[volIt.GetPosition()] +
                                        matlabGreenFactor * grnData[volIt.GetPosition()] +
                                        matlabBlueFactor * bluData[volIt.GetPosition()];
    }while(volIt.Next());

    return grayMatrix;
}

void ImageManipulator::CalculateCasellesDescriptors(PaddedMatrixDim * const dim, PaddedMatrix<float> * const redMatrix,
                                                    PaddedMatrix<float> * const greenMatrix, PaddedMatrix<float> * const blueMatrix,
                                                    PaddedMatrix<float> * const occMatrix, PaddedMatrix<float> * const gradXMatrix,
                                                    PaddedMatrix<float> * const gradYMatrix, int const averagingAreaSide)
{
    // Calculate the size needed by the new, temporary, matrices
    int halfAreaSide = (int)ceilf(averagingAreaSide / 2.0f);
    PaddedMatrixDim tempDim = PaddedMatrixDim(dim->GetSizeX(), dim->GetSizeY(), dim->GetSizeZ(),
                                              halfAreaSide, halfAreaSide, 1);

    // Three new temporary matrices are needed for this algorithm to work.
    PaddedMatrix<float> grayMatrix = ImageManipulator::GrayScaleFromRGB(redMatrix, greenMatrix, blueMatrix);
    PaddedMatrix<float> tGradXMatrix = PaddedMatrix<float>(&tempDim);
    PaddedMatrix<float> tGradYMatrix = PaddedMatrix<float>(&tempDim);

    // Fill the occlusion matrix's padding with occlusion
    occMatrix->FillBufferWith(1.0f);
    tGradXMatrix.FillBufferWith(0.0f);
    tGradYMatrix.FillBufferWith(0.0f);

    // All the data we'll work with
    float* grayData = grayMatrix.GetData();
    float* occData = occMatrix->GetData();
    float* gradXData = gradXMatrix->GetData();
    float* gradYData = gradYMatrix->GetData();

    float* tGradXData = tGradXMatrix.GetData();
    float* tGradYData = tGradYMatrix.GetData();

    // First loop is to calculate the displacement for each pixel.
    // We cannot use buffers for these, so all the border pixels have different ways of being calculated.
    PaddedMatrixIt stdMatIt = PaddedMatrixIt(dim);
    PaddedMatrixIt tmpMatIt = PaddedMatrixIt(&tempDim);
    do{
        int pos = stdMatIt.GetPosition();
        int row = dim->GetBuffSizeX();

        int tmpPos = tmpMatIt.GetPosition();

        if(occData[pos] == 1){
            tGradXData[tmpPos] = 0.0f;
            tGradYData[tmpPos] = 0.0f;
        }
        else{
            float gradX = 0, gradY = 0;
            if(stdMatIt.IsLeftBorderX() || occData[pos - 1] > 0)
                gradX = grayData[pos + 1] - grayData[pos];
            else if(stdMatIt.IsRightBorderX() || occData[pos + 1] > 0)
                gradX = grayData[pos] - grayData[pos - 1];
            else
                gradX = (grayData[pos + 1] - grayData[pos - 1])/2; // Simplified version of : ((next - current) + (current - previous))/2 : which give the average gradient.

            if(stdMatIt.IsTopBorderY() || occData[pos - row] > 0)
                gradY = grayData[pos + row] - grayData[pos];
            else if(stdMatIt.IsBottomBorderY() || occData[pos + row] > 0)
                gradY = grayData[pos] - grayData[pos - row];
            else
                gradY = (grayData[pos + row] - grayData[pos - row])/2; // Simplified version of : ((next - current) + (current - previous))/2 : which give the average gradient.

            tGradXData[tmpPos] = fabsf(gradX);
            tGradYData[tmpPos] = fabsf(gradY);
        }
    }while(stdMatIt.Next() && tmpMatIt.Next());

    // Second loop is to average the displacement in an area around each pixel.
    // Occluded pixels or pixels that are out of bounds do not count towards the average for a pixel.

    PaddedMatrixDim kernelDimension = PaddedMatrixDim(averagingAreaSide, averagingAreaSide, 1);
    PaddedMatrixKernelIt stdKernelIt = PaddedMatrixKernelIt(dim, &kernelDimension);
    PaddedMatrixKernelIt tmpKernelIt = PaddedMatrixKernelIt(&tempDim, &kernelDimension);

    stdMatIt.Start();
    tmpMatIt.Start();
    do{
        int occCounter = 0;
        float gradXSum = 0.0f, gradYSum = 0.0f;

        stdKernelIt.Start(stdMatIt.GetPosition());
        tmpKernelIt.Start(tmpMatIt.GetPosition());
        do{
            if(occData[stdKernelIt.GetPosition()] == 0.0f){
                occCounter++;
                gradXSum += tGradXData[tmpKernelIt.GetPosition()];
                gradYSum += tGradYData[tmpKernelIt.GetPosition()];
            }
        }while(stdKernelIt.Next() && tmpKernelIt.Next());

        if(occCounter == 0){
            gradXData[stdMatIt.GetPosition()] = 0.0f;
            gradYData[stdMatIt.GetPosition()] = 0.0f;
        }
        else{
            gradXData[stdMatIt.GetPosition()] = gradXSum / occCounter;
            gradYData[stdMatIt.GetPosition()] = gradYSum / occCounter;
        }
    }while(stdMatIt.Next() && tmpMatIt.Next());
}

template <class T> PaddedMatrix<T> ImageManipulator::ImageFilter(PaddedMatrix<T> * const sourceMatrix, PaddedMatrix<float> * const filterMatrix){
    PaddedMatrix<T> outMatrix = PaddedMatrix<T>(sourceMatrix->GetDimension());
    sourceMatrix->MirrorIntoBuffer();

    PaddedMatrixIt sourceIt = PaddedMatrixIt(sourceMatrix->GetDimension());
    PaddedMatrixKernelIt kernelIt = PaddedMatrixKernelIt(sourceMatrix->GetDimension(), filterMatrix->GetDimension());

    do{
        T outResult = 0; // Hmm, will work for like all numeric values...

        kernelIt.Start(sourceIt.GetPosition());
        do{
            // This won't work if the filterMatrix has padding.
            outResult += sourceMatrix->GetData()[kernelIt.GetPosition()] * filterMatrix->GetData()[kernelIt.GetIndex()];
        }while(kernelIt.Next());

        outMatrix.GetData()[sourceIt.GetPosition()] = outResult;
    }while(sourceIt.Next());

    return outMatrix;
}

template PaddedMatrix<float> ImageManipulator::ImageFilter(PaddedMatrix<float> * const sourceMatrix, PaddedMatrix<float> * const filterMatrix);
template PaddedMatrix<int> ImageManipulator::ImageFilter(PaddedMatrix<int> * const sourceMatrix, PaddedMatrix<float> * const filterMatrix);