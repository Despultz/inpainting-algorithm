//
// Created by Jonathan on 01/03/2016.
//

#ifndef INPAINTINGALGORITHM_VOLUMEDIMENSION_H
#define INPAINTINGALGORITHM_VOLUMEDIMENSION_H

#include <math.h>

class PaddedMatrixDim {
private:
    // The dimensions and pre-calculated values without the buffers.
    int const sizeX, sizeY, sizeZ;
    int const areaXY;
    int const volume;

    // The dimensions and pre-calculated values with the buffers.
    int const buffX, buffY, buffZ;
    int const buffSizeX, buffSizeY, buffSizeZ;
    int const buffAreaXY;
    int const buffVolume;

    int const frameBuffer, videoBuffer;

    // Pre-calculated half sizes, the center being in the second half (after) for impair values (usefull in kernels)
    // Before ; Values that come before the center.
    // After  ; Values that come after the center.
    int const halfSizeXBefore, halfSizeYBefore, halfSizeZBefore;
    int const halfSizeXAfter, halfSizeYAfter, halfSizeZAfter;

public:
    PaddedMatrixDim(int sizeX, int sizeY, int sizeZ);
    PaddedMatrixDim(int sizeX, int sizeY, int sizeZ, int buffX, int buffY, int buffZ);

    int DeterminePosition(int x, int y, int z) const;
    void DetailPosition(int i, int &x, int &y, int &z) const;

    const int GetSizeX() const {
        return sizeX;
    }

    const int GetSizeY() const {
        return sizeY;
    }

    const int GetSizeZ() const {
        return sizeZ;
    }

    const int GetAreaXY() const {
        return areaXY;
    }

    const int GetVolume() const {
        return volume;
    }

    const int GetBuffX() const {
        return buffX;
    }

    const int GetBuffY() const {
        return buffY;
    }

    const int GetBuffZ() const {
        return buffZ;
    }

    const int GetBuffSizeX() const {
        return buffSizeX;
    }

    const int GetBuffSizeY() const {
        return buffSizeY;
    }

    const int GetBuffSizeZ() const {
        return buffSizeZ;
    }

    const int GetBuffAreaXY() const {
        return buffAreaXY;
    }

    const int GetBuffVolume() const {
        return buffVolume;
    }

    const int GetFrameBuffer() const {
        return frameBuffer;
    }

    const int GetVideoBuffer() const {
        return videoBuffer;
    }

    const int GetHalfSizeXBefore() const {
        return halfSizeXBefore;
    }

    const int GetHalfSizeYBefore() const {
        return halfSizeYBefore;
    }

    const int GetHalfSizeZBefore() const {
        return halfSizeZBefore;
    }

    const int GetHalfSizeXAfter() const {
        return halfSizeXAfter;
    }

    const int GetHalfSizeYAfter() const {
        return halfSizeYAfter;
    }

    const int GetHalfSizeZAfter() const {
        return halfSizeZAfter;
    }
};


#endif //INPAINTINGALGORITHM_VOLUMEDIMENSION_H
