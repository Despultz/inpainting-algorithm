//
// Created by Jonathan on 15/03/2016.
//

#include "PaddedMatrixRevIt.h"

PaddedMatrixRevIt::PaddedMatrixRevIt(PaddedMatrixDim *dim) : PaddedMatrixRevIt(dim, dim->GetVolume() - 1, 0){}

PaddedMatrixRevIt::PaddedMatrixRevIt(PaddedMatrixDim *dim, int startAt, int stopAt) : PaddedMatrixIt(dim, startAt, stopAt){}

bool PaddedMatrixRevIt::Next() {
    if(curI <= endI) return false;
    --curI;

    if(curX != 0) {
        --curX;
        curBuffI -= 1;
    }
    else{
        curX = dim->GetSizeX() - 1;

        if(curY != 0) {
            --curY;
            curBuffI -= 2 * dim->GetBuffX() + 1;
        }
        else{
            curY = dim->GetSizeY() - 1;

            --curZ;
            curBuffI -= 2 * dim->GetFrameBuffer() + 2 * dim->GetBuffX() + 1; // Skip the buffer after and before the frames.

            // If curZ is greater than the number of frames, then we have a problem.
        }
    }

    return true;
}

bool PaddedMatrixRevIt::Next(int leapX, int leapY) {
    curX -= leapX;
    if(curX < 0){
        curX = dim->GetSizeX() - 1;
        curY -= leapY;
        if(curY < 0){
            curY = dim->GetSizeY() - 1;
            curZ--;
        }
    }

    curBuffI = dim->DeterminePosition(curX, curY, curZ);
    curI = curZ * dim->GetAreaXY() + curY * dim->GetSizeX() + curX;

    return curI >= endI;
}
