//
// Created by Jonathan on 07/03/2016.
//

#include "PaddedMatrixIt.h"

PaddedMatrixIt::PaddedMatrixIt(PaddedMatrixDim const * const dim) : PaddedMatrixIt::PaddedMatrixIt(dim, 0, dim->GetVolume()){}

PaddedMatrixIt::PaddedMatrixIt(PaddedMatrixDim const * const dim, int startAt, int stopAt) : dim(dim), curI(startAt), endI(stopAt)
{
    this->dim->DetailPosition(curI, this->curX, this->curY, this->curZ);
    curBuffI = this->dim->DeterminePosition(this->curX, this->curY, this->curZ);
}

void PaddedMatrixIt::Start(int const startAt){
    Start(startAt, endI);
}

void PaddedMatrixIt::Start(int const startAt, int const endAt){
    curI = startAt;
    endI = endAt;

    this->dim->DetailPosition(curI, this->curX, this->curY, this->curZ);
    curBuffI = this->dim->DeterminePosition(this->curX, this->curY, this->curZ);
}

bool PaddedMatrixIt::Next() {
    ++curI;
    if(curI >= endI) return false;

    ++curX;
    ++curBuffI;
    if(curX >= dim->GetSizeX()){
        curX = 0;
        ++curY;
        curBuffI += 2 * dim->GetBuffX(); // Skip the buffer after and before the rows.

        if(curY >=  dim->GetSizeY()){
            curY = 0;
            ++curZ;
            curBuffI += 2 * dim->GetFrameBuffer(); // Skip the buffer after and before the frames.

            // If curZ is greater than the number of frames, then we have a problem.
        }
    }

    return true;
}

bool PaddedMatrixIt::Next(int leapX, int leapY) {
    curX += leapX;
    if(curX >= dim->GetSizeX()){
        curX = 0;
        curY += leapY;
        if(curY >= dim->GetSizeY()){
            curY = 0;
            ++curZ;
        }
    }

    curBuffI = dim->DeterminePosition(curX, curY, curZ);
    curI = curZ * dim->GetAreaXY() + curY * dim->GetSizeX() + curX;

    return curI < endI;
}

bool PaddedMatrixIt::IsLeftBorderX() {
    return curX == 0;
}

bool PaddedMatrixIt::IsRightBorderX() {
    return curX == (dim->GetSizeX() - 1);
}

bool PaddedMatrixIt::IsTopBorderY() {
    return curY == 0;
}

bool PaddedMatrixIt::IsBottomBorderY() {
    return curY == (dim->GetSizeY() - 1);
}

int PaddedMatrixIt::GetCurX() const {
    return curX;
}

int PaddedMatrixIt::GetCurY() const {
    return curY;
}

int PaddedMatrixIt::GetCurZ() const {
    return curZ;
}

int PaddedMatrixIt::GetPosition() const {
    return curBuffI;
}

int PaddedMatrixIt::GetIndex() const {
    return curI;
}
