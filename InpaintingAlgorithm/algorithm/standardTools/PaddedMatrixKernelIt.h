//
// Created by Jonathan on 11/03/2016.
//

#ifndef INPAINTINGALGORITHM_VOLUMEINNERITERATOR_H
#define INPAINTINGALGORITHM_VOLUMEINNERITERATOR_H

#include <cmath>

#include "PaddedMatrixIt.h"

class PaddedMatrixKernelIt {
protected:
    PaddedMatrixDim const * const outDim, * const inDim;

    int curBuffI;

    int curI, endI;
    int curInZ, curInY, curInX;

    int backStepFromCenter;
public:
    PaddedMatrixKernelIt(PaddedMatrixDim const * const outerDimension, PaddedMatrixDim const * const innerDimension);

    void Start(int centerBuffI);
    bool Next();

    int GetPosition();
    int GetIndex();

    int GetRelX();
    int GetRelY();
    int GetRelZ();
};


#endif //INPAINTINGALGORITHM_VOLUMEINNERITERATOR_H
