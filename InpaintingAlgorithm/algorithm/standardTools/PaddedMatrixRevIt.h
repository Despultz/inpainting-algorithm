//
// Created by Jonathan on 15/03/2016.
//

#ifndef INPAINTINGALGORITHM_BACKWARDVOLUMEITERATOR_H
#define INPAINTINGALGORITHM_BACKWARDVOLUMEITERATOR_H

#include "PaddedMatrixIt.h"

class PaddedMatrixRevIt : public PaddedMatrixIt {
public:
    PaddedMatrixRevIt(PaddedMatrixDim * dim);
    PaddedMatrixRevIt(PaddedMatrixDim * dim, int startAt, int stopAt);

    virtual bool Next();
    virtual bool Next(int leapX, int leapY);
};

#endif //INPAINTINGALGORITHM_BACKWARDVOLUMEITERATOR_H
