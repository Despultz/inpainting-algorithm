//
// Created by Jonathan on 25/02/2016.
//

#include "PaddedMatrix.h"

template <class T> PaddedMatrix<T>::PaddedMatrix(PaddedMatrixDim const * const dim)
    : dim(dim)
{
    dataVolume = new T[this->dim->GetBuffVolume()];
}

template <class T> PaddedMatrix<T>::~PaddedMatrix(){
    delete [] dataVolume;
}

template <class T> PaddedMatrixDim const * const PaddedMatrix<T>::GetDimension(){
    return dim;
}

template <class T> T* PaddedMatrix<T>::GetData(){
    return dataVolume;
}

template <class T> T PaddedMatrix<T>::Get(int x, int y, int z){
    return dataVolume[dim->DeterminePosition(x, y, z)];
}

template <class T> void PaddedMatrix<T>::Set(int x, int y, int z, T value){
    dataVolume[dim->DeterminePosition(x, y, z)] = value;
}

template <class T> void PaddedMatrix<T>::FillBufferWith(T value) {
    T* buffPointer = GetData();

    for(int i = 0; i < dim->GetVideoBuffer(); ++i){
        *(buffPointer++) = value;
    }

    for(int i = 0; i < dim->GetSizeZ(); ++i) {
        for(int j = 0; j < dim->GetFrameBuffer(); ++j){
            *(buffPointer++) = value;
        }

        for(int j = 0; j < dim->GetSizeY(); ++j){
            for(int k = 0; k < dim->GetBuffX(); ++k){
                *(buffPointer++) = value;
            }

            buffPointer += dim->GetSizeX();

            for(int k = 0; k < dim->GetBuffX(); ++k){
                *(buffPointer++) = value;
            }
        }

        for(int j = 0; j < dim->GetFrameBuffer(); ++j){
            *(buffPointer++) = value;
        }
    }

    for(int i = 0; i < dim->GetVideoBuffer(); ++i){
        *(buffPointer++) = value;
    }
}

template <class T> void PaddedMatrix<T>::MirrorIntoBuffer(){
    // TODO : More generalized way of doing this? Like... Cuter loops?
    int frameStart = dim->GetVideoBuffer();
    for(int i = 0; i < dim->GetSizeZ(); ++i){
        // Top left corner
        T* buffPointer = GetData() + frameStart;
        T* dataPointer = GetData() + frameStart + dim->GetFrameBuffer() + dim->GetBuffX() * 2 + (dim->GetBuffY() * dim->GetBuffSizeX());

        for(int buffY = 0; buffY < dim->GetBuffY(); ++buffY){
            for(int buffX = 0; buffX < dim->GetBuffX(); ++buffX)
                *(buffPointer + buffX) = *(dataPointer - buffX);

            buffPointer += dim->GetBuffSizeX();
            dataPointer -= dim->GetBuffSizeX();
        }

        // Top rectangle
        buffPointer = GetData() + frameStart + dim->GetBuffX();
        dataPointer = GetData() + frameStart + dim->GetFrameBuffer() + dim->GetBuffX() + (dim->GetBuffY() * dim->GetBuffSizeX());

        for(int buffY = 0; buffY < dim->GetBuffY(); ++buffY){
            for(int buffX = 0; buffX < dim->GetSizeX(); ++buffX)
                *(buffPointer + buffX) = *(dataPointer + buffX);

            buffPointer += dim->GetBuffSizeX();
            dataPointer -= dim->GetBuffSizeX();
        }

        // Top right corner
        buffPointer = GetData() + frameStart + dim->GetSizeX() + 2 * dim->GetBuffY() - 1;
        dataPointer = GetData() + frameStart + dim->GetFrameBuffer() + dim->GetBuffY() * dim->GetBuffSizeX() + dim->GetSizeX() - 1;

        for(int buffY = 0; buffY < dim->GetBuffY(); ++buffY){
            for(int buffX = 0; buffX < dim->GetBuffX(); ++buffX)
                *(buffPointer - buffX) = *(dataPointer + buffX);

            buffPointer += dim->GetBuffSizeX();
            dataPointer -= dim->GetBuffSizeX();
        }

        // Left middle rectangle
        buffPointer = GetData() + frameStart + dim->GetFrameBuffer();
        dataPointer = GetData() + frameStart + dim->GetFrameBuffer() + 2 * dim->GetBuffX();

        for(int buffY = 0; buffY < dim->GetSizeY(); ++buffY){
            for(int buffX = 0; buffX < dim->GetBuffX(); ++buffX)
                *(buffPointer + buffX) = *(dataPointer - buffX);

            buffPointer += dim->GetBuffSizeX();
            dataPointer += dim->GetBuffSizeX();
        }

        // Right middle rectangle
        buffPointer = GetData() + frameStart + dim->GetFrameBuffer() + dim->GetBuffSizeX() - 1;
        dataPointer = GetData() + frameStart + dim->GetFrameBuffer() + dim->GetSizeX() - 1;

        for(int buffY = 0; buffY < dim->GetSizeY(); ++buffY){
            for(int buffX = 0; buffX < dim->GetBuffX(); ++buffX)
                *(buffPointer - buffX) = *(dataPointer + buffX);

            buffPointer += dim->GetBuffSizeX();
            dataPointer += dim->GetBuffSizeX();
        }

        // Bottom left corner
        buffPointer = GetData() + frameStart + dim->GetBuffAreaXY() - dim->GetBuffSizeX();
        dataPointer = GetData() + frameStart + dim->GetFrameBuffer() + dim->GetBuffX() * 2 + ((dim->GetSizeY() - dim->GetBuffX() - 1) * dim->GetBuffSizeX());

        for(int buffY = 0; buffY < dim->GetBuffY(); ++buffY){
            for(int buffX = 0; buffX < dim->GetBuffX(); ++buffX)
                *(buffPointer + buffX) = *(dataPointer - buffX);

            buffPointer -= dim->GetBuffSizeX();
            dataPointer += dim->GetBuffSizeX();
        }

        // Bottom rectangle
        buffPointer = GetData() + frameStart + dim->GetBuffAreaXY() - dim->GetBuffX() - dim->GetSizeX();
        dataPointer = GetData() + frameStart + dim->GetFrameBuffer() + ((dim->GetSizeY() - dim->GetBuffX() - 1) * dim->GetBuffSizeX()) + dim->GetBuffX();

        for(int buffY = 0; buffY < dim->GetBuffY(); ++buffY){
            for(int buffX = 0; buffX < dim->GetSizeX(); ++buffX)
                *(buffPointer + buffX) = *(dataPointer + buffX);

            buffPointer -= dim->GetBuffSizeX();
            dataPointer += dim->GetBuffSizeX();
        }

        // Bottom right corner
        buffPointer = GetData() + frameStart + dim->GetBuffAreaXY() - 1;
        dataPointer = GetData() + frameStart + dim->GetFrameBuffer() + ((dim->GetSizeY() - dim->GetBuffX() - 1) * dim->GetBuffSizeX()) + dim->GetSizeX() - 1 ;

        for(int buffY = 0; buffY < dim->GetBuffY(); ++buffY){
            for(int buffX = 0; buffX < dim->GetBuffX(); ++buffX)
                *(buffPointer - buffX) = *(dataPointer + buffX);

            buffPointer -= dim->GetBuffSizeX();
            dataPointer += dim->GetBuffSizeX();
        }

        frameStart += dim->GetBuffAreaXY();
    }

    T* startVideoBuff = GetData();
    T* endVideoBuff = GetData() + dim->GetBuffVolume() - 1;
    for(int i = 0; i < dim->GetBuffZ(); ++i){
        int delta = 2 * (dim->GetBuffZ() - i) * dim->GetBuffAreaXY();
        for(int j = 0; j < dim->GetBuffAreaXY(); ++j){
            *startVideoBuff = *(startVideoBuff + delta);
            *endVideoBuff = *(endVideoBuff - delta);
            startVideoBuff++;
            endVideoBuff--;
        }
    }
}

// Specify the specializations of the templated class that will be required.
template class PaddedMatrix<float>;
template class PaddedMatrix<int>;