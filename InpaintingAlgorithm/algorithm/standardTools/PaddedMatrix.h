//
// Created by Jonathan on 25/02/2016.
//

#ifndef INPAINTINGALGORITHM_CHANNELVOLUME_H
#define INPAINTINGALGORITHM_CHANNELVOLUME_H

#include "PaddedMatrixDim.h"
#include "PaddedMatrixIt.h"

template <class T> class PaddedMatrix {
private:
    T* dataVolume;
    PaddedMatrixDim const * const dim;

public:
    PaddedMatrix(PaddedMatrixDim const * const dim);

    ~PaddedMatrix();

    T* GetData();
    T Get(int x, int y, int z);
    void Set(int x, int y, int z, T value);

    void MirrorIntoBuffer();
    void FillBufferWith(T value);

    PaddedMatrixDim const * const GetDimension();
};


#endif //INPAINTINGALGORITHM_CHANNELVOLUME_H
