//
// Created by Jonathan on 01/03/2016.
//

#include "PaddedMatrixDim.h"

PaddedMatrixDim::PaddedMatrixDim(int sizeX, int sizeY, int sizeZ)
        : PaddedMatrixDim(sizeX, sizeY, sizeZ, 0, 0, 0)
{}

PaddedMatrixDim::PaddedMatrixDim(int sizeX, int sizeY, int sizeZ, int buffX, int buffY, int buffZ)
    : sizeX(sizeX), sizeY(sizeY), sizeZ(sizeZ),
      buffX(buffX), buffY(buffY), buffZ(buffZ),
      areaXY(this->sizeX * this->sizeY), volume(areaXY * this->sizeZ),
      buffSizeX(this->sizeX + 2 * this->buffX), buffSizeY(this->sizeY + 2 * this->buffY), buffSizeZ(this->sizeZ + 2 * this->buffZ),
      buffAreaXY(this->buffSizeX * this->buffSizeY), buffVolume(this->buffAreaXY * this->buffSizeZ),
      frameBuffer(this->buffSizeX * this->buffY), videoBuffer(this->buffAreaXY * this->buffZ),
      halfSizeXBefore((int)floorf((sizeX-1)/2.0f)), halfSizeYBefore((int)floorf((sizeY-1)/2.0f)), halfSizeZBefore((int)floorf((sizeZ-1)/2.0f)),
      halfSizeXAfter((int)floorf(sizeX/2.0f)), halfSizeYAfter((int)floorf(sizeY/2.0f)), halfSizeZAfter((int)floorf(sizeZ/2.0f))
{}

int PaddedMatrixDim::DeterminePosition(int x, int y, int z) const{
    int position = z * buffAreaXY + frameBuffer + videoBuffer;
    position += y * buffSizeX + buffX;
    position += x;

    return position;
}

void PaddedMatrixDim::DetailPosition(int i, int &x, int &y, int &z) const{
    z = i / areaXY;
    y = (i - z * areaXY) / sizeX;
    x = i - (z * areaXY) - (y * sizeX);
}