//
// Created by Jonathan on 07/03/2016.
//

#ifndef INPAINTINGALGORITHM_VOLUMEITERATOR_H
#define INPAINTINGALGORITHM_VOLUMEITERATOR_H

#include "PaddedMatrixDim.h"

class PaddedMatrixIt {
protected:
    PaddedMatrixDim const * const dim;

    int curBuffI; // Points to the dataSources directly.

    int curI;
    int endI;

    int curZ;
    int curY;
    int curX;

public:
    PaddedMatrixIt(PaddedMatrixDim const * const dim);
    PaddedMatrixIt(PaddedMatrixDim const * const dim, int startAt, int stopAt);

    virtual bool Next();
    virtual bool Next(int leapX, int leapY);

    virtual void Start(int const startAt = 0);
    virtual void Start(int const startAt, int const endAt);

    bool IsLeftBorderX();
    bool IsRightBorderX();
    bool IsTopBorderY();
    bool IsBottomBorderY();

    int GetPosition() const;
    int GetIndex() const;

    int GetCurX() const;
    int GetCurY() const;
    int GetCurZ() const;
};


#endif //INPAINTINGALGORITHM_VOLUMEITERATOR_H
