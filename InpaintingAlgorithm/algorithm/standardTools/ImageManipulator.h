//
// Created by Jonathan on 23/02/2016.
//

#ifndef ACCELERATEDALGORITHMS_IMAGEMANIPULATOR_H
#define ACCELERATEDALGORITHMS_IMAGEMANIPULATOR_H

#include <cmath>
#include "PaddedMatrix.h"
#include "PaddedMatrixKernelIt.h"

class ImageManipulator {
public:
    static double *GaussianFilter(double sigma, int sizeX, int sizeY);

    static PaddedMatrix<float> GrayScaleFromRGB(PaddedMatrix<float> *redMat, PaddedMatrix<float> *blueMat, PaddedMatrix<float> *grnMat);

    static void CalculateCasellesDescriptors(PaddedMatrixDim * const dim, PaddedMatrix<float> * const redMatrix,
                                             PaddedMatrix<float> * const greenMatrix, PaddedMatrix<float> * const blueMatrix,
                                             PaddedMatrix<float> * const occMatrix, PaddedMatrix<float> * const gradXMatrix,
                                             PaddedMatrix<float> * const gradYMatrix, int const averagingAreaSide);

    template <class T> static PaddedMatrix<T> ImageFilter(PaddedMatrix<T> * const sourceMatrix, PaddedMatrix<float> * const filterMatrix);
};


#endif //ACCELERATEDALGORITHMS_IMAGEMANIPULATOR_H
