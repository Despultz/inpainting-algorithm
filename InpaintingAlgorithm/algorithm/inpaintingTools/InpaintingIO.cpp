//
// Created by Jonathan on 5/5/2016.
//

#include "InpaintingIO.h"

template <class T> void InpaintingIO::LoadMatrixFromCSV(std::string csvFilename, PaddedMatrix<T> *loadedMatrix, float multiplyBy){
    std::ifstream fileStream(csvFilename);
    std::string line;

    if(fileStream.good())
    {
        std::stringstream lineStream;
        std::string value;

        PaddedMatrixIt matIt = PaddedMatrixIt(loadedMatrix->GetDimension());
        bool itOk = true;

        while(std::getline(fileStream, line) && itOk){
            lineStream << line;
            while(std::getline(lineStream, value, ',') && itOk){
                loadedMatrix->GetData()[matIt.GetPosition()] = std::stof(value) * multiplyBy;
                itOk = matIt.Next();
            }
            lineStream.clear();
        }
    }
}

template void InpaintingIO::LoadMatrixFromCSV<float>(std::string csvFilename, PaddedMatrix<float> *loadedMatrix, float multiplyBy);
template void InpaintingIO::LoadMatrixFromCSV<int>(std::string csvFilename, PaddedMatrix<int> *loadedMatrix, float multiplyBy);

template <class T> void InpaintingIO::LoadMatrixFromBin(std::string binFilename, PaddedMatrix<T> *loadedMatrix, float multiplyBy){
    ::std::ifstream fileStream(binFilename, ::std::ios::in | ::std::ios::binary | ::std::ios::ate);

    long long int size = fileStream.tellg();
    char * fileBytes = new char[size];
    fileStream.seekg(0, ::std::ios::beg);
    fileStream.read(fileBytes, size);
    fileStream.close();

    // Verify we have all the required data for the matrix, then proceed to loading
    int unitSize = sizeof(T);
    if(unitSize * loadedMatrix->GetDimension()->GetVolume() == size){
        T* fileData = (T*)fileBytes;
        T* matData = loadedMatrix->GetData();
        PaddedMatrixIt matIt = PaddedMatrixIt(loadedMatrix->GetDimension());
        do{
            matData[matIt.GetPosition()] = fileData[matIt.GetIndex()] * multiplyBy;
        }while(matIt.Next());
    }
}

template void InpaintingIO::LoadMatrixFromBin<float>(std::string binFilename, PaddedMatrix<float> *loadedMatrix, float multiplyBy);
template void InpaintingIO::LoadMatrixFromBin<int>(std::string binFilename, PaddedMatrix<int> *loadedMatrix, float multiplyBy);