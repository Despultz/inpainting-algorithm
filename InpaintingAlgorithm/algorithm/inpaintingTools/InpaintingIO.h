//
// Created by Jonathan on 29/02/2016.
//

#ifndef INPAINTINGALGORITHM_INPAINTINGHELPER_H
#define INPAINTINGALGORITHM_INPAINTINGHELPER_H

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include "../standardTools/PaddedMatrix.h"

class InpaintingIO {
public:
    static const int IH_DBG_BUFF = 0x01;
    static const int IH_DBG_SHOW = 0x02;
    static const int IH_DBG_SAVE = 0x04;
    static const int IH_DBG_SCAL = 0x08;

public:
    virtual void GetVideoDimensions(std::string videoFilename, int& widthX, int& heightY, int& framesZ) = 0;

    virtual void LoadRGBMatrixFromVideo(std::string videoFilename, PaddedMatrix<float> *redMatrix,
                                        PaddedMatrix<float> *greenMatrix, PaddedMatrix<float> *blueMatrix) = 0;

    virtual void LoadOcclusionMatrixFromImage(std::string imageFilename, PaddedMatrix<float> *occMatrix) = 0;

    virtual void GeneralIOForRGB(PaddedMatrix<float> *redMatrix, PaddedMatrix<float> *greenMatrix,
                                 PaddedMatrix<float> *blueMatrix,
                                 int frameNo, int options = IH_DBG_SHOW, std::string saveFilename = "debugFrame") = 0;

    virtual void GeneralIOForMatrix(PaddedMatrix<float> *matrix, int frameNo, int options = IH_DBG_SHOW,
                                    const std::string saveFilename = "debugFrame") = 0;

    template <class T> void LoadMatrixFromCSV(std::string csvFilename, PaddedMatrix<T> *loadedMatrix, float multiplyBy = 1.0f/255.0f);

    template <class T> void LoadMatrixFromBin(std::string binFilename, PaddedMatrix<T> *loadedMatrix, float multiplyBy = 1.0f/255.0f);
};

#endif //INPAINTINGALGORITHM_INPAINTINGHELPER_H
