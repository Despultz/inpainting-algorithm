//
// Created by Jonathan on 25/02/2016.
//

#include "InpaintingOpenCVIO.h"

InpaintingOpenCVIO::InpaintingOpenCVIO() : InpaintingIO(){}

void InpaintingOpenCVIO::GetVideoDimensions(std::string videoFilename, int &widthX, int &heightY, int &framesZ){
    cv::VideoCapture cap(videoFilename);

    if (!cap.isOpened())
        return;

    widthX = (int)cap.get(CV_CAP_PROP_FRAME_WIDTH);
    heightY = (int)cap.get(CV_CAP_PROP_FRAME_HEIGHT);
    framesZ = (int)cap.get(CV_CAP_PROP_FRAME_COUNT);

    return;
}

void InpaintingOpenCVIO::LoadRGBMatrixFromVideo(std::string videoFilename, PaddedMatrix<float> *redMatrix,
                                                PaddedMatrix<float> *greenMatrix, PaddedMatrix<float> *blueMatrix){
    cv::VideoCapture cap(videoFilename);

    if (!cap.isOpened())
        return;

    cv::Mat frame;
    int frameCount = 0;
    while(cap.read(frame)){
        int cn = frame.channels();
        for(int i = 0; i < frame.rows; i++){
            for(int j = 0; j < frame.cols; j++){
                int position = i * frame.cols * cn + j * cn;
                redMatrix->Set(j, i, frameCount, frame.data[position + 2] / 255.0f);
                greenMatrix->Set(j, i, frameCount, frame.data[position + 1] / 255.0f);
                blueMatrix->Set(j, i, frameCount, frame.data[position + 0] / 255.0f);
            }
        }
        ++frameCount;
    }

    return;
}

void InpaintingOpenCVIO::LoadOcclusionMatrixFromImage(std::string imageFilename, PaddedMatrix<float> *occMatrix){
    cv::Mat image;
    image = cv::imread(imageFilename, CV_LOAD_IMAGE_COLOR);
    int cn = image.channels();

    for(int i = 0; i < image.rows; i++){
        for(int j = 0; j < image.cols; j++){
            int position = i * image.cols * cn + j * cn;
            int value = 0;
            if(image.data[position + 0] > 0 || image.data[position + 1] > 0 || image.data[position + 2] > 0)
                value = 1;

            for(int k = 0; k < occMatrix->GetDimension()->GetSizeZ(); ++k)
                occMatrix->Set(j, i, k, value);
        }
    }
}

// TODO Makes this call another generic function instead of this repetition. No need to be efficient, these methods are for debugging.
void InpaintingOpenCVIO::GeneralIOForRGB(PaddedMatrix<float> *redMatrix, PaddedMatrix<float> *greenMatrix,
                                         PaddedMatrix<float> *blueMatrix,
                                         int frameI, int options, std::string saveFilename)
{
    cv::Mat frame;
    PaddedMatrixDim const * const dim = redMatrix->GetDimension(); // All matrices are same size.

    float* blueData = blueMatrix->GetData();
    float* greenData = greenMatrix->GetData();
    float* redData = redMatrix->GetData();

    if(options & IH_DBG_BUFF){
        frame = cv::Mat(dim->GetBuffSizeY(), dim->GetBuffSizeX(), CV_32FC3);
        float * frameData = reinterpret_cast<float *>(frame.data);

        int startIndex = dim->GetBuffAreaXY() * frameI;
        int stopIndex = startIndex + dim->GetBuffAreaXY();

        int frameDataIndex = 0;
        for(int i = startIndex; i < stopIndex; ++i){
            frameData[frameDataIndex * 3 + 0] = blueData[i];
            frameData[frameDataIndex * 3 + 1] = greenData[i];
            frameData[frameDataIndex * 3 + 2] = redData[i];
            frameDataIndex++;
        }
    }
    else{
        frame = cv::Mat(dim->GetSizeY(), dim->GetSizeX(), CV_32FC3);
        float * frameData = reinterpret_cast<float *>(frame.data);

        int startIndex = dim->GetAreaXY() * frameI;
        int stopIndex = startIndex + dim->GetAreaXY();

        int frameDataIndex = 0;
        PaddedMatrixIt volIt = PaddedMatrixIt(dim, startIndex, stopIndex);
        do{
            frameData[frameDataIndex * 3 + 0] = blueData[volIt.GetPosition()];
            frameData[frameDataIndex * 3 + 1] = greenData[volIt.GetPosition()];
            frameData[frameDataIndex * 3 + 2] = redData[volIt.GetPosition()];
            frameDataIndex++;
        }while(volIt.Next());
    }

    ProcessOptions(frame, 3, options, saveFilename);
}

void InpaintingOpenCVIO::GeneralIOForMatrix(PaddedMatrix<float> *matrix, int frameNo, int options,
                                            const std::string saveFilename) {
    cv::Mat frame;
    PaddedMatrixDim const * const dim = matrix->GetDimension();
    float* singleData = matrix->GetData();

    if(options & IH_DBG_BUFF){
        frame = cv::Mat(dim->GetBuffSizeY(), dim->GetBuffSizeX(), CV_32FC1);
        float* frameData = reinterpret_cast<float*>(frame.data);

        int startIndex = dim->GetBuffAreaXY() * frameNo;
        int stopIndex = startIndex + dim->GetBuffAreaXY();


        int frameDataIndex = 0;
        for(int i = startIndex; i < stopIndex; ++i){
            frameData[frameDataIndex] = singleData[i];
            frameDataIndex++;
        }
    }
    else{
        frame = cv::Mat(dim->GetSizeY(), dim->GetSizeX(), CV_32FC1);
        float* frameData = reinterpret_cast<float*>(frame.data);

        int startIndex = dim->GetAreaXY() * frameNo;
        int stopIndex = startIndex + dim->GetAreaXY();

        int frameDataIndex = 0;
        PaddedMatrixIt volIt = PaddedMatrixIt(dim, startIndex, stopIndex);
        do{
            frameData[frameDataIndex] = singleData[volIt.GetPosition()];
            frameDataIndex++;
        }while(volIt.Next());
    }

    ProcessOptions(frame, 1, options, saveFilename);
}

void InpaintingOpenCVIO::ProcessOptions(cv::Mat frame, int channelCount, int options, std::string filename) {
    double min = 0, max = 1;
    if(options & IH_DBG_SCAL) cv::minMaxIdx(frame, &min, &max);

    cv::Mat adjFrame;
    double incrementAlpha = 65535 / (max-min);
    double zeroBeta = min * incrementAlpha;
    frame.convertTo(adjFrame, channelCount==1?CV_16UC1:CV_16UC3, incrementAlpha, -zeroBeta);
    frame = adjFrame;

    if(options & IH_DBG_SAVE){
        std::string fullFilename = saveFolder + filename + saveExtension;
        cv::imwrite(fullFilename, frame, writeOptions);
    }

    if(options & IH_DBG_SHOW){
        cv::imshow(windowName, frame);
        cv::waitKey(0);
    }
}
