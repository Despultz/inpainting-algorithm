//
// Created by Jonathan on 01/04/2016.
//

#include "InpaintingProfiler.h"

InpaintingProfiler::InpaintingProfiler() {
}

void InpaintingProfiler::BeforeInpainting() {
    StartSection("Inpainting", inpaintingClock);
}

void InpaintingProfiler::AfterInpainting() {
    EndSection("Inpainting", inpaintingClock);
}

void InpaintingProfiler::BeforeDownsampling() {
    StartSection("Downsampling", scalingClock);
}

void InpaintingProfiler::AfterDownsampling() {
    EndSection("Downsampling", scalingClock);
}

void InpaintingProfiler::BeforeCaselles() {
    StartSection("Caselles", casellesClock);
}

void InpaintingProfiler::AfterCaselles() {
    EndSection("Caselles", casellesClock);
}

void InpaintingProfiler::BeforeInpaintLevel(int level) {
    StartSection("InpaintLevel_L" + std::to_string(level), inpaintLevelClock);
}

void InpaintingProfiler::AfterInpaintLevel(int level) {
    EndSection("InpaintLevel_L" + std::to_string(level), inpaintLevelClock);
}

void InpaintingProfiler::BeforeInterpolate(int level) {
    StartSection("Interpolate_L" + std::to_string(level), interpolateClock);
}

void InpaintingProfiler::AfterInterpolate(int level) {
    EndSection("Interpolate_L" + std::to_string(level), interpolateClock);
}

void InpaintingProfiler::BeforeInpaintingIterations(int level) {
    StartSection("InpaintingIterations_L" + std::to_string(level), inpaintingIterationsClock);
}

void InpaintingProfiler::AfterInpaintingIterations(int level) {
    EndSection("InpaintingIterations_L" + std::to_string(level), inpaintingIterationsClock);
}

void InpaintingProfiler::BeforeInpaintingIteration(int level, int iteration) {
    StartSection("InpaintingIteration_L" + std::to_string(level) + "I" + std::to_string(iteration), inpaintingIterationClock);
}

void InpaintingProfiler::AfterInpaintingIteration(int level, int iteration) {
    EndSection("InpaintingIteration_L" + std::to_string(level) + "I" + std::to_string(iteration), inpaintingIterationClock);
}

void InpaintingProfiler::BeforeReconstruct(int level, int iteration) {
    StartSection("Reconstruct_L" + std::to_string(level) + "I" + std::to_string(iteration), reconstructClock);
}

void InpaintingProfiler::AfterReconstruct(int level, int iteration) {
    EndSection("Reconstruct_L" + std::to_string(level) + "I" + std::to_string(iteration), reconstructClock);
}

void InpaintingProfiler::BeforeSearch(int level, int iteration) {
    StartSection("Search_L" + std::to_string(level) + "I" + std::to_string(iteration), searchClock);
}

void InpaintingProfiler::AfterSearch(int level, int iteration) {
    EndSection("Search_L" + std::to_string(level) + "I" + std::to_string(iteration), searchClock);
}

void InpaintingProfiler::BeforeInitialisation() {
    StartSection("Initialisation", initialisationClock);
}

void InpaintingProfiler::AfterInitialisation() {
    EndSection("Initialisation", initialisationClock);
}

void InpaintingProfiler::BeforeInitialisationPeel(int peel) {
    StartSection("Initialisation_P" + std::to_string(peel), initialisationPeelClock);
}

void InpaintingProfiler::AfterInitialisationPeel(int peel) {
    EndSection("Initialisation_P" + std::to_string(peel), initialisationPeelClock);
}

void InpaintingProfiler::StartSection(std::string  sectionName, std::chrono::steady_clock::time_point &sectionClock) {
    for(int i = 0; i < sectionDepth; ++i)
        std::cout << " "; // Indenting each section.

    std::cout << "Start of section <" << sectionName << ">" << std::endl;
    sectionClock = std::chrono::steady_clock::now();

    sectionDepth++;
}

double InpaintingProfiler::EndSection(std::string sectionName, std::chrono::steady_clock::time_point &sectionClock) {
    sectionDepth--;

    for(int i = 0; i < sectionDepth; ++i)
        std::cout << " "; // Indenting each section.

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    double timeDiffSecs = (std::chrono::duration_cast<std::chrono::nanoseconds> (end - sectionClock).count()) * pow(10,-9);

    std::cout << "End of section <" << sectionName << "> " << timeDiffSecs << " s" << std::endl;

    return timeDiffSecs;
}
