//
// Created by Jonathan on 01/04/2016.
//

#ifndef INPAINTINGALGORITHM_INPAINTINGPROFILER_H
#define INPAINTINGALGORITHM_INPAINTINGPROFILER_H

#include <math.h>
#include <chrono>
#include <iostream>
#include <string.h>

class InpaintingProfiler {
private:
    std::chrono::steady_clock::time_point inpaintingClock;
    std::chrono::steady_clock::time_point scalingClock;
    std::chrono::steady_clock::time_point casellesClock;
    std::chrono::steady_clock::time_point inpaintLevelClock;
    std::chrono::steady_clock::time_point interpolateClock;
    std::chrono::steady_clock::time_point inpaintingIterationsClock;
    std::chrono::steady_clock::time_point inpaintingIterationClock;
    std::chrono::steady_clock::time_point reconstructClock;
    std::chrono::steady_clock::time_point searchClock;
    std::chrono::steady_clock::time_point initialisationClock;
    std::chrono::steady_clock::time_point initialisationPeelClock;

    int sectionDepth = 0;

public:
    InpaintingProfiler();

    void BeforeInpainting();
    void AfterInpainting();

    void BeforeDownsampling();
    void AfterDownsampling();

    void BeforeCaselles();
    void AfterCaselles();

    void BeforeInpaintLevel(int level);
    void AfterInpaintLevel(int level);

    void BeforeInterpolate(int level);
    void AfterInterpolate(int level);

    void BeforeInpaintingIterations(int level);
    void AfterInpaintingIterations(int level);

    void BeforeInpaintingIteration(int level, int iteration);
    void AfterInpaintingIteration(int level, int iteration);

    void BeforeReconstruct(int level, int iteration);
    void AfterReconstruct(int level, int iteration);

    void BeforeSearch(int level, int iteration);
    void AfterSearch(int level, int iteration);

    void BeforeInitialisation();
    void AfterInitialisation();

    void BeforeInitialisationPeel(int peel);
    void AfterInitialisationPeel(int peel);

private:
    void StartSection(std::string sectionName, std::chrono::steady_clock::time_point &sectionClock);
    double EndSection(std::string sectionName, std::chrono::steady_clock::time_point &sectionClock);
};


#endif //INPAINTINGALGORITHM_INPAINTINGPROFILER_H
