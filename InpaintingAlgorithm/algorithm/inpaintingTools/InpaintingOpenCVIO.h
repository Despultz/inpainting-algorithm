//
// Created by Jonathan on 25/02/2016.
//

#ifndef INPAINTINGALGORITHM_INPAINTINGOPENCVHELPER_H
#define INPAINTINGALGORITHM_INPAINTINGOPENCVHELPER_H

#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

#include "../standardTools/PaddedMatrix.h"
#include "InpaintingIO.h"

class InpaintingOpenCVIO : public InpaintingIO {
private:
    const std::string windowName = "OpenCV Helper Window";
    const std::string saveFolder = "D:\\temp\\";
    const std::string saveExtension = ".png";
    const std::vector<int> writeOptions = {CV_IMWRITE_PNG_COMPRESSION, 0};

public:
    InpaintingOpenCVIO();

    void GetVideoDimensions(std::string videoFilename, int& widthX, int& heightY, int& framesZ);

    void LoadRGBMatrixFromVideo(std::string videoFilename, PaddedMatrix<float> *redMatrix,
                                PaddedMatrix<float> *greenMatrix, PaddedMatrix<float> *blueMatrix);

    void LoadOcclusionMatrixFromImage(std::string imageFilename, PaddedMatrix<float> *occMatrix);

    void GeneralIOForRGB(PaddedMatrix<float> *redMatrix, PaddedMatrix<float> *greenMatrix,
                         PaddedMatrix<float> *blueMatrix,
                         int frameNo, int options = IH_DBG_SHOW, std::string saveFilename = "debugFrame");

    void GeneralIOForMatrix(PaddedMatrix<float> *matrix, int frameNo, int options = IH_DBG_SHOW,
                            const std::string saveFilename = "debugFrame");

private:
    void ProcessOptions(cv::Mat frame, int channelCount, int options, std::string filename);
};


#endif //INPAINTINGALGORITHM_INPAINTINGOPENCVHELPER_H
