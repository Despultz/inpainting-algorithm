//
// Created by Jonathan on 13/04/2016.
//

#ifndef INPAINTINGALGORITHMRUN_INPAINTINGSEARCHER_H
#define INPAINTINGALGORITHMRUN_INPAINTINGSEARCHER_H

#include "CommonInpainting.h"
#include "dataSources/InpaintingDataSource.h"
#include "InpaintingEvaluator.h"

class InpaintingSearcher {
public:
    InpaintingSearcher(){};
    virtual ~InpaintingSearcher(){}

    virtual void Search(InpaintingDataSource * const iDataProvider, bool hasInvalidPixels) = 0;
};

#endif //INPAINTINGALGORITHMRUN_INPAINTINGSEARCHER_H
