//
// Created by Jonathan on 13/04/2016.
//

#ifndef INPAINTINGALGORITHMRUN_COMMONRECONSTRUCTION_H
#define INPAINTINGALGORITHMRUN_COMMONRECONSTRUCTION_H

#include "../standardTools/PaddedMatrixDim.h"
#include "../standardTools/PaddedMatrix.h"

// All the different values for the Detailed OCClusion.
#define DOCC_BUFFER -1.0f
#define DOCC_NORMAL 0.0f
#define DOCC_NEARBORDER 0.5f
#define DOCC_NEAROCC 1.0f
#define DOCC_BORDER 2.0f
#define DOCC_BORDER_ALT 2.1f
#define DOCC_CORE 3.0f

class CommonInpainting {
public:
    static int IsLegalDisplacement(PaddedMatrix<float> *detailedOcc, PaddedMatrixDim *dim,
                                   int curX, int dispX, int curY, int dispY, int curZ, int dispZ);
};


#endif //INPAINTINGALGORITHMRUN_COMMONRECONSTRUCTION_H
