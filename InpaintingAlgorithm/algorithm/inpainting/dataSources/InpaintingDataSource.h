//
// Created by Jonathan on 5/17/2016.
//

#ifndef INPAINTINGALGORITHMRUN_INPAINTINGDATASOURCE_H
#define INPAINTINGALGORITHMRUN_INPAINTINGDATASOURCE_H

#include "../../standardTools/PaddedMatrixDim.h"
#include "../../standardTools/PaddedMatrix.h"

class InpaintingDataSource{
public:
    virtual PaddedMatrixDim * const GetDim() = 0;

    virtual PaddedMatrix<float> * const GetRed() = 0;
    virtual PaddedMatrix<float> * const GetBlue() = 0;
    virtual PaddedMatrix<float> * const GetGreen() = 0;

    virtual PaddedMatrix<float> * const GetOcc() = 0;

    virtual PaddedMatrix<float> * const GetGradX() = 0;
    virtual PaddedMatrix<float> * const GetGradY() = 0;

    virtual PaddedMatrix<int> * const GetDispX() = 0;
    virtual PaddedMatrix<int> * const GetDispY() = 0;
    virtual PaddedMatrix<int> * const GetDispZ() = 0;
    virtual PaddedMatrix<float> * const GetDispA() = 0;
};

#endif //INPAINTINGALGORITHMRUN_INPAINTINGDATASOURCE_H
