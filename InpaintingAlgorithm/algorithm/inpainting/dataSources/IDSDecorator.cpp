//
// Created by Jonathan on 5/17/2016.
//

#include "IDSDecorator.h"

IDSDecorator::IDSDecorator(InpaintingDataSource * const decoratedIDS) : decoratedIDS(decoratedIDS){}

IDSDecorator::~IDSDecorator(){}

PaddedMatrixDim * const IDSDecorator::GetDim(){
    return decoratedIDS->GetDim();
}

PaddedMatrix<float> * const IDSDecorator::GetRed(){
    return decoratedIDS->GetRed();
}

PaddedMatrix<float> * const IDSDecorator::GetBlue(){
    return decoratedIDS->GetBlue();
}

PaddedMatrix<float> * const IDSDecorator::GetGreen(){
    return decoratedIDS->GetGreen();
}

PaddedMatrix<float> * const IDSDecorator::GetOcc(){
    return decoratedIDS->GetOcc();
}

PaddedMatrix<float> * const IDSDecorator::GetGradX(){
    return decoratedIDS->GetGradX();
}

PaddedMatrix<float> * const IDSDecorator::GetGradY(){
    return decoratedIDS->GetGradY();
}

PaddedMatrix<int> * const IDSDecorator::GetDispX(){
    return decoratedIDS->GetDispX();
}

PaddedMatrix<int> * const IDSDecorator::GetDispY(){
    return decoratedIDS->GetDispY();
}

PaddedMatrix<int> * const IDSDecorator::GetDispZ(){
    return decoratedIDS->GetDispZ();
}

PaddedMatrix<float> * const IDSDecorator::GetDispA(){
    return decoratedIDS->GetDispA();
}