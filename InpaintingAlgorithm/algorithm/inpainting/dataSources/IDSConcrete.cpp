//
// Created by Jonathan on 5/17/2016.
//

#include "IDSConcrete.h"

IDSConcrete::IDSConcrete(PaddedMatrixDim * const dim,
                         PaddedMatrix<float> * const redMatrix, PaddedMatrix<float> * const greenMatrix,
                         PaddedMatrix<float> * const blueMatrix, PaddedMatrix<float> * const occMatrix,
                         PaddedMatrix<float> * const gradXMatrix, PaddedMatrix<float> * const gradYMatrix,
                         PaddedMatrix<int> * const dispXMatrix, PaddedMatrix<int> * const dispYMatrix,
                         PaddedMatrix<int> * const dispZMatrix, PaddedMatrix<float> * const dispAMatrix)
        : dim(dim), redMatrix(redMatrix), greenMatrix(greenMatrix), blueMatrix(blueMatrix), occMatrix(occMatrix),
          gradXMatrix(gradXMatrix), gradYMatrix(gradYMatrix),
          dispXMatrix(dispXMatrix), dispYMatrix(dispYMatrix), dispZMatrix(dispZMatrix), dispAMatrix(dispAMatrix)
{
}

IDSConcrete::IDSConcrete(PaddedMatrixDim * const dim,
                         PaddedMatrix<float> * const redMatrix, PaddedMatrix<float> * const greenMatrix,
                         PaddedMatrix<float> * const blueMatrix, PaddedMatrix<float> * const occMatrix)
: dim(dim), redMatrix(redMatrix), greenMatrix(greenMatrix), blueMatrix(blueMatrix), occMatrix(occMatrix),
  gradXMatrix(new PaddedMatrix<float>(dim)), gradYMatrix(new PaddedMatrix<float>(dim)),
  dispXMatrix(new PaddedMatrix<int>(dim)), dispYMatrix(new PaddedMatrix<int>(dim)), dispZMatrix(new PaddedMatrix<int>(dim)), dispAMatrix(new PaddedMatrix<float>(dim))
{
    // Calculate the gradients.
    ImageManipulator::CalculateCasellesDescriptors(dim,
                                                   redMatrix, greenMatrix, blueMatrix, occMatrix,
                                                   gradXMatrix, gradYMatrix,
                                                   (int)exp2f(3)); // TODO : The 3 comes from the downsampling, which is stupid. It's basically random. The whole number looks pretty random. Eh.

    InitialiseDisplacementMatrices();
}

IDSConcrete::IDSConcrete(PaddedMatrixDim * const dim)
: dim(dim), redMatrix(new PaddedMatrix<float>(dim)), greenMatrix(new PaddedMatrix<float>(dim)), blueMatrix(new PaddedMatrix<float>(dim)),
  occMatrix(new PaddedMatrix<float>(dim)), gradXMatrix(new PaddedMatrix<float>(dim)), gradYMatrix(new PaddedMatrix<float>(dim)),
  dispXMatrix(new PaddedMatrix<int>(dim)), dispYMatrix(new PaddedMatrix<int>(dim)), dispZMatrix(new PaddedMatrix<int>(dim)), dispAMatrix(new PaddedMatrix<float>(dim))
{
    InitialiseDisplacementMatrices();
}

IDSConcrete::~IDSConcrete(){
    delete dim;
    delete redMatrix, blueMatrix, greenMatrix, occMatrix, gradXMatrix, gradYMatrix,
           dispXMatrix, dispYMatrix, dispZMatrix, dispAMatrix;
}

PaddedMatrixDim * const IDSConcrete::GetDim(){
    return dim;
}

PaddedMatrix<float> * const IDSConcrete::GetRed(){
    return redMatrix;
}

PaddedMatrix<float> * const IDSConcrete::GetBlue(){
    return blueMatrix;
}

PaddedMatrix<float> * const IDSConcrete::GetGreen(){
    return greenMatrix;
}

PaddedMatrix<float> * const IDSConcrete::GetOcc(){
    return occMatrix;
}

PaddedMatrix<float> * const IDSConcrete::GetGradX(){
    return gradXMatrix;
}

PaddedMatrix<float> * const IDSConcrete::GetGradY(){
    return gradYMatrix;
}

PaddedMatrix<int> * const IDSConcrete::GetDispX(){
    return dispXMatrix;
}

PaddedMatrix<int> * const IDSConcrete::GetDispY(){
    return dispYMatrix;
}

PaddedMatrix<int> * const IDSConcrete::GetDispZ(){
    return dispZMatrix;
}

PaddedMatrix<float> * const IDSConcrete::GetDispA(){
    return dispAMatrix;
}

void IDSConcrete::InitialiseDisplacementMatrices(){
    // Give an initial value to the displacement matrices.
    PaddedMatrixIt matIt = PaddedMatrixIt(dim);
    do{
        dispXMatrix->GetData()[matIt.GetPosition()] = 0;
        dispYMatrix->GetData()[matIt.GetPosition()] = 0;
        dispZMatrix->GetData()[matIt.GetPosition()] = 0;
        dispAMatrix->GetData()[matIt.GetPosition()] = FLT_MAX;
    }while(matIt.Next());
}