//
// Created by Jonathan on 5/17/2016.
//

#ifndef INPAINTINGALGORITHMRUN_IDSCONCRETE_H
#define INPAINTINGALGORITHMRUN_IDSCONCRETE_H

#include <cfloat>
#include "../../standardTools/ImageManipulator.h"
#include "InpaintingDataSource.h"

class IDSConcrete : public InpaintingDataSource {
private:
    // The dataSources
    PaddedMatrixDim * const dim;
    PaddedMatrix<float> * const redMatrix, * const greenMatrix, * const blueMatrix;
    PaddedMatrix<float> * const occMatrix;
    PaddedMatrix<float> * const gradXMatrix, * const gradYMatrix;
    PaddedMatrix<int> * const dispXMatrix, * const dispYMatrix, * const dispZMatrix;
    PaddedMatrix<float> * const dispAMatrix;

public:
    IDSConcrete(PaddedMatrixDim * const dim,
                PaddedMatrix<float> * const redMatrix, PaddedMatrix<float> * const greenMatrix,
                PaddedMatrix<float> * const blueMatrix, PaddedMatrix<float> * const occMatrix,
                PaddedMatrix<float> * const gradXMatrix, PaddedMatrix<float> * const gradYMatrix,
                PaddedMatrix<int> * const dispXMatrix, PaddedMatrix<int> * const dispYMatrix,
                PaddedMatrix<int> * const dispZMatrix, PaddedMatrix<float> * const dispAMatrix);
    IDSConcrete(PaddedMatrixDim * const dim,
                PaddedMatrix<float> * const redMatrix, PaddedMatrix<float> * const greenMatrix,
                PaddedMatrix<float> * const blueMatrix, PaddedMatrix<float> * const occMatrix);
    IDSConcrete(PaddedMatrixDim * const dim);
    ~IDSConcrete();

    PaddedMatrixDim * const GetDim() override;

    PaddedMatrix<float> * const GetRed() override;
    PaddedMatrix<float> * const GetBlue() override;
    PaddedMatrix<float> * const GetGreen() override;

    PaddedMatrix<float> * const GetOcc() override;

    PaddedMatrix<float> * const GetGradX() override;
    PaddedMatrix<float> * const GetGradY() override;

    PaddedMatrix<int> * const GetDispX() override;
    PaddedMatrix<int> * const GetDispY() override;
    PaddedMatrix<int> * const GetDispZ() override;
    PaddedMatrix<float> * const GetDispA() override;

private:
    void InitialiseDisplacementMatrices();
};


#endif //INPAINTINGALGORITHMRUN_IDSCONCRETE_H
