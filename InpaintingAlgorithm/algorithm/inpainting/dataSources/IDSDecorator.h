//
// Created by Jonathan on 5/17/2016.
//

#ifndef INPAINTINGALGORITHMRUN_IDSDECORATOR_H
#define INPAINTINGALGORITHMRUN_IDSDECORATOR_H


#include "InpaintingDataSource.h"

class IDSDecorator : public InpaintingDataSource {
protected:
    InpaintingDataSource * const decoratedIDS;

public:
    IDSDecorator(InpaintingDataSource * const decoratedIDS);
    virtual ~IDSDecorator();

    virtual PaddedMatrixDim * const GetDim() override;

    virtual PaddedMatrix<float> * const GetRed() override;
    virtual PaddedMatrix<float> * const GetBlue() override;
    virtual PaddedMatrix<float> * const GetGreen() override;

    virtual PaddedMatrix<float> * const GetOcc() override;

    virtual PaddedMatrix<float> * const GetGradX() override;
    virtual PaddedMatrix<float> * const GetGradY() override;

    virtual PaddedMatrix<int> * const GetDispX() override;
    virtual PaddedMatrix<int> * const GetDispY() override;
    virtual PaddedMatrix<int> * const GetDispZ() override;
    virtual PaddedMatrix<float> * const GetDispA() override;
};


#endif //INPAINTINGALGORITHMRUN_IDSDECORATOR_H
