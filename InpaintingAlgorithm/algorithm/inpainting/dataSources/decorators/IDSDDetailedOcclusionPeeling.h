//
// Created by Jonathan on 5/18/2016.
//

#ifndef INPAINTINGALGORITHMRUN_IDSDDETAILEDOCCLUSIONPEELING_H
#define INPAINTINGALGORITHMRUN_IDSDDETAILEDOCCLUSIONPEELING_H

#include "IDSDDetailedOcclusion.h"
#include "../../CommonInpainting.h"

class IDSDDetailedOcclusionPeeling : public IDSDDetailedOcclusion {
private:
    float currentBorder = DOCC_BORDER_ALT;
    float previousBorder = DOCC_BORDER;

public:
    IDSDDetailedOcclusionPeeling(InpaintingDataSource * const decoratedIDS, int const patchX, int const patchY, int const patchZ);

    bool Peel();
};


#endif //INPAINTINGALGORITHMRUN_IDSDDETAILEDOCCLUSIONPEELING_H
