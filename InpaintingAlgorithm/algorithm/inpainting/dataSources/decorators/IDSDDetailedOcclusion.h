//
// Created by Jonathan on 5/18/2016.
//

#ifndef INPAINTINGALGORITHMRUN_IDSDDETAILEDOCCLUSION_H
#define INPAINTINGALGORITHMRUN_IDSDDETAILEDOCCLUSION_H

#include "../IDSDecorator.h"

class IDSDDetailedOcclusion : public IDSDecorator {
protected:
    int const patchX, patchY, patchZ; // These are used to construct the detailed occlusion. (Must be initialised before the latter)
    PaddedMatrix<float> * const detailedOcc;

public:
    IDSDDetailedOcclusion(InpaintingDataSource * const decoratedIDS, int const patchX, int const patchY, int const patchZ);
    ~IDSDDetailedOcclusion();

    PaddedMatrix<float> * const GetOcc() override;

protected:
    IDSDDetailedOcclusion(InpaintingDataSource * const decoratedIDS, int const patchX, int const patchY, int const patchZ,
                          bool const hasLayers);

    PaddedMatrix<float> * const ConstructDetailedOcclusion(bool hasLayers);
};


#endif //INPAINTINGALGORITHMRUN_IDSDDETAILEDOCCLUSION_H
