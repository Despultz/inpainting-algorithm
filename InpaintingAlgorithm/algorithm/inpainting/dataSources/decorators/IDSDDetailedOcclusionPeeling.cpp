//
// Created by Jonathan on 5/18/2016.
//

#include <PaddedMatrixKernelIt.h>
#include "IDSDDetailedOcclusionPeeling.h"

IDSDDetailedOcclusionPeeling::IDSDDetailedOcclusionPeeling(InpaintingDataSource * const decoratedIDS, int const patchX, int const patchY, int const patchZ)
        : IDSDDetailedOcclusion(decoratedIDS, patchX, patchY, patchZ, true)
{}

bool IDSDDetailedOcclusionPeeling::Peel(){
    PaddedMatrixIt matIt = PaddedMatrixIt(this->GetDim());
    PaddedMatrixDim borderKernelDim = PaddedMatrixDim(3, 3, 3);
    PaddedMatrixKernelIt borderKernelIt = PaddedMatrixKernelIt(this->GetDim(), &borderKernelDim);

    float * detailedOcc = this->GetOcc()->GetData();

    bool occlusionExists = false;
    do{
        if(detailedOcc[matIt.GetPosition()] == previousBorder)
            detailedOcc[matIt.GetPosition()] = DOCC_NEAROCC;
        else if(detailedOcc[matIt.GetPosition()] == DOCC_CORE){
            occlusionExists = true; // If we set a value to 2 or 3, which are the only outcomes from here, there's still layers to peel.

            bool inRange = false;
            borderKernelIt.Start(matIt.GetPosition());
            do{
                if(detailedOcc[borderKernelIt.GetPosition()] == previousBorder || detailedOcc[borderKernelIt.GetPosition()] == DOCC_NEAROCC)
                    inRange = true;
            }while(!inRange && borderKernelIt.Next());

            if(inRange) detailedOcc[matIt.GetPosition()] = currentBorder;
        }
    }while(matIt.Next());

    // Permute border values
    float oldValue = currentBorder;
    currentBorder = previousBorder;
    previousBorder = oldValue;

    return occlusionExists;
}