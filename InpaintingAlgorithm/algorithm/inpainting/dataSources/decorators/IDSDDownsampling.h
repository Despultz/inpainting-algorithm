//
// Created by Jonathan on 5/17/2016.
//

#ifndef INPAINTINGALGORITHMRUN_IDSDDOWNSAMLING_H
#define INPAINTINGALGORITHMRUN_IDSDDOWNSAMLING_H

#include "../../../StandardTools/ImageManipulator.h"
#include "../IDSDecorator.h"
#include "../IDSConcrete.h"

class IDSDDownsampling : public IDSDecorator {
private:
    InpaintingDataSource ** const idsLevels;

    int const downsamplingLevels;
    int const downsamplingRate;

    int curLevel;

public:
    IDSDDownsampling(InpaintingDataSource * const source, int const downsamplingLevels, int const downsamplingRate);
    ~IDSDDownsampling() override;

    void SetCurLevel(int const curLevel);
    void UpsampleShiftMatrices();

    PaddedMatrixDim * const GetDim() override;
    PaddedMatrix<float> * const GetRed() override;
    PaddedMatrix<float> * const GetBlue() override;
    PaddedMatrix<float> * const GetGreen() override;
    PaddedMatrix<float> * const GetOcc() override;
    PaddedMatrix<float> * const GetGradX() override;
    PaddedMatrix<float> * const GetGradY() override;
    PaddedMatrix<int> * const GetDispX() override;
    PaddedMatrix<int> * const GetDispY() override;
    PaddedMatrix<int> * const GetDispZ() override;
    PaddedMatrix<float> * const GetDispA() override;

private:
    void DownsampleColorsAndOcclusion(int const targetLevel);
    void DownsampleCasellesDescriptors(int const targetLevel);
};


#endif //INPAINTINGALGORITHMRUN_IDSDDOWNSAMLING_H
