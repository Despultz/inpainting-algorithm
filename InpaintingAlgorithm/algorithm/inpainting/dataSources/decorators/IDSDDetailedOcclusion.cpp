//
// Created by Jonathan on 5/18/2016.
//

#include <PaddedMatrixKernelIt.h>
#include "IDSDDetailedOcclusion.h"
#include "../../CommonInpainting.h"

IDSDDetailedOcclusion::IDSDDetailedOcclusion(InpaintingDataSource * const decoratedIDS,
                                             int const patchX, int const patchY, int const patchZ)
        : IDSDecorator(decoratedIDS), patchX(patchX), patchY(patchY), patchZ(patchZ), detailedOcc(ConstructDetailedOcclusion(false))
{}

IDSDDetailedOcclusion::IDSDDetailedOcclusion(InpaintingDataSource * const decoratedIDS,
                                             int const patchX, int const patchY, int const patchZ, bool hasLayers)
        : IDSDecorator(decoratedIDS), patchX(patchX), patchY(patchY), patchZ(patchZ), detailedOcc(ConstructDetailedOcclusion(hasLayers))
{}

IDSDDetailedOcclusion::~IDSDDetailedOcclusion(){
    delete detailedOcc;
}

PaddedMatrix<float> * const IDSDDetailedOcclusion::GetOcc(){
    return detailedOcc;
}

PaddedMatrix<float> * const IDSDDetailedOcclusion::ConstructDetailedOcclusion(bool hasLayers){
    PaddedMatrixDim * const levelDim = decoratedIDS->GetDim();

    PaddedMatrix<float> *detailOcc = new PaddedMatrix<float>(levelDim);

    // Out of bounds shouldn't be 1.0 or 0.0 to not corrupt the detailed occlusion.
    decoratedIDS->GetOcc()->FillBufferWith(-1.0f);

    float* occDetailData = detailOcc->GetData();
    float* occData = decoratedIDS->GetOcc()->GetData();

    PaddedMatrixDim patchDim = PaddedMatrixDim(patchX, patchY, patchZ);
    PaddedMatrixKernelIt patchKernelIt = PaddedMatrixKernelIt(levelDim, &patchDim);

    PaddedMatrixDim borderKernelDim = PaddedMatrixDim(3, 3, 3);
    PaddedMatrixKernelIt borderKernelIt = PaddedMatrixKernelIt(levelDim, &borderKernelDim);

    PaddedMatrixIt matIt = PaddedMatrixIt(levelDim);
    do{
        if(occData[matIt.GetPosition()] == 0.0f){
            bool inRange = false;
            patchKernelIt.Start(matIt.GetPosition());
            do{
                if(occData[patchKernelIt.GetPosition()] == 1.0f) inRange = true;
            }while(!inRange && patchKernelIt.Next());

            if(inRange) occDetailData[matIt.GetPosition()] = DOCC_NEAROCC;
            else // If we are too near the border, the pixel is invalid ; displacements can't point to it.
            if(matIt.GetCurX() - patchDim.GetHalfSizeXBefore() < 0 || matIt.GetCurX() + patchDim.GetHalfSizeXAfter() >= levelDim->GetSizeX() ||
               matIt.GetCurY() - patchDim.GetHalfSizeYBefore() < 0 || matIt.GetCurY() + patchDim.GetHalfSizeYAfter() >= levelDim->GetSizeY() ||
               matIt.GetCurZ() - patchDim.GetHalfSizeZBefore() < 0 || matIt.GetCurZ() + patchDim.GetHalfSizeZAfter() >= levelDim->GetSizeZ())
                occDetailData[matIt.GetPosition()] = DOCC_NEARBORDER;
            else
                occDetailData[matIt.GetPosition()] = DOCC_NORMAL;
        }
        else if(occData[matIt.GetPosition()] == 1.0f){
            if(!hasLayers)
                occDetailData[matIt.GetPosition()] = DOCC_BORDER;
            else{
                bool inRange = false;
                borderKernelIt.Start(matIt.GetPosition());
                do{
                    if(occData[borderKernelIt.GetPosition()] == 0.0f) inRange = true;
                }while(!inRange && borderKernelIt.Next());

                if(inRange) occDetailData[matIt.GetPosition()] = DOCC_BORDER;
                else occDetailData[matIt.GetPosition()] = DOCC_CORE;
            }
        }
    }while(matIt.Next());
    detailOcc->FillBufferWith(DOCC_BUFFER);

    return detailOcc;
}