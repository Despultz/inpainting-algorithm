//
// Created by Jonathan on 5/17/2016.
//

#include "IDSDDownsampling.h"

IDSDDownsampling::IDSDDownsampling(InpaintingDataSource * const source, int const downsamplingLevels, int const downsamplingRate)
: IDSDecorator(source), idsLevels(new InpaintingDataSource*[downsamplingLevels]),
  downsamplingLevels(downsamplingLevels), downsamplingRate(downsamplingRate), curLevel(downsamplingLevels - 1)
{
    idsLevels[0] = decoratedIDS; // The base downsampling is the same as the source.

    PaddedMatrixDim * const oriDim = idsLevels[0]->GetDim();
    int x = oriDim->GetSizeX();
    int y = oriDim->GetSizeY();
    int z = oriDim->GetSizeZ(); // No Z downsampling. Could be added.
    for(int i = 1; i < downsamplingLevels; ++i){
        x /= downsamplingRate;
        y /= downsamplingRate;
        // z /= downsamplingRate;

        PaddedMatrixDim * const nextDim = new PaddedMatrixDim(x, y, z, oriDim->GetBuffX(), oriDim->GetBuffY(), oriDim->GetBuffZ());
        idsLevels[i] = new IDSConcrete(nextDim); // All the matrices are empty.

        DownsampleColorsAndOcclusion(i); // Fill the RGB and OCC channels of the "i" IDS.
        DownsampleCasellesDescriptors(i);
    }
}

IDSDDownsampling::~IDSDDownsampling(){
    for(int i = 1; i < downsamplingLevels; ++i)
        delete idsLevels[i]; // Index 0 deletes the ids contained in the base class. (Which is bad, since it doesn't own it.)

    delete[] idsLevels;
}

void IDSDDownsampling::SetCurLevel(int const curLevel){
    this->curLevel = curLevel;
}

void IDSDDownsampling::UpsampleShiftMatrices(){
    PaddedMatrixDim * fromDim = idsLevels[curLevel]->GetDim();
    int* fromXDispData = idsLevels[curLevel]->GetDispX()->GetData();
    int* fromYDispData = idsLevels[curLevel]->GetDispY()->GetData();
    int* fromZDispData = idsLevels[curLevel]->GetDispZ()->GetData();
    float* fromADispData = idsLevels[curLevel]->GetDispA()->GetData();

    PaddedMatrixDim * toDim = idsLevels[curLevel - 1]->GetDim();
    int* toXDispData = idsLevels[curLevel - 1]->GetDispX()->GetData();
    int* toYDispData = idsLevels[curLevel - 1]->GetDispY()->GetData();
    int* toZDispData = idsLevels[curLevel - 1]->GetDispZ()->GetData();
    float* toADispData = idsLevels[curLevel - 1]->GetDispA()->GetData();

    PaddedMatrixIt nextVolIt = PaddedMatrixIt(toDim);
    PaddedMatrixIt sourceVolIt = PaddedMatrixIt(fromDim);
    do{
        int curPos = nextVolIt.GetPosition(); // The same position
        int nexPos = nextVolIt.GetPosition() + 1; // The pixel to the right of the current.
        int lowPos = nextVolIt.GetPosition() + toDim->GetBuffSizeX(); // The pixel below the current.
        int diaPos = nextVolIt.GetPosition() + toDim->GetBuffSizeX() + 1; // The pixel below and to to right - in diagonal.

        int nextXDisp = downsamplingRate * fromXDispData[sourceVolIt.GetPosition()]; // There are more X pixels in the next level, so we multiply the current value.
        toXDispData[curPos] = nextXDisp;
        toXDispData[nexPos] = nextXDisp;
        toXDispData[lowPos] = nextXDisp;
        toXDispData[diaPos] = nextXDisp;

        int nextYDisp = downsamplingRate * fromYDispData[sourceVolIt.GetPosition()]; // There are more Y pixels in the next level, so we multiply the current value.
        toYDispData[curPos] = nextYDisp;
        toYDispData[nexPos] = nextYDisp;
        toYDispData[lowPos] = nextYDisp;
        toYDispData[diaPos] = nextYDisp;

        int nextZDisp = fromZDispData[sourceVolIt.GetPosition()]; // There is no Z downsampling... So we keep the same value.
        toZDispData[curPos] = nextZDisp;
        toZDispData[nexPos] = nextZDisp;
        toZDispData[lowPos] = nextZDisp;
        toZDispData[diaPos] = nextZDisp;

        float nextADisp = fromADispData[sourceVolIt.GetPosition()]; // We can't evaluate the true value of the displacement until it gets reconstructed. (0 means uninitialized!)
        toADispData[curPos] = nextADisp;
        toADispData[nexPos] = nextADisp;
        toADispData[lowPos] = nextADisp;
        toADispData[diaPos] = nextADisp;
    }while(nextVolIt.Next(downsamplingRate, downsamplingRate) && sourceVolIt.Next());
}

PaddedMatrixDim * const IDSDDownsampling::GetDim(){
    return idsLevels[curLevel]->GetDim();
}

PaddedMatrix<float> * const IDSDDownsampling::GetRed(){
    return idsLevels[curLevel]->GetRed();
}

PaddedMatrix<float> * const IDSDDownsampling::GetBlue(){
    return idsLevels[curLevel]->GetBlue();
}

PaddedMatrix<float> * const IDSDDownsampling::GetGreen(){
    return idsLevels[curLevel]->GetGreen();
}

PaddedMatrix<float> * const IDSDDownsampling::GetOcc(){
    return idsLevels[curLevel]->GetOcc();
}

PaddedMatrix<float> * const IDSDDownsampling::GetGradX(){
    return idsLevels[curLevel]->GetGradX();
}

PaddedMatrix<float> * const IDSDDownsampling::GetGradY(){
    return idsLevels[curLevel]->GetGradY();
}

PaddedMatrix<int> * const IDSDDownsampling::GetDispX(){
    return idsLevels[curLevel]->GetDispX();
}

PaddedMatrix<int> * const IDSDDownsampling::GetDispY(){
    return idsLevels[curLevel]->GetDispY();
}

PaddedMatrix<int> * const IDSDDownsampling::GetDispZ(){
    return idsLevels[curLevel]->GetDispZ();
}

PaddedMatrix<float> * const IDSDDownsampling::GetDispA(){
    return idsLevels[curLevel]->GetDispA();
}

// TODO : This function assumes a minimum of "downsamplingRate" padding on the X and Y axis.
void IDSDDownsampling::DownsampleColorsAndOcclusion(int const targetLevel){
    float gaussianFilter[] = {0.307801329123470f, 0.384397341753060f, 0.307801329123470f}; // ImageManipulator::GaussianFilter(0.5, downsamplingRate + 1, 1); // TODO : Soft code the 0.5

    PaddedMatrixDim xFilterDim = PaddedMatrixDim(3,1,1);
    PaddedMatrix<float> gaussFilterX = PaddedMatrix<float>(&xFilterDim);

    PaddedMatrixDim yFilterDim = PaddedMatrixDim(1,3,1);
    PaddedMatrix<float> gaussFilterY = PaddedMatrix<float>(&yFilterDim);

    for(int i = 0; i < 3; i++){
        gaussFilterX.GetData()[i] = gaussianFilter[i];
        gaussFilterY.GetData()[i] = gaussianFilter[i];
    }

    InpaintingDataSource * const sourceIds = idsLevels[targetLevel - 1];
    InpaintingDataSource * const targetIds = idsLevels[targetLevel];

    PaddedMatrix<float> xFilteredRed = ImageManipulator::ImageFilter(sourceIds->GetRed(), &gaussFilterX);
    PaddedMatrix<float> xFilteredGreen = ImageManipulator::ImageFilter(sourceIds->GetGreen(), &gaussFilterX);
    PaddedMatrix<float> xFilteredBlue = ImageManipulator::ImageFilter(sourceIds->GetBlue(), &gaussFilterX);
    PaddedMatrix<float> xFilteredOcc = ImageManipulator::ImageFilter(sourceIds->GetOcc(), &gaussFilterX);

    PaddedMatrix<float> fullFilteredRed = ImageManipulator::ImageFilter(&xFilteredRed, &gaussFilterY);
    PaddedMatrix<float> fullFilteredGreen = ImageManipulator::ImageFilter(&xFilteredGreen, &gaussFilterY);
    PaddedMatrix<float> fullFilteredBlue = ImageManipulator::ImageFilter(&xFilteredBlue, &gaussFilterY);
    PaddedMatrix<float> fullFilteredOcc = ImageManipulator::ImageFilter(&xFilteredOcc, &gaussFilterY);

    PaddedMatrixIt sourceLevelIt = PaddedMatrixIt(sourceIds->GetDim());
    PaddedMatrixIt targetLevelIt = PaddedMatrixIt(targetIds->GetDim());
    do{
        targetIds->GetRed()->GetData()[targetLevelIt.GetPosition()] = fullFilteredRed.GetData()[sourceLevelIt.GetPosition()];
        targetIds->GetGreen()->GetData()[targetLevelIt.GetPosition()] = fullFilteredGreen.GetData()[sourceLevelIt.GetPosition()];
        targetIds->GetBlue()->GetData()[targetLevelIt.GetPosition()] = fullFilteredBlue.GetData()[sourceLevelIt.GetPosition()];
        targetIds->GetOcc()->GetData()[targetLevelIt.GetPosition()] = ceilf(fullFilteredOcc.GetData()[sourceLevelIt.GetPosition()]);
    }while(targetLevelIt.Next() && sourceLevelIt.Next(downsamplingRate, downsamplingRate));
}

void IDSDDownsampling::DownsampleCasellesDescriptors(int const targetLevel){
    int leap = (int)powf(downsamplingRate, targetLevel); // Could overflow easy.

    InpaintingDataSource * const sourceIds = idsLevels[0];
    InpaintingDataSource * const targetIds = idsLevels[targetLevel];

    PaddedMatrixIt sourceIt = PaddedMatrixIt(sourceIds->GetDim());
    PaddedMatrixIt targetIt = PaddedMatrixIt(targetIds->GetDim());
    do{
        targetIds->GetGradX()->GetData()[targetIt.GetPosition()] = sourceIds->GetGradX()->GetData()[sourceIt.GetPosition()];
        targetIds->GetGradY()->GetData()[targetIt.GetPosition()] = sourceIds->GetGradY()->GetData()[sourceIt.GetPosition()];
    }while(sourceIt.Next(leap, leap) && targetIt.Next());
}

/*
    PaddedMatrixDim gaussianKernelDim = PaddedMatrixDim(downsamplingRate + 1, downsamplingRate + 1, 1);

    InpaintingDataSource * const sourceIds = idsLevels[targetLevel - 1];
    InpaintingDataSource * const targetIds = idsLevels[targetLevel];

    // Need approximation for out-of-bound dataSources, so we mirror the edge pixels in the padding.
    sourceIds->GetRed()->MirrorIntoBuffer();
    sourceIds->GetGreen()->MirrorIntoBuffer();
    sourceIds->GetBlue()->MirrorIntoBuffer();
    sourceIds->GetOcc()->MirrorIntoBuffer();

    float* sourceRed = sourceIds->GetRed()->GetData();
    float* sourceBlu = sourceIds->GetBlue()->GetData();
    float* sourceGrn = sourceIds->GetGreen()->GetData();
    float* sourceOcc = sourceIds->GetOcc()->GetData();

    float* dwnSmpRed = targetIds->GetRed()->GetData();
    float* dwnSmpBlu = targetIds->GetBlue()->GetData();
    float* dwnSmpGrn = targetIds->GetGreen()->GetData();
    float* dwnSmpOcc = targetIds->GetOcc()->GetData();

    PaddedMatrixIt sourceLevelIt = PaddedMatrixIt(sourceIds->GetDim());
    PaddedMatrixIt targetLevelIt = PaddedMatrixIt(targetIds->GetDim());
    PaddedMatrixKernelIt gaussianKernelIt = PaddedMatrixKernelIt(sourceIds->GetDim(), &gaussianKernelDim);
    do{
        float redVal = 0.0f, bluVal = 0.0f, grnVal = 0.0f, occVal = 0.0f;
        gaussianKernelIt.Start(sourceLevelIt.GetPosition());
        do{
            redVal += sourceRed[gaussianKernelIt.GetPosition()] * gaussianFilter[gaussianKernelIt.GetIndex()];
            bluVal += sourceBlu[gaussianKernelIt.GetPosition()] * gaussianFilter[gaussianKernelIt.GetIndex()];
            grnVal += sourceGrn[gaussianKernelIt.GetPosition()] * gaussianFilter[gaussianKernelIt.GetIndex()];
            occVal += sourceOcc[gaussianKernelIt.GetPosition()] * gaussianFilter[gaussianKernelIt.GetIndex()];
        }while(gaussianKernelIt.Next());

        // If always occlude more.
        occVal = ceilf(occVal);

        dwnSmpRed[targetLevelIt.GetPosition()] = redVal;
        dwnSmpBlu[targetLevelIt.GetPosition()] = bluVal;
        dwnSmpGrn[targetLevelIt.GetPosition()] = grnVal;
        dwnSmpOcc[targetLevelIt.GetPosition()] = occVal;
    }while(targetLevelIt.Next() && sourceLevelIt.Next(downsamplingRate, downsamplingRate));
    */