//
// Created by Jonathan on 13/04/2016.
//

#ifndef INPAINTINGALGORITHMRUN_INPAINTINGRECONSTRUCTOR_H
#define INPAINTINGALGORITHMRUN_INPAINTINGRECONSTRUCTOR_H

#include "CommonInpainting.h"
#include "dataSources/InpaintingDataSource.h"

class InpaintingReconstructor {
public:
    InpaintingReconstructor(){}
    virtual ~InpaintingReconstructor(){}

    virtual float Reconstruct(InpaintingDataSource * const iDataProvider, bool hasInvalidPixels) = 0;
};

#endif //INPAINTINGALGORITHMRUN_INPAINTINGRECONSTRUCTOR_H
