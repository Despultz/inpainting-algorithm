//
// Created by Jonathan on 16/04/2016.
//

#include "InpaintingEvaluator.h"

InpaintingEvaluator::InpaintingEvaluator(int const beta, int const patchSizeX, int const patchSizeY, int const patchSizeZ)
        : beta(beta), patchDim(PaddedMatrixDim(patchSizeX, patchSizeY, patchSizeZ))
{}

InpaintingEvaluator::~InpaintingEvaluator(){}

// TODO Calling this function for every pixel is very costly, revise efficiency of it.
float InpaintingEvaluator::EvaluateDisplacement(InpaintingDataSource * const iDataProvider,
                                                float const minVal, bool const hasInvalidPixels,
                                                int oriX, int oriY, int oriZ,
                                                int dispX, int dispY, int dispZ)
{
    // Calculate arrival position of displacement
    int dispPosX = oriX + dispX;
    int dispPosY = oriY + dispY;
    int dispPosZ = oriZ + dispZ;

    // Get the level's dimension
    PaddedMatrixDim dim = *iDataProvider->GetDim();

    // All the data this algorithm needs.
    float* detailOccData = iDataProvider->GetOcc()->GetData();
    float* redData = iDataProvider->GetRed()->GetData();
    float* bluData = iDataProvider->GetBlue()->GetData();
    float* grnData = iDataProvider->GetGreen()->GetData();
    float* gradXData = iDataProvider->GetGradX()->GetData();
    float* gradYData = iDataProvider->GetGradY()->GetData();

    int oriPos = dim.DeterminePosition(oriX, oriY, oriZ);
    int dispPos = dim.DeterminePosition(dispPosX, dispPosY, dispPosZ);

    auto patchOriKernelIt = PaddedMatrixKernelIt(&dim, &patchDim);
    auto patchDispKernelIt = PaddedMatrixKernelIt(&dim, &patchDim);

    int sumOcc = 0;
    if(hasInvalidPixels){
        patchOriKernelIt.Start(oriPos);
        do{
            if(detailOccData[patchOriKernelIt.GetPosition()] != DOCC_BUFFER &&
               detailOccData[patchOriKernelIt.GetPosition()] < DOCC_NEAROCC)
                sumOcc++; // If the pixel is from the original image, it counts.
        }while(patchOriKernelIt.Next());
    }
    else{
        // Faster than checking the kernel for != DOCC_BUFFER ... I think.
        int patchSizeX = __min(oriX + patchDim.GetHalfSizeXAfter(), dim.GetSizeX() - 1) - __max(oriX - patchDim.GetHalfSizeXBefore(), 0) + 1;
        int patchSizeY = __min(oriY + patchDim.GetHalfSizeYAfter(), dim.GetSizeY() - 1) - __max(oriY - patchDim.GetHalfSizeXBefore(), 0) + 1;
        int patchSizeZ = __min(oriZ + patchDim.GetHalfSizeZAfter(), dim.GetSizeZ() - 1) - __max(oriZ - patchDim.GetHalfSizeXBefore(), 0) + 1;

        sumOcc = patchSizeX * patchSizeY * patchSizeZ;
    }
    sumOcc = __max(sumOcc, 1); // We can't evaluate from this origin yet.

    float ssd = 0;
    patchOriKernelIt.Start(oriPos);
    patchDispKernelIt.Start(dispPos);
    do{
        // Check if the origin pixel is within boundaries
        if(detailOccData[patchOriKernelIt.GetPosition()] == DOCC_BUFFER)
            continue;

        // If the origin pixel is in the occlusion mask, it doesn't count.
        if(hasInvalidPixels && detailOccData[patchOriKernelIt.GetPosition()] > DOCC_NEAROCC)
            continue;

        // Calculate the difference between the color values.
        float tempSum = 0;
        float tempFloat = 0;

        tempFloat = redData[patchOriKernelIt.GetPosition()] - redData[patchDispKernelIt.GetPosition()];
        tempSum += tempFloat * tempFloat;

        tempFloat = bluData[patchOriKernelIt.GetPosition()] - bluData[patchDispKernelIt.GetPosition()];
        tempSum += tempFloat * tempFloat;

        tempFloat = grnData[patchOriKernelIt.GetPosition()] - grnData[patchDispKernelIt.GetPosition()];
        tempSum += tempFloat * tempFloat;

        tempFloat = gradXData[patchOriKernelIt.GetPosition()] - gradXData[patchDispKernelIt.GetPosition()];
        tempSum += beta * tempFloat * tempFloat;

        tempFloat = gradYData[patchOriKernelIt.GetPosition()] - gradYData[patchDispKernelIt.GetPosition()];
        tempSum += beta * tempFloat * tempFloat;

        ssd += tempSum/sumOcc;

        // If we are already less good than the previous value, we stop comparing.
        if(minVal != -1.0f && ssd > minVal)
            return -1.0f;

    }while(patchOriKernelIt.Next() && patchDispKernelIt.Next());

    return ssd;
}