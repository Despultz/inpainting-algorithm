//
// Created by Jonathan on 6/1/2016.
//

#include <PaddedMatrixKernelIt.h>
#include <cfloat>
#include "IRBestPatch.h"

IRBestPatch::IRBestPatch(int const patchSizeX, int const patchSizeY, int const patchSizeZ)
: patchDim(PaddedMatrixDim(patchSizeX, patchSizeY, patchSizeZ))
{}

IRBestPatch::~IRBestPatch()
{}

float IRBestPatch::Reconstruct(InpaintingDataSource * const iDataProvider, bool hasInvalidPixels)
{
    // The big data
    float * redMatData = iDataProvider->GetRed()->GetData();
    float * greenMatData = iDataProvider->GetGreen()->GetData();
    float * blueMatData = iDataProvider->GetBlue()->GetData();
    float * occMatData = iDataProvider->GetOcc()->GetData();
    float * dispAData = iDataProvider->GetDispA()->GetData();
    int * dispXData = iDataProvider->GetDispX()->GetData();
    int * dispYData = iDataProvider->GetDispY()->GetData();
    int * dispZData = iDataProvider->GetDispZ()->GetData();

    // The loop (Find the best patch amongst the neighbors)
    PaddedMatrixIt matIt = PaddedMatrixIt(iDataProvider->GetDim());
    PaddedMatrixKernelIt kernelIt = PaddedMatrixKernelIt(iDataProvider->GetDim(), &patchDim);
    do{
        if(occMatData[matIt.GetPosition()] < DOCC_BORDER || occMatData[matIt.GetPosition()] > DOCC_BORDER_ALT)
            continue;

        bool correctInfo = false;
        float minWeight = FLT_MAX;
        float colorRed = 0.0f;
        float colorGreen = 0.0f;
        float colorBlue = 0.0f;

        kernelIt.Start(matIt.GetPosition());
        do{
            if(occMatData[kernelIt.GetPosition()] != DOCC_BUFFER &&
               (!hasInvalidPixels || occMatData[kernelIt.GetPosition()] < DOCC_BORDER))
            {
                if(dispAData[kernelIt.GetPosition()] < minWeight){
                    minWeight = dispAData[kernelIt.GetPosition()];

                    // Get the colors this neighbor points to.
                    int dispX = matIt.GetCurX() + dispXData[kernelIt.GetPosition()];
                    int dispY = matIt.GetCurY() + dispYData[kernelIt.GetPosition()];
                    int dispZ = matIt.GetCurZ() + dispZData[kernelIt.GetPosition()];
                    int dispPos = iDataProvider->GetDim()->DeterminePosition(dispX, dispY, dispZ);

                    colorRed = redMatData[dispPos];
                    colorGreen = greenMatData[dispPos];
                    colorBlue = blueMatData[dispPos];
                }

                correctInfo = true;
            }
        }while(kernelIt.Next());

        if(!correctInfo) continue;

        redMatData[matIt.GetPosition()] = colorRed;
        greenMatData[matIt.GetPosition()] = colorGreen;
        blueMatData[matIt.GetPosition()] = colorBlue;

    }while(matIt.Next());
}