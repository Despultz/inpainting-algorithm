//
// Created by Jonathan on 4/25/2016.
//

#ifndef INPAINTINGALGORITHMRUN_IRPROFILER_H
#define INPAINTINGALGORITHMRUN_IRPROFILER_H

#include "../../InpaintingTools/InpaintingProfiler.h"
#include "../InpaintingReconstructor.h"
#include "../dataSources/InpaintingDataSource.h"

class IRProfiler : public InpaintingReconstructor{
private:
    InpaintingReconstructor * const iReconstructor;
    InpaintingProfiler * const iProfiler;

public:
    IRProfiler(InpaintingProfiler * const iProfiler, InpaintingReconstructor * const iReconstructor);
    ~IRProfiler();

    float Reconstruct(InpaintingDataSource *iDataProvider, bool hasInvalidPixels);
};


#endif //INPAINTINGALGORITHMRUN_IRPROFILER_H
