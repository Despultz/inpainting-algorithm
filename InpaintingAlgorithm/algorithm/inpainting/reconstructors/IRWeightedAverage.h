//
// Created by Jonathan on 13/04/2016.
//

#ifndef INPAINTINGALGORITHMRUN_IRWEIGHTEDAVERAGE_H
#define INPAINTINGALGORITHMRUN_IRWEIGHTEDAVERAGE_H

#include <math.h>
#include <algorithm>
#include <iostream>

#include "../../standardTools/PaddedMatrixKernelIt.h"
#include "../InpaintingReconstructor.h"
#include "../dataSources/InpaintingDataSource.h"

class IRWeightedAverage : public InpaintingReconstructor{
private:
    float const sigmaColor = 0.75;
    PaddedMatrixDim const patchDim;

public:
    IRWeightedAverage(float const sigmaColor, int const patchSizeX, int const patchSizeY, int const patchSizeZ);
    ~IRWeightedAverage();

    virtual float Reconstruct(InpaintingDataSource *iDataProvider, bool hasInvalidPixels);

private:
    float GetAdaptativeSigma(PaddedMatrix<float> *weightMatrix);
};


#endif //INPAINTINGALGORITHMRUN_IRWEIGHTEDAVERAGE_H
