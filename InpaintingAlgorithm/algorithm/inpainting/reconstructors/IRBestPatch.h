//
// Created by Jonathan on 6/1/2016.
//

#ifndef INPAINTINGALGORITHMRUN_IRBESTPATCH_H
#define INPAINTINGALGORITHMRUN_IRBESTPATCH_H


#include "../InpaintingReconstructor.h"

class IRBestPatch : public InpaintingReconstructor{
private:
    PaddedMatrixDim const patchDim;

public:
    IRBestPatch(int const patchSizeX, int const patchSizeY, int const patchSizeZ);
    ~IRBestPatch();

    float Reconstruct(InpaintingDataSource * const iDataProvider, bool hasInvalidPixels) override;

};


#endif //INPAINTINGALGORITHMRUN_IRBESTPATCH_H
