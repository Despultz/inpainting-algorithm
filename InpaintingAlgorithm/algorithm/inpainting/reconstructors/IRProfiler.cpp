//
// Created by Jonathan on 4/25/2016.
//

#include "IRProfiler.h"

IRProfiler::IRProfiler(InpaintingProfiler * const iProfiler, InpaintingReconstructor * const iReconstructor)
        : InpaintingReconstructor(), iProfiler(iProfiler), iReconstructor(iReconstructor)
{}

IRProfiler::~IRProfiler(){}

float IRProfiler::Reconstruct(InpaintingDataSource *iDataProvider, bool hasInvalidPixels){
    iProfiler->BeforeReconstruct(0, 0);
    float result = iReconstructor->Reconstruct(iDataProvider, hasInvalidPixels);
    iProfiler->AfterReconstruct(0, 0);

    return result;
}