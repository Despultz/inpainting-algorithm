//
// Created by Jonathan on 13/04/2016.
//

#include "IRWeightedAverage.h"

IRWeightedAverage::IRWeightedAverage(float const sigmaColor, int const patchSizeX, int const patchSizeY, int const patchSizeZ)
        : InpaintingReconstructor(), sigmaColor(sigmaColor), patchDim(PaddedMatrixDim(patchSizeX, patchSizeY, patchSizeZ))
{}

IRWeightedAverage::~IRWeightedAverage() {}

float IRWeightedAverage::Reconstruct(InpaintingDataSource *iDataProvider, bool hasInvalidPixels){
    // Value the amount of change happening during the reconstruction
    int pixelsChanged = 0;
    float perPixelChangeSum = 0.0f;

    // Big volume info
    PaddedMatrixDim * levelDim = iDataProvider->GetDim();

    // The data we'll work with
    float* detailedOccData = iDataProvider->GetOcc()->GetData();
    int* dispXData = iDataProvider->GetDispX()->GetData();
    int* dispYData = iDataProvider->GetDispY()->GetData();
    int* dispZData = iDataProvider->GetDispZ()->GetData();
    float* dispAData = iDataProvider->GetDispA()->GetData();
    float* redData = iDataProvider->GetRed()->GetData();
    float* bluData = iDataProvider->GetBlue()->GetData();
    float* grnData = iDataProvider->GetGreen()->GetData();
    float* gradXData = iDataProvider->GetGradX()->GetData();
    float* gradYData = iDataProvider->GetGradY()->GetData();

    // Kernel
    PaddedMatrix<float> weightMatrix = PaddedMatrix<float>(&patchDim);
    float* weightData = weightMatrix.GetData();
    PaddedMatrixKernelIt kernelIt = PaddedMatrixKernelIt(levelDim, &patchDim);

    // The real loop.
    PaddedMatrixIt matIt = PaddedMatrixIt(levelDim);
    do{
        if(detailedOccData[matIt.GetPosition()] <= DOCC_NEAROCC || detailedOccData[matIt.GetPosition()] >= DOCC_CORE)
            continue; // Only the border pixels get through and get reconstructed.

        bool correctInfo = false;

        float sumWeights = 0.0f;
        float avgColourR = 0.0f;
        float avgColourG = 0.0f;
        float avgColourB = 0.0f;
        float avgGradX = 0.0f;
        float avgGradY = 0.0f;
        float adaptiveSigma;

        // Calculate the weights
        kernelIt.Start(matIt.GetPosition());
        do{
            if(detailedOccData[kernelIt.GetPosition()] != DOCC_BUFFER &&
               (!hasInvalidPixels || detailedOccData[kernelIt.GetPosition()] <= DOCC_NEAROCC))
            {
                weightData[kernelIt.GetIndex()] = dispAData[kernelIt.GetPosition()];
                correctInfo = true;
            }
            else
                weightData[kernelIt.GetIndex()] = -1.0f;
        }while(kernelIt.Next());

        if(!correctInfo) continue;

        adaptiveSigma = __max(GetAdaptativeSigma(&weightMatrix), 0.1f); // Since our values and scores and 255 times smaller?

        // Calculate the pixel value
        kernelIt.Start(matIt.GetPosition());
        do{
            if(detailedOccData[kernelIt.GetPosition()] != DOCC_BUFFER &&
               (!hasInvalidPixels || detailedOccData[kernelIt.GetPosition()] <= DOCC_NEAROCC))
            {
                float weight = expf(-weightData[kernelIt.GetIndex()]/(2*adaptiveSigma*adaptiveSigma));
                sumWeights += weight;

                int dispShiftX = matIt.GetCurX() + dispXData[kernelIt.GetPosition()];
                int dispShiftY = matIt.GetCurY() + dispYData[kernelIt.GetPosition()];
                int dispShiftZ = matIt.GetCurZ() + dispZData[kernelIt.GetPosition()];
                int dispShiftPos = levelDim->DeterminePosition(dispShiftX, dispShiftY, dispShiftZ);

                avgColourR += weight * redData[dispShiftPos];
                avgColourG += weight * grnData[dispShiftPos];
                avgColourB += weight * bluData[dispShiftPos];
                avgGradX += weight * gradXData[dispShiftPos];
                avgGradY += weight * gradYData[dispShiftPos];
            }
        }while(kernelIt.Next());

        // Calculate the average difference between the two images
        float red = avgColourR / sumWeights;
        float green = avgColourG / sumWeights;
        float blue = avgColourB / sumWeights;

        perPixelChangeSum += fabsf(redData[matIt.GetPosition()] - red)
                          + fabsf(grnData[matIt.GetPosition()] - green)
                          + fabsf(bluData[matIt.GetPosition()] - blue);
        pixelsChanged++;

        // Assign the new values.
        redData[matIt.GetPosition()] = red;
        grnData[matIt.GetPosition()] = green;
        bluData[matIt.GetPosition()] = blue;
        gradXData[matIt.GetPosition()] = avgGradX / sumWeights;
        gradYData[matIt.GetPosition()] = avgGradY / sumWeights;

    }while(matIt.Next());

    return perPixelChangeSum / (3 * pixelsChanged); // Three for the three colors.
}

float IRWeightedAverage::GetAdaptativeSigma(PaddedMatrix<float> *weightMatrix) {
    float *weightsTemp = new float[weightMatrix->GetDimension()->GetVolume()]; // TODO Non-heap memory better
    int weightIndex = 0;

    PaddedMatrixIt vIt = PaddedMatrixIt(weightMatrix->GetDimension());
    do{
        if(weightMatrix->GetData()[vIt.GetPosition()] != -1)
            weightsTemp[weightIndex++] = weightMatrix->GetData()[vIt.GetPosition()];
    }while(vIt.Next());

    weightIndex--;
    std::sort(weightsTemp, weightsTemp + weightIndex);

    float adaptiveSigma = sqrtf(weightsTemp[(int)floorf(sigmaColor * weightIndex)]);
    delete[] weightsTemp;

    return adaptiveSigma;
}