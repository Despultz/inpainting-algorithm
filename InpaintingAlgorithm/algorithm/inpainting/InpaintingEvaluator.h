//
// Created by Jonathan on 16/04/2016.
//

#ifndef INPAINTINGALGORITHMRUN_INPAINTINGEVALUATOR_H
#define INPAINTINGALGORITHMRUN_INPAINTINGEVALUATOR_H

#include <stdlib.h>
#include "../standardTools/PaddedMatrixKernelIt.h"
#include "CommonInpainting.h"
#include "dataSources/InpaintingDataSource.h"

class InpaintingEvaluator {
private:
    int const beta = 50;
    PaddedMatrixDim const patchDim;

public:
    InpaintingEvaluator(int const beta, int const patchSizeX, int const patchSizeY, int const patchSizeZ);
    ~InpaintingEvaluator();

    float EvaluateDisplacement(InpaintingDataSource * const iDataProvider,
                               float const minVal, bool const hasInvalidPixels,
                               int oriX, int oriY, int oriZ,
                               int dispX, int dispY, int dispZ);

    const PaddedMatrixDim &GetPatchDim() const {
        return patchDim;
    }
};


#endif //INPAINTINGALGORITHMRUN_INPAINTINGEVALUATOR_H
