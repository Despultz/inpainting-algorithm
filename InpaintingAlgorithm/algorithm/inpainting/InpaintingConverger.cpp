//
// Created by Jonathan on 23/02/2016.
//

#include "InpaintingConverger.h"

InpaintingConverger::InpaintingConverger(int const downsamplingLevels, int const downsamplingRate,
                     int const nbIterations, float const residualThreshold, InpaintingSearcher * const iSearcher,
                     InpaintingReconstructor * const iReconstructor, InpaintingEvaluator * const iEvaluator)
        : downsamplingLevels(downsamplingLevels), downsamplingRate(downsamplingRate),
          nbIterations(nbIterations), residualThreshold(residualThreshold),
          iSearcher(iSearcher), iReconstructor(iReconstructor), iEvaluator(iEvaluator)
{}

InpaintingConverger::~InpaintingConverger() {}

void InpaintingConverger::Inpaint(InpaintingDataSource * const iDataSource, InpaintingProfiler * const iProfiler) {
    IDSDDownsampling downsampledDataSource = IDSDDownsampling(iDataSource, downsamplingLevels, downsamplingRate);

    for(int curLevel = downsamplingLevels - 1; curLevel >= 0; curLevel--){
        iProfiler->BeforeInpaintLevel(curLevel);

        downsampledDataSource.SetCurLevel(curLevel);
        IDSDDetailedOcclusion detailedOcclusionDataSource = IDSDDetailedOcclusion(&downsampledDataSource, iEvaluator->GetPatchDim().GetSizeX(), iEvaluator->GetPatchDim().GetSizeY(), iEvaluator->GetPatchDim().GetSizeZ());

        if(curLevel < downsamplingLevels - 1)
            iReconstructor->Reconstruct(&detailedOcclusionDataSource, false);

        float residual = FLT_MAX;
        iProfiler->BeforeInpaintingIterations(curLevel);
        for(int iteration = 0; iteration < nbIterations && residual > residualThreshold; iteration++){
            iProfiler->BeforeInpaintingIteration(curLevel, iteration);
            if(curLevel == downsamplingLevels-1 && iteration == 0)
                OnionPeelInitialisation(&downsampledDataSource, iProfiler);
            else{
                iSearcher->Search(&detailedOcclusionDataSource, false);
                residual = iReconstructor->Reconstruct(&detailedOcclusionDataSource, false);
            }
            iProfiler->AfterInpaintingIteration(curLevel, iteration);
        }

        if(curLevel != 0)
            downsampledDataSource.UpsampleShiftMatrices();

        iProfiler->AfterInpaintingIterations(curLevel);
        iProfiler->AfterInpaintLevel(curLevel);
    }
}

void InpaintingConverger::OnionPeelInitialisation(InpaintingDataSource * const iDataSource, InpaintingProfiler * const iProfiler) {
    iProfiler->BeforeInitialisation();

    IDSDDetailedOcclusionPeeling peelingDataSource = IDSDDetailedOcclusionPeeling(iDataSource, iEvaluator->GetPatchDim().GetSizeX(), iEvaluator->GetPatchDim().GetSizeY(), iEvaluator->GetPatchDim().GetSizeZ());

    // Slowly peel back the layers!
    int peelNo = 0;
    do{
        iProfiler->BeforeInitialisationPeel(peelNo);

        iSearcher->Search(&peelingDataSource, true);
        iReconstructor->Reconstruct(&peelingDataSource, true);

        iProfiler->AfterInitialisationPeel(peelNo++);
    }while(peelingDataSource.Peel());

    iProfiler->AfterInitialisation();
}