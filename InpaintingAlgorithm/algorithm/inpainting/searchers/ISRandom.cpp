//
// Created by Jonathan on 13/04/2016.
//

#include <iostream>
#include <chrono>
#include "ISRandom.h"

ISRandom::ISRandom(int const searchIterations, InpaintingEvaluator * const iEvaluator)
        : InpaintingSearcher(), iEvaluator(iEvaluator), searchIterations(searchIterations){}

ISRandom::~ISRandom(){}

void ISRandom::Search(InpaintingDataSource * const iDataProvider, bool const hasInvalidPixels)
{
    srand(0);
    std::chrono::steady_clock::time_point sectionClock;

    sectionClock = std::chrono::steady_clock::now();
    InitialiseDisplacementField(iDataProvider, hasInvalidPixels);
    std::cout << "       Initialisation time : " << ((std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now() - sectionClock).count()) * pow(10,-9)) << std::endl;

    double propTime = 0, randTime = 0;
    int propCount = 0, randCount = 0;
    for(int i = 0; i < searchIterations; ++i){
        bool goForward = (i & 1) != 0;

        sectionClock = std::chrono::steady_clock::now();
        propCount += NeighborSearch(iDataProvider, hasInvalidPixels, goForward);
        propTime += (std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now() - sectionClock).count()) * pow(10,-9);

        sectionClock = std::chrono::steady_clock::now();
        randCount += RandomSearch(iDataProvider, hasInvalidPixels);
        randTime += (std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now() - sectionClock).count()) * pow(10,-9);
    }

    std::cout << "       Propagation time : " << propTime << std::endl;
    std::cout << "       Propagation count : " << propCount << std::endl;
    std::cout << "       Rand search time : " << randTime << std::endl;
    std::cout << "       Rand search count : " << randCount << std::endl;
}

bool ISRandom::CheckForBetterDisplacement(InpaintingDataSource * const iDataProvider, PaddedMatrixDim *dim, bool const hasInvalidPixels,
                                          int curPosition, int curX, int newDispX, int curY, int newDispY, int curZ, int newDispZ)
{
    // If the proposed displacement is the same as the current one; no need to compare.
    if(iDataProvider->GetDispX()->GetData()[curPosition] == newDispX &&
       iDataProvider->GetDispY()->GetData()[curPosition] == newDispY &&
       iDataProvider->GetDispZ()->GetData()[curPosition] == newDispZ)
        return false;

    // If the proposed displacement is illegal; no need to compare.
    if(0 != CommonInpainting::IsLegalDisplacement(iDataProvider->GetOcc(), dim, curX, newDispX, curY, newDispY, curZ, newDispZ))
        return false;

    // No more reason not to evaluate the proposed displacement...
    float minEval = iDataProvider->GetDispA()->GetData()[curPosition];
    float curEval = iEvaluator->EvaluateDisplacement(iDataProvider, minEval, hasInvalidPixels, curX, curY, curZ, newDispX, newDispY, newDispZ);

    // If the proposed displacement evaluates as better, replace the current displacement with the proposed displacement
    if(curEval >= 0 && curEval < minEval){
        iDataProvider->GetDispA()->GetData()[curPosition] = curEval;
        iDataProvider->GetDispX()->GetData()[curPosition] = newDispX;
        iDataProvider->GetDispY()->GetData()[curPosition] = newDispY;
        iDataProvider->GetDispZ()->GetData()[curPosition] = newDispZ;

        return true;
    }

    return false;
}

int ISRandom::RandomSearch(InpaintingDataSource * const iDataProvider, bool const hasInvalidPixels)
{
    // Counts the amount of modified pixels and returns it.
    int nbModified = 0;

    // The dimension of the Matrices we'll be working with.
    PaddedMatrixDim * levelDim = iDataProvider->GetDim();

    // The data we'll be working with.
    float* detailOccData = iDataProvider->GetOcc()->GetData();
    int* shiftVolXData = iDataProvider->GetDispX()->GetData();
    int* shiftVolYData = iDataProvider->GetDispY()->GetData();
    int* shiftVolZData = iDataProvider->GetDispZ()->GetData();

    // Calculate the random TODO : this could be done only once per level
    int wMax = __max(levelDim->GetSizeX(),__max(levelDim->GetSizeY(), levelDim->GetSizeZ()));
    int zMax = (int)ceil((float)-(log((float)wMax))/log(0.5f)); //0.5 is alpha in matlab, represents downsampling rate...
    int wValues[zMax];
    for(int z = 0; z < zMax; ++z)
        wValues[z] = (int)roundf(wMax*(powf(0.5f,z)));

    PaddedMatrixIt matIt = PaddedMatrixIt(levelDim);
    do{
        if(detailOccData[matIt.GetPosition()] < DOCC_NEAROCC)
            continue;

        bool wasModified = false;

        for(int ii = 0; ii < zMax; ++ii){
            int xTemp = matIt.GetCurX() + shiftVolXData[matIt.GetPosition()];
            int yTemp = matIt.GetCurY() + shiftVolYData[matIt.GetPosition()];
            int zTemp = matIt.GetCurZ() + shiftVolZData[matIt.GetPosition()];
            int wTemp = wValues[ii];

            // X Values // TODO The 2 comes from the half patch size...
            int randMinX = __max(xTemp - wTemp, 2);
            int randMaxX = __min(xTemp + wTemp, levelDim->GetSizeX() - 2 - 1);

            // Y Values
            int randMinY = __max(yTemp - wTemp, 2);
            int randMaxY = __min(yTemp + wTemp, levelDim->GetSizeY() - 2 - 1);

            // Z Values
            int randMinZ = __max(zTemp - wTemp, 2);
            int randMaxZ = __min(zTemp + wTemp, levelDim->GetSizeZ() - 2 - 1);

            int randDispX = (randMaxX == randMinX ? randMaxX : rand()%(randMaxX - randMinX + 1) + randMinX);
            int randDispY = (randMaxY == randMinY ? randMaxY : rand()%(randMaxY - randMinY + 1) + randMinY);
            int randDispZ = (randMaxZ == randMinZ ? randMaxZ : rand()%(randMaxZ - randMinZ + 1) + randMinZ);

            if(CheckForBetterDisplacement(iDataProvider, levelDim, hasInvalidPixels,
                                          matIt.GetPosition(),
                                          matIt.GetCurX(), randDispX - matIt.GetCurX(),
                                          matIt.GetCurY(), randDispY - matIt.GetCurY(),
                                          matIt.GetCurZ(), randDispZ - matIt.GetCurZ()))
                wasModified = true;
        }

        if(wasModified)
            nbModified++;

    }while(matIt.Next());

    return nbModified;
}

int ISRandom::NeighborSearch(InpaintingDataSource * const iDataProvider, bool const hasInvalidPixels, bool const goForward)
{
    // Counts the amount of modified pixels and returns it.
    int nbModified = 0;

    // The dimension of the Matrices we'll be working with.
    PaddedMatrixDim * levelDim = iDataProvider->GetDim();

    // The data we'll be working with.
    float* detailOccData = iDataProvider->GetOcc()->GetData();
    int* shiftVolXData = iDataProvider->GetDispX()->GetData();
    int* shiftVolYData = iDataProvider->GetDispY()->GetData();
    int* shiftVolZData = iDataProvider->GetDispZ()->GetData();

    PaddedMatrixIt * volumeIt = (goForward ? new PaddedMatrixIt(levelDim) : (PaddedMatrixIt *)new PaddedMatrixRevIt(levelDim));
    do{
        if(detailOccData[volumeIt->GetPosition()] < DOCC_NEAROCC)
            continue;

        // Propagate
        int checkDispX = 0, checkDispY = 0, checkDispZ = 0;
        if(goForward){
            if(volumeIt->GetCurX() != 0) checkDispX = -1;
            if(volumeIt->GetCurY() != 0) checkDispY = -1;
            if(volumeIt->GetCurZ() != 0) checkDispZ = -1;
        }
        else{
            if(volumeIt->GetCurX() != (levelDim->GetSizeX() - 1)) checkDispX = 1;
            if(volumeIt->GetCurY() != (levelDim->GetSizeY() - 1)) checkDispY = 1;
            if(volumeIt->GetCurZ() != (levelDim->GetSizeZ() - 1)) checkDispZ = 1;
        }

        bool wasModified = false;
        int newDispX, newDispY, newDispZ;

        if(checkDispX != 0){
            int xNeighborPos = volumeIt->GetPosition() + checkDispX;
            newDispX = shiftVolXData[xNeighborPos];
            newDispY = shiftVolYData[xNeighborPos];
            newDispZ = shiftVolZData[xNeighborPos];

            if(CheckForBetterDisplacement(iDataProvider, levelDim, hasInvalidPixels,
                                          volumeIt->GetPosition(),
                                          volumeIt->GetCurX(), newDispX,
                                          volumeIt->GetCurY(), newDispY,
                                          volumeIt->GetCurZ(), newDispZ))
                wasModified = true;
        }

        if(checkDispY != 0){
            int yNeighborPos = volumeIt->GetPosition() + checkDispY * levelDim->GetBuffSizeX();
            newDispX = shiftVolXData[yNeighborPos];
            newDispY = shiftVolYData[yNeighborPos];
            newDispZ = shiftVolZData[yNeighborPos];

            if(CheckForBetterDisplacement(iDataProvider, levelDim, hasInvalidPixels,
                                          volumeIt->GetPosition(),
                                          volumeIt->GetCurX(), newDispX,
                                          volumeIt->GetCurY(), newDispY,
                                          volumeIt->GetCurZ(), newDispZ))
                wasModified = true;
        }

        if(checkDispZ != 0){
            int zNeighborPos = volumeIt->GetPosition() + checkDispZ * levelDim->GetBuffAreaXY();
            newDispX = shiftVolXData[zNeighborPos];
            newDispY = shiftVolYData[zNeighborPos];
            newDispZ = shiftVolZData[zNeighborPos];

            if(CheckForBetterDisplacement(iDataProvider, levelDim, hasInvalidPixels,
                                          volumeIt->GetPosition(),
                                          volumeIt->GetCurX(), newDispX,
                                          volumeIt->GetCurY(), newDispY,
                                          volumeIt->GetCurZ(), newDispZ))
                wasModified = true;
        }

        if(wasModified)
            nbModified++;

    }while(volumeIt->Next());
    delete volumeIt;

    return nbModified;
}

void ISRandom::InitialiseDisplacementField(InpaintingDataSource * const iDataProvider, bool const hasInvalidPixels){
    // The dimension of the Matrices we'll be working with.
    PaddedMatrixDim * levelDim = iDataProvider->GetDim();

    // The data we'll be working with.
    int* dispXData = iDataProvider->GetDispX()->GetData();
    int* dispYData = iDataProvider->GetDispY()->GetData();
    int* dispZData = iDataProvider->GetDispZ()->GetData();
    float* dispAData = iDataProvider->GetDispA()->GetData();

    PaddedMatrixIt volIt = PaddedMatrixIt(levelDim);
    do{
        int xDispT = dispXData[volIt.GetPosition()];
        int yDispT = dispYData[volIt.GetPosition()];
        int zDispT = dispZData[volIt.GetPosition()];

        int status = CommonInpainting::IsLegalDisplacement(iDataProvider->GetOcc(), levelDim,
                                                           volIt.GetCurX(), xDispT,
                                                           volIt.GetCurY(), yDispT,
                                                           volIt.GetCurZ(), zDispT);
        while(status != 0){
            xDispT = rand() % (levelDim->GetSizeX() - 2 * patchHalfSize - 1) + patchHalfSize - volIt.GetCurX();
            yDispT = rand() % (levelDim->GetSizeY() - 2 * patchHalfSize - 1) + patchHalfSize - volIt.GetCurY();
            zDispT = rand() % (levelDim->GetSizeZ() - 2 * patchHalfSize - 1) + patchHalfSize - volIt.GetCurZ();

            status = CommonInpainting::IsLegalDisplacement(iDataProvider->GetOcc(), levelDim,
                                                           volIt.GetCurX(), xDispT,
                                                           volIt.GetCurY(), yDispT,
                                                           volIt.GetCurZ(), zDispT);
        }

        dispXData[volIt.GetPosition()] = xDispT;
        dispYData[volIt.GetPosition()] = yDispT;
        dispZData[volIt.GetPosition()] = zDispT;
        dispAData[volIt.GetPosition()] = iEvaluator->EvaluateDisplacement(iDataProvider,
                                                                          -1.0f, hasInvalidPixels,
                                                                          volIt.GetCurX(), volIt.GetCurY(), volIt.GetCurZ(),
                                                                          xDispT, yDispT, zDispT);
    }while(volIt.Next());
}