//
// Created by Jonathan on 13/04/2016.
//

#ifndef INPAINTINGALGORITHMRUN_ISRANDOM_H
#define INPAINTINGALGORITHMRUN_ISRANDOM_H

#include <random>
#include <cfloat>

#include "../../standardTools/PaddedMatrixRevIt.h"
#include "../InpaintingSearcher.h"

class ISRandom : public InpaintingSearcher{
private:
    const int searchIterations = 20; // Max number of propagate / random loops per PatchMatach iteration.

    // Tools
    // std::mt19937 randEngine = std::mt19937(0);

    // TODO Hardcoded so we know what to remove later...
    int patchHalfSize = 2; // Works for X, Y and Z since they're all the same...

    InpaintingEvaluator * const iEvaluator;

public:
    ISRandom(int const searchIterations, InpaintingEvaluator * const iEvaluator);
    ~ISRandom();

    virtual void Search(InpaintingDataSource * const iDataProvider, bool const hasInvalidPixels);

private:
    bool CheckForBetterDisplacement(InpaintingDataSource * const iDataProvider, PaddedMatrixDim *dim, bool const hasInvalidPixels,
                                    int curPosition, int curX, int newDispX, int curY, int newDispY, int curZ, int newDispZ);

    int RandomSearch(InpaintingDataSource * const iDataProvider, bool const hasInvalidPixels);

    int NeighborSearch(InpaintingDataSource * const iDataProvider, bool const hasInvalidPixels, bool const goForward);

    void InitialiseDisplacementField(InpaintingDataSource * const iDataProvider, bool const hasInvalidPixels);
};


#endif //INPAINTINGALGORITHMRUN_ISRANDOM_H
