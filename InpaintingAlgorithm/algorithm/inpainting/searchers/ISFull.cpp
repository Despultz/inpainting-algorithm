//
// Created by Jonathan on 4/25/2016.
//

#include <iostream>
#include "ISFull.h"

ISFull::ISFull(InpaintingEvaluator * const iEvaluator) : InpaintingSearcher(), iEvaluator(iEvaluator){}
ISFull::~ISFull(){}

void ISFull::Search(InpaintingDataSource *iDataProvider, bool hasInvalidPixels)
{
    float* detailOccData = iDataProvider->GetOcc()->GetData();
    int* dispXData = iDataProvider->GetDispX()->GetData();
    int* dispYData = iDataProvider->GetDispY()->GetData();
    int* dispZData = iDataProvider->GetDispZ()->GetData();
    float* dispAData = iDataProvider->GetDispA()->GetData();

    PaddedMatrixDim * levelDim = iDataProvider->GetDim();
    PaddedMatrixIt volumeIt = PaddedMatrixIt(levelDim);
    PaddedMatrixIt fullVolumeIt = PaddedMatrixIt(levelDim);

    int totalPixelsToInpaint = 0;
    do{
        if(detailOccData[volumeIt.GetPosition()] <= DOCC_NEARBORDER
           || detailOccData[volumeIt.GetPosition()] == DOCC_CORE)
            continue;

        totalPixelsToInpaint++;
    }while(volumeIt.Next());

    int pixelCount = 0;
    volumeIt.Start();
    do{
        if(detailOccData[volumeIt.GetPosition()] <= DOCC_NEARBORDER
           || detailOccData[volumeIt.GetPosition()] == DOCC_CORE)
            continue;

        float smallestError = dispAData[volumeIt.GetPosition()];
        int bestDispX = dispXData[volumeIt.GetPosition()];
        int bestDispY = dispYData[volumeIt.GetPosition()];
        int bestDispZ = dispZData[volumeIt.GetPosition()];

        fullVolumeIt.Start();
        do{
            if(detailOccData[fullVolumeIt.GetPosition()] != DOCC_NORMAL) continue;

            int dispX = fullVolumeIt.GetCurX() - volumeIt.GetCurX();
            int dispY = fullVolumeIt.GetCurY() - volumeIt.GetCurY();
            int dispZ = fullVolumeIt.GetCurZ() - volumeIt.GetCurZ();

            float ssd = iEvaluator->EvaluateDisplacement(iDataProvider, smallestError, hasInvalidPixels, volumeIt.GetCurX(), volumeIt.GetCurY(), volumeIt.GetCurZ(), dispX, dispY, dispZ);
            if(ssd > 0.0f){ // -1.0f != ssd){ // TODO The fact that the evaluation can't return 0 without causing problems is problematic. Zero could mean perfect match.
                smallestError = ssd;
                bestDispX = dispX;
                bestDispY = dispY;
                bestDispZ = dispZ;
            }
        }while(fullVolumeIt.Next());
        std::cout << "Found best match for a pixel..." << pixelCount++ << " on " << totalPixelsToInpaint << std::endl;

        dispAData[volumeIt.GetPosition()] = smallestError;
        dispXData[volumeIt.GetPosition()] = bestDispX;
        dispYData[volumeIt.GetPosition()] = bestDispY;
        dispZData[volumeIt.GetPosition()] = bestDispZ;

    }while(volumeIt.Next());
}