//
// Created by Jonathan on 4/25/2016.
//

#ifndef INPAINTINGALGORITHMRUN_ISPROFILER_H
#define INPAINTINGALGORITHMRUN_ISPROFILER_H

#include "../../InpaintingTools/InpaintingProfiler.h"
#include "../InpaintingSearcher.h"
#include "../InpaintingEvaluator.h"

class ISProfiler : public InpaintingSearcher{
private:
    InpaintingProfiler * const iProfiler;
    InpaintingSearcher * const iSearcher;

public:
    ISProfiler(InpaintingProfiler * const iProfiler, InpaintingSearcher * const iSearcher);
    ~ISProfiler();

    void Search(InpaintingDataSource *iDataProvider, bool hasInvalidPixels);
};


#endif //INPAINTINGALGORITHMRUN_ISPROFILER_H
