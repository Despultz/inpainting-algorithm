//
// Created by Jonathan on 4/25/2016.
//

#include "ISProfiler.h"

ISProfiler::ISProfiler(InpaintingProfiler * const iProfiler, InpaintingSearcher * const iSearcher)
        : InpaintingSearcher(), iProfiler(iProfiler), iSearcher(iSearcher) {}

ISProfiler::~ISProfiler(){}

void ISProfiler::Search(InpaintingDataSource *iDataProvider, bool hasInvalidPixels)
{
    iProfiler->BeforeSearch(0, 0);
    iSearcher->Search(iDataProvider, hasInvalidPixels);
    iProfiler->AfterSearch(0, 0);
}