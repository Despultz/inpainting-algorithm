//
// Created by Jonathan on 4/25/2016.
//

#ifndef INPAINTINGALGORITHMRUN_ISFULL_H
#define INPAINTINGALGORITHMRUN_ISFULL_H

#include "../InpaintingSearcher.h"

class ISFull : public InpaintingSearcher{
private:
    InpaintingEvaluator * const iEvaluator;

public:
    ISFull(InpaintingEvaluator * const iEvaluator);
    ~ISFull();

    virtual void Search(InpaintingDataSource *iDataProvider, bool hasInvalidPixels);
};


#endif //INPAINTINGALGORITHMRUN_ISFULL_H
