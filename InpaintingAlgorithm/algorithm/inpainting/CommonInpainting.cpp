//
// Created by Jonathan on 13/04/2016.
//

#include "CommonInpainting.h"

// 1 : Out of bounds
// 2 : Occluded pixel
// 3 : Uninitialized
int CommonInpainting::IsLegalDisplacement(PaddedMatrix<float> *detailedOcc, PaddedMatrixDim *dim,
                                          int curX, int dispX, int curY, int dispY, int curZ, int dispZ)
{
    if(dispX == 0 || dispY == 0 || dispZ == 0)
        return 3;

    // TODO This whole function shouldn't exist.
    int newX = curX + dispX;
    int newY = curY + dispY;
    int newZ = curZ + dispZ;

    if(newX >= 0 &&
       newY >= 0 &&
       newZ >= 0 &&
       newX < dim->GetSizeX() &&
       newY < dim->GetSizeY() &&
       newZ < dim->GetSizeZ())
        if(detailedOcc->Get(newX, newY, newZ) == DOCC_NORMAL)
            return 0;
        else
            return 2;
    else
        return 1;
}