//
// Created by Jonathan on 23/02/2016.
//

#ifndef INPAINTINGALGORITHM_INPAINTER_H
#define INPAINTINGALGORITHM_INPAINTER_H

#include <string>
#include <cmath>
#include <cfloat>
#include <random>
#include <algorithm>

#include "../standardTools/PaddedMatrix.h"
#include "../standardTools/PaddedMatrixKernelIt.h"
#include "../standardTools/PaddedMatrixRevIt.h"
#include "../inpaintingTools/InpaintingIO.h"
#include "../inpaintingTools/InpaintingProfiler.h"
#include "../standardTools/ImageManipulator.h"
#include "CommonInpainting.h"
#include "dataSources/InpaintingDataSource.h"
#include "dataSources/decorators/IDSDDownsampling.h"
#include "dataSources/decorators/IDSDDetailedOcclusion.h"
#include "dataSources/decorators/IDSDDetailedOcclusionPeeling.h"
#include "InpaintingReconstructor.h"
#include "reconstructors/IRWeightedAverage.h"
#include "searchers/ISRandom.h"

class InpaintingConverger {
private:
    // Parameters of the convergence
    const int downsamplingLevels = 3; // Number of downsampled levels there will be in the pyramid.
    const int downsamplingRate = 2;
    const int nbIterations = 5; // TODO 20 and make it settable through the constructor // Max number of iteration per level.
    const float residualThreshold = 0.1; // When the variance between two iteration is small enough, we can skip.

    // All the dataSources
    InpaintingSearcher * const iSearcher;
    InpaintingReconstructor * const iReconstructor;
    InpaintingEvaluator * const iEvaluator;

public:
    InpaintingConverger(int const downsamplingLevels, int const downsamplingRate,
                        int const nbIterations, float const residualThreshold,
                        InpaintingSearcher * const iSearcher,
                        InpaintingReconstructor * const iReconstructor,
                        InpaintingEvaluator * const iEvaluator);

    ~InpaintingConverger();

    void Inpaint(InpaintingDataSource * const iDataSource, InpaintingProfiler * const iProfiler);

private:
    void OnionPeelInitialisation(InpaintingDataSource * const iDataSource, InpaintingProfiler * const iProfiler);
};


#endif //INPAINTINGALGORITHM_INPAINTER_H
