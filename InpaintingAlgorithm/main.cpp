#include <iostream>
#include <inpainting/searchers/ISFull.h>
#include <inpainting/searchers/ISProfiler.h>
#include <inpainting/reconstructors/IRProfiler.h>
#include <inpainting/reconstructors/IRBestPatch.h>

#include "inpaintingTools/InpaintingOpenCVIO.h"
#include "inpaintingTools/InpaintingProfiler.h"
#include "inpainting/dataSources/IDSConcrete.h"
#include "inpainting/InpaintingConverger.h"


int main(){
    InpaintingOpenCVIO openCVIO = InpaintingOpenCVIO();
    InpaintingProfiler profiler = InpaintingProfiler();

    const std::string videoFileName = "./resources/beach_umbrella.avi";
    const std::string occFileName = "./resources/beach_umbrella_occlusion.png";

    profiler.BeforeInpainting();

    // Select and parameterize the algorithms
    InpaintingEvaluator evaluationAlgo = InpaintingEvaluator(50, 5, 5, 5);
    ISRandom searchAlgo = ISRandom(20, &evaluationAlgo);
    // ISFull searchAlgo = ISFull(&evaluationAlgo);
    IRWeightedAverage reconstructAlgo = IRWeightedAverage(0.75f, 5, 5, 5);

    // To profile
    ISProfiler searchProfiler = ISProfiler(&profiler, &searchAlgo);
    IRProfiler reconstructProfiler = IRProfiler(&profiler, &reconstructAlgo);

    // Final algo
    InpaintingConverger inpainter = InpaintingConverger(3, 2, 20, 0.1f/255.0f, &searchProfiler, &reconstructProfiler, &evaluationAlgo);

    // Load the data source // TODO Move this to another class.
    int sizeX, sizeY, sizeZ;
    openCVIO.GetVideoDimensions(videoFileName, sizeX, sizeY, sizeZ);
    PaddedMatrixDim * const dim = new PaddedMatrixDim(sizeX, sizeY, sizeZ, 2, 2, 2);
    PaddedMatrix<float> * const redMatrix = new PaddedMatrix<float>(dim);
    PaddedMatrix<float> * const greenMatrix = new PaddedMatrix<float>(dim);
    PaddedMatrix<float> * const blueMatrix = new PaddedMatrix<float>(dim);
    PaddedMatrix<float> * const occMatrix = new PaddedMatrix<float>(dim);
    openCVIO.LoadRGBMatrixFromVideo(videoFileName, redMatrix, greenMatrix, blueMatrix);
    openCVIO.LoadOcclusionMatrixFromImage(occFileName, occMatrix);

    IDSConcrete baseIPS = IDSConcrete(dim, redMatrix, greenMatrix, blueMatrix, occMatrix);

    // Execute the work
    inpainter.Inpaint(&baseIPS, &profiler);

    // Get better quality, less blurry, final images
    IDSDDetailedOcclusion detOcc = IDSDDetailedOcclusion(&baseIPS, 5, 5, 5);
    IRBestPatch bestPatch = IRBestPatch(5, 5, 5);
    bestPatch.Reconstruct(&detOcc, false);

    // Output the new images.
    for(int i = 0; i < baseIPS.GetDim()->GetSizeZ(); ++i)
        openCVIO.GeneralIOForRGB(baseIPS.GetRed(), baseIPS.GetGreen(), baseIPS.GetBlue(), i,
                                 InpaintingIO::IH_DBG_SAVE, "finalReconstructedImgF" + std::to_string(i));

    profiler.AfterInpainting();

    return 0;
}