# Introduction #

This repository contains the code for an inpainting algorithm. The inpainting algorithm contained in this repository comes from the work of Newson, A., Almansa, A., Fradet, M., Gousseau, Y., & Pérez, P. The repository contains the code translated in C++ in the hopes of obtaining a faster and more adaptable version of the algorithm.

## References ##

Newson, A., Almansa, A., Fradet, M., Gousseau, Y., & Pérez, P. (2014). Video Inpainting of Complex Scenes. SIAM Journal of Imaging Science, 7(4), 26. 

## Getting started ##

This project uses CMake (3.4.3) and MinGW (w64 3.4). However to get the project running you'll need to setup OpenCV (3.1).

Additionally, this project includes Google Test (1.7.0) to perform the unit tests.

The IDE uses to bind this all together is JetBrains CLion (2016.1.2).

## Current State ##

This project is still a work in progress. The initial translation of the algorithm from MATLAB to C++ is almost done, but the final result is not quite of the same quality as the original implementation, and so the focus of the development is to find the issue(s) causing the lesser quality output.

To do so many unit tests are being implemented that are aiming to verify that the C++ implementation reproduces the behaviour of the MATLAB algorithm. These unit test take the raw data (uncompressed binary) from MATLAB before and after various functions and verify that the C++ algorithm gives the same output if the same input is given.

The next step - once all the unit tests are written and that the quality of the output is the same for both implmentation - is to use various technologies (OpenMP, OpenCL, CUDA) to accelerate the algorithm.

## Development Team ##

As seen in the commits, I (Jonathan Cameron) am the only one contributing to this project.

The original idea of this project goes to Professor Carlos Vázquez and Pierre Antoine Vidal from the ÉTS, and a little non-development help was given by them.

## Questions or Inquiries ##

You can email jonathan.cameron@hotmail.com for any comments regarding this project.